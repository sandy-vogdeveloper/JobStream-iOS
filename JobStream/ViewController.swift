//
//  ViewController.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 24/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
    
    //UIControls declarations here
   // var btnAddPImage = UIButton!
   // var btnSkip = UIButton!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnLogin1: UIButton!
    
    @IBOutlet var btnSkip: UIButton!
    @IBOutlet var btnSkip1: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self .loadUI()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    //MARK: initialUI
    func loadUI()
    {
        btnLogin.addTarget(self, action:#selector(self.btnLoginPressed), for: .touchUpInside)
        
        btnLogin1.addTarget(self, action:#selector(self.btnLoginPressed), for: .touchUpInside)
        
        btnSkip.addTarget(self, action:#selector(self.btnSkipPressed), for: .touchUpInside)
        
        btnSkip1.addTarget(self, action:#selector(self.btnSkipPressed), for: .touchUpInside)
    }
    
    //MARK: Button Pressed delegate declarations here
    func btnLoginPressed()
    {
        let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
        self.navigationController?.pushViewController(LC, animated: true)
    }
    
    func btnSkipPressed()
    {
        let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
        self.navigationController?.pushViewController(UMD, animated: true)
    }
}

