//
//  AppDelegate.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 24/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var viewSpinner:UIView!
    var activityIndicator:UIActivityIndicatorView!
    var strSelectedUser = NSString()
    var arrULoginData: [String] = []
    var isPostAJobPressed = Bool()
    var strLUID = NSString()
    var strLEmail = NSString()
    var strLPhone = NSString()
    var strPStatus = NSString()
    var strLRole = NSString()
    var strLName = NSString()
    var strLPassword = NSString()
    /*--------      Employer job selection      ----------*/
    var strSelectedJobAppliedID = NSString()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        isPostAJobPressed=false
        STPPaymentConfiguration.shared().publishableKey = "pk_test_dSXnNqrdDHx34RUKgR329650"
        //let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
        
        let LC = MainHomeScreen(nibName: "MainHomeScreen", bundle: nil)
       
        let navigationController = UINavigationController.init(rootViewController: LC)
        
        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)
        
        window!.rootViewController = navigationController
        window!.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    //MARK: show spinner and stop spinner view
    func showSpinnerView(_view:UIView)
    {
        //        if str_color_code == nil
        //        {
        //            clickedUIColor=UIColor.init(colorLiteralRed:52/255.0, green:144/255.0, blue: 197/255.0, alpha: 1)
        //        }
        //        else
        //        {
        //            clickedUIColor = self.convertHexToUIColor(hexColor: str_color_code as String)
        //        }
        viewSpinner=UIView()
        viewSpinner?.frame=CGRect(x:Constant.GlobalConstants.screenWidth/2-35, y:Constant.GlobalConstants.screenHeight/2-35, width: 70, height: 70)
        viewSpinner?.backgroundColor=UIColor.gray
        viewSpinner?.layer.borderColor=UIColor.white.cgColor
        viewSpinner?.layer.borderWidth=1
        viewSpinner?.layer.cornerRadius=5
        _view .addSubview(viewSpinner!)
        
        activityIndicator=UIActivityIndicatorView()
        activityIndicator?.frame = CGRect(x: (viewSpinner?.frame.size.width)!/2-15, y: (viewSpinner?.frame.size.width)!/2-15, width: 30, height: 30)
        activityIndicator?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator?.startAnimating()
        viewSpinner?.addSubview(activityIndicator!)
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopSpinner()
    {
        viewSpinner?.removeFromSuperview()
        activityIndicator?.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
    }


}

