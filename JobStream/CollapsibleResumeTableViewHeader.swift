//
//  CollapsibleTableViewHeader.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 5/30/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//

import UIKit

protocol CollapsibleRTableViewHeaderDelegate {
    func toggleSection(_ header: CollapsibleResumeTableViewHeader, section: Int)
}

class CollapsibleResumeTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleRTableViewHeaderDelegate?
    var section: Int = 0
  
    var lblTitle=UILabel()
    var btn_down=UIButton()
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        //
        
        
        // btn_down.tag=section)
        lblTitle .removeFromSuperview()
        lblTitle = UILabel(frame: CGRect(x: 10, y: 5, width: 120, height: 30))
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 20)
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        lblTitle.textAlignment=NSTextAlignment.left
      
        contentView.addSubview(lblTitle)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(gestureRecognizer:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool) {
        btn_down.rotate(collapsed ? 1 : CGFloat(M_PI_2))
    }
    

    
}
