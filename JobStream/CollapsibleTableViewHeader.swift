
import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: CollapsibleTableViewHeader, section: Int)
}



class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    var arrowLabel = UILabel()
    var btn_down=UIButton()
    var headerLabel=UILabel()
    var headerlbldose=UILabel()
    var headerlblactual=UILabel()
    var headerlblschedule=UILabel()
    var lbl_1=UILabel()
    
    var view_tfpass:UIView!
    
    var imgBar=UIImageView()
    var lblTitle=UILabel()
    var lblDesc=UILabel()
    var lblType=UILabel()
    var lblUpdate = UILabel()
    var viewForBG = UIView()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
               //
        contentView.backgroundColor=UIColor.clear
        
        viewForBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: 68)
        viewForBG.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        contentView.addSubview(viewForBG)
        
       // btn_down.tag=section)
        lblTitle .removeFromSuperview()
        lblTitle = UILabel(frame: CGRect(x: 5, y: 2, width: 120, height: 30))
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 20)
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        lblType.textAlignment=NSTextAlignment.left
        
        lblDesc .removeFromSuperview()
        lblDesc = UILabel(frame: CGRect(x: 5, y: lblTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth/2+30, height: 40))
        lblDesc.font = UIFont(name: "Helvetica-Light", size: 14)
        lblDesc.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        lblDesc.text="Brif description of what this \n category has"
        lblDesc.textAlignment=NSTextAlignment.left
        lblDesc.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDesc.numberOfLines = 0
        
        lblType .removeFromSuperview()
        lblType = UILabel(frame: CGRect(x: Constant.GlobalConstants.screenWidth-100, y: 2, width: 90, height: 30))
        lblType.font = UIFont(name: "Helvetica-Light", size: 14)
        lblType.text="Fulltime"
        lblType.textAlignment=NSTextAlignment.right
        lblType.textColor=UIColor.init(red: 59/255.0, green: 113/255.0, blue: 175/255.0, alpha: 1)
        
        lblUpdate .removeFromSuperview()
        lblUpdate = UILabel(frame: CGRect(x:Constant.GlobalConstants.screenWidth-60, y: lblType.frame.maxY, width: 50, height: 30))
        lblUpdate.text="2 New"
        lblUpdate.font = UIFont(name: "Helvetica-Light", size: 14)
        lblUpdate.textColor=UIColor.init(red: 59/255.0, green: 113/255.0, blue: 175/255.0, alpha: 1)
        lblUpdate.textAlignment=NSTextAlignment.right
        
        viewForBG.addSubview(lblTitle)
        viewForBG.addSubview(lblDesc)
        viewForBG.addSubview(lblType)
        viewForBG.addSubview(lblUpdate)
        //
        
        let viewSept = UIView()
        viewSept.frame=CGRect(x: 0, y: contentView.frame.size.height-2, width: Constant.GlobalConstants.screenWidth, height: 2)
        viewSept.backgroundColor=UIColor.white
        contentView.addSubview(viewSept)
        
        // Call tapHeader when tapping on this header
        //
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(gestureRecognizer:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()             
        
 }
    
    //
    // Trigger toggle section when tapping on the header
    //
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        
       
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool) {
               //btn_down.rotate(collapsed ? 1 : CGFloat(M_PI_2))
    }
    
}
