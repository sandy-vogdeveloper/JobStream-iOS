//
//  ProfileCompleteScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 11/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class ProfileCompleteScreen: UIViewController {

    @IBOutlet var btnContinue: UIButton!
    @IBOutlet var btnContinue1: UIButton!
    
    @IBOutlet var lblProf1: UILabel!
    @IBOutlet var lblProf2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblProf1.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        
        lblProf2.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        
         lblProf1.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        
        btnContinue.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnContinue.addTarget(self, action:#selector(self.btnContinuePressed), for:.touchUpInside)
        btnContinue1.addTarget(self, action:#selector(self.btnContinuePressed), for:.touchUpInside)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: UIButton Pressed delegate
    
    //MARK: Button save pressed
    func btnContinuePressed(sender: UIButton!)
    {
        Constant.GlobalConstants.theAppDelegate.isPostAJobPressed=false
        let WTS = WorkTypeScreen(nibName: "WorkTypeScreen", bundle: nil)
        self.navigationController?.pushViewController(WTS, animated: true)

    }
}
