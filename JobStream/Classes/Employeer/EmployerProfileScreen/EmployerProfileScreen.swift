//
//  EmployerProfileScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 12/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class EmployerProfileScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var viewForAlert = UIView()
    
    var tblProfileInfo = UITableView()
    
    var arrTitles : [String] = ["Name","Email","Phone Number","Password","Address","Website","Description"]
    
    var btnEdit = UIButton()
    var btnJob = UIButton()
    var btnCalagry = UIButton()
    var imgProfile = UIImageView()
    var imgProfileBG = UIImageView()
    var lblUName = UILabel()
    var lblJob = UILabel()
    var lblCalagry = UILabel()
    var arrProfInfo:NSMutableArray=[]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadInitialUI()
        getProfileInfoAPI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: Load InitialUI
    func loadInitialUI()
    {
        if Constant.GlobalConstants.screenWidth<325
        {
           imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*35)
        }
        else
        {
           imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*30)
        }
        
        imgProfileBG.image=UIImage(named: "icon_NProfile")
        //imgProfileBG.alpha=0.5
        self.view.addSubview(imgProfileBG)
        
        var viewForAlpha = UIView()
        viewForAlpha=UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width,height:imgProfileBG.frame.size.height))
        let whte = UIColor.black
        viewForAlpha.backgroundColor = whte.withAlphaComponent(0.7)
        imgProfileBG.addSubview(viewForAlpha)
        
        btnBack.frame=CGRect(x: 3, y: 5, width: 18,height: 18)
        let image = UIImage(named: "icon_BackMove") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btnBackClicked), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btnBackClicked), for:.touchUpInside)
        //btnEdit.addTarget(self, action:#selector(self.btnEditProfPressed), for:.touchUpInside)
        self.view.addSubview(btnBack)
        
        imgProfile.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-60, y: Constant.GlobalConstants.screenHeight/100*5, width: 120, height: 120)
        imgProfile.image=UIImage(named: "icon_NProfile")
        imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
        imgProfile.clipsToBounds=true
        self.view.addSubview(imgProfile)
        
        //let tap = UITapGestureRecognizer(target: self, action: #selector(showActionSheet))
        //imgProfile.addGestureRecognizer(tap)
        imgProfile.isUserInteractionEnabled = true
        
        lblUName.frame=CGRect(x: 0, y: imgProfile.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblUName.text=""
        lblUName.textAlignment=NSTextAlignment.center
        lblUName.font=UIFont(name: "Helvetica", size: 16)
        lblUName.textColor=UIColor.white
        self.view.addSubview(lblUName)
        
        btnEdit.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: 0, width: 80, height: 30)
        btnEdit .setTitle("Edit", for: .normal)
        btnEdit.titleLabel?.font = UIFont(name: "Helvetica", size: 18)
        btnEdit .setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(btnEdit)
        
        btnJob.frame=CGRect(x: 30, y: imgProfileBG.frame.size.height/2-25, width: 25, height: 27)
        let image_1 = UIImage(named: "img_Jobs") as UIImage?
        btnJob.setImage(image_1, for: .normal)
        btnJob.addTarget(self, action:#selector(self.btnJobListPressed), for:.touchUpInside)
        self.view.addSubview(btnJob)
        
        lblJob.frame=CGRect(x: 12, y: btnJob.frame.maxY, width: 60, height: 30)
        lblJob.text="Jobs"
        lblJob.textColor=UIColor.white
        lblJob.font = UIFont(name: "Helvetica", size: 14)
        lblJob.textAlignment=NSTextAlignment.center
        self.view.addSubview(lblJob)
        
        btnCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-53, y: imgProfileBG.frame.size.height/2-25, width: 18, height: 23)
        let image1 = UIImage(named: "icon_MLocation") as UIImage?
        btnCalagry.setImage(image1, for: .normal)
        btnCalagry.addTarget(self, action:#selector(self.btnCalgrayPressed), for:.touchUpInside)
        self.view.addSubview(btnCalagry)
        
        lblCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: btnCalagry.frame.maxY, width: 70, height: 30)
        lblCalagry.text="Calgary"
        lblCalagry.textColor=UIColor.white
        lblCalagry.font = UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblCalagry)
        
        tblProfileInfo.frame = CGRect(x: 0, y: imgProfileBG.frame.maxY, width: self.view.frame.size.width, height: self.view.frame.size.height-200)
        tblProfileInfo.dataSource = self
        tblProfileInfo.delegate = self
        tblProfileInfo.tableFooterView=UIView()
        tblProfileInfo.backgroundColor=UIColor.clear
        tblProfileInfo.isScrollEnabled=false
        self.view.addSubview(tblProfileInfo)
    }
    
    func btnBackClicked()
    {
        navigationController?.popViewController(animated:true)
    }
    
    func btnJobListPressed()
    {
        let dispJobs=DisplayAllJobList(nibName: "DisplayAllJobList", bundle: nil)
        self.navigationController?.pushViewController(dispJobs, animated: true)
        
    }
    
    func btnCalgrayPressed()
    {
        let dispJobs=MapScreen(nibName: "MapScreen", bundle: nil)
        self.navigationController?.pushViewController(dispJobs, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrProfInfo .count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let lblApplied=UILabel()
        let lblTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblProfileInfo.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        
        if indexPath.row == 6
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 69)
            
            lblApplied.frame=CGRect(x: 10, y: 0, width: 120, height: 20)
            lblApplied.text="Description"
            lblApplied.textAlignment=NSTextAlignment.left
            lblApplied.font =  UIFont(name:"Helvetica-Light", size: 16)
            lblApplied.textColor=UIColor.black
            
            lblTitle.frame=CGRect(x: 10, y: lblApplied.frame.maxY, width: viewForCell.frame.size.width-10, height: 50)
            lblTitle.text="Add a description of who you are. This will \n  be seen by employers so make sure you \n showcase your best."
            lblTitle.textAlignment=NSTextAlignment.center
            lblTitle.textColor=UIColor.gray
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment=NSTextAlignment.left
            
            viewForCell.addSubview(lblApplied)
            viewForCell .addSubview(lblTitle)
        }
        else
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
            lblTitle.frame=CGRect(x: 10, y: 5, width: Constant.GlobalConstants.screenWidth, height: 30)
            
            lblTitle.text=(arrProfInfo [(indexPath as NSIndexPath).row]as! NSString) as String
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
            
            lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
            viewForCell .addSubview(lblTitle)
        }
        viewForCell.addSubview(lblApplied)
        
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 6
        {
            return 70
        }
        else
        {
            return 40
        }
    }
    
    //MARK: update user Info
    
    func getProfileInfoAPI()
    {
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        //bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLRole), forKey: "role" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsprofile_edit.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                var strName=""
                strName=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "name")as!NSString) as String
                
                lblUName.text=strName
                
                var strEmail = NSString()
                var strPhone = NSString()
                var strPass = NSString()
                var strAddres = NSString()
                var strWeb = NSString()
                var strImage = ""
                var strImgURL = Constant.GlobalConstants.Imgbase_URL
            
                
                strImage=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "image")as!NSString) as String
                if strImage == "no"
                {
                    imgProfile.image=UIImage(named: "icon_NProfile")
                    imgProfileBG.image=UIImage(named: "icon_NProfile")
                }
                else
                {
                    strImgURL = strImgURL+strImage
                    imgProfile.setImageWith(NSURL(string: strImgURL) as URL!, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.white)
                    imgProfileBG.setImageWith(NSURL(string: strImgURL) as URL!, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.white)
                }
                
                strEmail=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "email")as!NSString
                
                strPhone=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "phone")as!NSString
                
                strPass=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "password")as!NSString
                
                strAddres=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "address")as!NSString
                
                strWeb=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "website")as!NSString
                
                
                arrProfInfo .add(Constant.GlobalConstants.theAppDelegate.strLName)
                arrProfInfo .add(strEmail)
                arrProfInfo .add(strPhone)
                arrProfInfo .add(strPass)
                arrProfInfo .add(strAddres)
                arrProfInfo .add(strWeb)
                
                print(arrProfInfo)
                tblProfileInfo.reloadData()
            }
        }
        catch
        {
            print("Error")
        }
    }
        
}
