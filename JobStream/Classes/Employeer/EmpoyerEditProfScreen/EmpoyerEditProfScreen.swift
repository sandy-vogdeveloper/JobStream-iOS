//
//  EmpoyerEditProfScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 11/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class EmpoyerEditProfScreen: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var btnSave = UIButton()
    var btnJob = UIButton()
    var btnCalagry = UIButton()
    
    var imgProfile = UIImageView()
    var imgProfileBG = UIImageView()
    var lblUName = UILabel()
    var lblJob = UILabel()
    var lblCalagry = UILabel()
    
    var viewForName = UIView()
    var viewForEmail = UIView()
    var viewForPassword = UIView()
    var viewForNumber = UIView()
    var viewForAddress = UIView()
    var viewForWebSite = UIView()
    
    var txtName = UITextField()
    var txtEmail = UITextField()
    var txtPassword = UITextField()
    var txtNumber = UITextField()
    var txtAddress = UITextField()
    var txtWebSite = UITextField()
    var app=UIApplication.shared.delegate as! AppDelegate
    var strImageURL = NSString()
    var strWeb = ""
    var strAddrs = ""
    var img_data:NSData!
    var str_profile_img = NSString()
    
    let imageController = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        loadInitialUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
   
    //MARK: Load InitialUI
    func loadInitialUI()
    {
        if Constant.GlobalConstants.screenWidth<325
        {
        imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*35)
        }
        else
        {
            imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*30)
        }
            imgProfileBG.image=UIImage(named: "img_PBG")
        //imgProfileBG.alpha=0.5
        self.view.addSubview(imgProfileBG)
        
        var viewForAlpha = UIView()
        viewForAlpha=UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width,height:imgProfileBG.frame.size.height))
        let whte = UIColor.black
        viewForAlpha.backgroundColor = whte.withAlphaComponent(0.7)
        imgProfileBG.addSubview(viewForAlpha)
        
        imgProfile.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-60, y: Constant.GlobalConstants.screenHeight/100*5, width: 120, height: 120)
        imgProfile.image=UIImage(named: "icon_NProfile")
        imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
        imgProfile.clipsToBounds=true
        imgProfile.layer.borderWidth=1.0
        imgProfile.layer.borderColor=UIColor.white.cgColor
        self.view.addSubview(imgProfile)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showActionSheet))
        imgProfile.addGestureRecognizer(tap)
        imgProfile.isUserInteractionEnabled = true
        
        lblUName.frame=CGRect(x: 0, y: imgProfile.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblUName.text=Constant.GlobalConstants.theAppDelegate.strLName as String
        lblUName.textAlignment=NSTextAlignment.center
        lblUName.font=UIFont(name: "Helvetica", size: 16)
        lblUName.textColor=UIColor.white
        self.view.addSubview(lblUName)
        
        btnSave.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: 0, width: 80, height: 30)
        btnSave .setTitle("Save", for: .normal)
        btnSave.titleLabel?.font = UIFont(name: "Helvetica", size: 18)
        btnSave .setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(btnSave)
        
        btnJob.frame=CGRect(x: 30, y: imgProfileBG.frame.size.height/2-25, width: 25, height: 27)
        let image = UIImage(named: "img_Jobs") as UIImage?
        btnJob.setImage(image, for: .normal)
        btnJob.addTarget(self, action:#selector(btnJobListPressed), for:.touchUpInside)
        self.view.addSubview(btnJob)
        
        lblJob.frame=CGRect(x: 12, y: btnJob.frame.maxY, width: 60, height: 30)
        lblJob.text="Jobs"
        lblJob.textColor=UIColor.white
        lblJob.font = UIFont(name: "Helvetica", size: 14)
        lblJob.textAlignment=NSTextAlignment.center
        self.view.addSubview(lblJob)
        
        btnCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-53, y: imgProfileBG.frame.size.height/2-25, width: 18, height: 23)
        let image1 = UIImage(named: "icon_MLocation") as UIImage?
        btnCalagry.setImage(image1, for: .normal)
        btnCalagry.addTarget(self, action:#selector(btnCalgrayPressed), for:.touchUpInside)
        self.view.addSubview(btnCalagry)
        
        lblCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: btnCalagry.frame.maxY, width: 70, height: 30)
        lblCalagry.text="Calgary"
        lblCalagry.textColor=UIColor.white
        lblCalagry.font = UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblCalagry)
        
        btnSave.addTarget(self, action:#selector(self.btnSavePressed), for:.touchUpInside)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            viewForName.frame=CGRect(x: 0, y: imgProfileBG.frame.maxY+15, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
           viewForName.frame=CGRect(x: 0, y: imgProfileBG.frame.maxY+25, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        
        viewForName.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForName)
        
        viewForEmail.frame=CGRect(x: 0, y: viewForName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForEmail.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForEmail)
        
        viewForPassword.frame=CGRect(x: 0, y: viewForEmail.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForPassword.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForPassword)
        
        viewForNumber.frame=CGRect(x: 0, y: viewForPassword.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForNumber.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForNumber)
       
        viewForAddress.frame=CGRect(x: 0, y: viewForNumber.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForAddress.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForAddress)
        
        viewForWebSite.frame=CGRect(x: 0, y: viewForAddress.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForWebSite.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewForWebSite)
        
        txtName.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtName.backgroundColor=UIColor.clear
        viewForName.addSubview(txtName)
        
        txtEmail.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtEmail.backgroundColor=UIColor.clear
        viewForEmail.addSubview(txtEmail)
        
        txtPassword.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtPassword.backgroundColor=UIColor.clear
        viewForPassword.addSubview(txtPassword)
        
        txtNumber.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtNumber.backgroundColor=UIColor.clear
        viewForNumber.addSubview(txtNumber)
        
        txtAddress.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtAddress.backgroundColor=UIColor.clear
        viewForAddress.addSubview(txtAddress)
        
        txtWebSite.frame=CGRect(x: 20, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtWebSite.backgroundColor=UIColor.clear
        viewForWebSite.addSubview(txtWebSite)
        
        
        txtName.attributedPlaceholder = NSAttributedString(string:"Name", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtNumber.attributedPlaceholder = NSAttributedString(string:"Number", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtAddress.attributedPlaceholder = NSAttributedString(string:"Address", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtWebSite.attributedPlaceholder = NSAttributedString(string:"Website", attributes:[NSForegroundColorAttributeName: UIColor.gray,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtName.delegate=self
        txtEmail.delegate=self
        txtPassword.delegate=self
        txtNumber.delegate=self
        txtAddress.delegate=self
        txtWebSite.delegate=self
        
        txtName.keyboardType=UIKeyboardType.default
        txtNumber.keyboardType=UIKeyboardType.numberPad
        txtEmail.keyboardType=UIKeyboardType.emailAddress
        txtPassword.keyboardType=UIKeyboardType.default
        txtAddress.keyboardType=UIKeyboardType.default
        txtWebSite.keyboardType=UIKeyboardType.default
        
        txtName.returnKeyType = UIReturnKeyType.next
        txtNumber.returnKeyType = UIReturnKeyType.next
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.next
        txtAddress.returnKeyType = UIReturnKeyType.next
        txtWebSite.returnKeyType=UIReturnKeyType.done
        
        txtName.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtNumber.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtEmail.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtPassword.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtAddress.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtWebSite.clearButtonMode = UITextFieldViewMode.whileEditing;
        
        txtWebSite.autocorrectionType = .no
        
        txtName.text=Constant.GlobalConstants.theAppDelegate.strLName as String
        txtEmail.text=Constant.GlobalConstants.theAppDelegate.strLEmail as String
        txtPassword.text=Constant.GlobalConstants.theAppDelegate.strLPassword as String
        txtNumber.text=Constant.GlobalConstants.theAppDelegate.strLPhone as String
     
        txtName.font=UIFont(name: "Helvetica", size: 18)
        txtEmail.font=UIFont(name: "Helvetica", size: 18)
        txtPassword.font=UIFont(name: "Helvetica", size: 18)
        txtNumber.font=UIFont(name: "Helvetica", size: 18)
        txtAddress.font=UIFont(name: "Helvetica", size: 18)
        txtWebSite.font=UIFont(name: "Helvetica", size: 18)
        
        txtName.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        txtEmail.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        txtPassword.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        txtNumber.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        txtAddress.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        txtWebSite.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        
        txtPassword.isSecureTextEntry=true
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtNumber.inputAccessoryView = toolBar
        
    }

    func doneClick() {
        
        txtNumber.resignFirstResponder()
    }
    
    
    //MARK: Button JobList Pressed
    func btnJobListPressed()
    {
        let dispJobs=DisplayAllJobList(nibName: "DisplayAllJobList", bundle: nil)
        self.navigationController?.pushViewController(dispJobs, animated: true)
        
    }
    //MARK: Button Calgray Pressed
    func btnCalgrayPressed()
    {
        let dispJobs=MapScreen(nibName: "MapScreen", bundle: nil)
        self.navigationController?.pushViewController(dispJobs, animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtName {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail
        {
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword
        {
            self.txtNumber.becomeFirstResponder()
        }
        else if textField == self.txtNumber
        {
            self.txtAddress.becomeFirstResponder()
        }
        else if textField == self.txtAddress
        {
            self.txtWebSite.becomeFirstResponder()
        }
        else if textField == self.txtWebSite
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    func isValidTextField() -> Bool
    {
        var strMsg = NSString()
        var intAdrs = NSInteger()
        var intWebSite = NSInteger()
        
        
        intAdrs=(txtAddress.text?.characters.count)!
        intWebSite=(txtWebSite.text?.characters.count)!
   
        if intAdrs > 0
        {
            if intWebSite > 0
            {
                return true;
            }
            else
            {
                strMsg="Please enter Website Name."
            }
        }
        else
        {
            strMsg = "Please enter address"
        }
        
        
        let alertMessage = UIAlertController(title: "Warning", message: strMsg as String, preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alertMessage, animated: true, completion: nil)
        
        return false
    }
    
 
    //MARK: UIButton Pressed delegate
    //MARK: Button save pressed
    func btnSavePressed(sender: UIButton!)
    {
        if app.strSelectedUser == "non-profit"
        {
            let profEdit = ProfileCompleteScreen(nibName: "ProfileCompleteScreen", bundle: nil)
            self.navigationController?.pushViewController(profEdit, animated: true)
        }
        else
        {
            if isValidTextField()
            {
                updateInfoAPI()
            }
        }
    }
    
    
    func showActionSheet(sender: AnyObject) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
           
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        imageController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imageController.allowsEditing = true
        imageController.mediaTypes = UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.photoLibrary)!
        imageController.delegate = self;
        
        present(imageController, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imageController.allowsEditing = false
            imageController.sourceType = UIImagePickerControllerSourceType.camera
            imageController.cameraCaptureMode = .photo
            imageController.delegate=self
            present(imageController, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
  
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("picker cancel.")
    }
    
    //MARK: - Imagpicker Delegates
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        print(chosenImage)
        imgProfile.image=chosenImage
        let size = CGSize(width: 200.0, height: 200.0)
        chosenImage = self.resizeImage(image: chosenImage,targetSize: size)
        
        img_data = UIImageJPEGRepresentation(chosenImage, 1) as NSData!
        
        self.dismiss(animated: true, completion: { () -> Void in
            Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
            self.uploadimagefile1()
        })
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func uploadimagefile1()  {
        //print(img_data)
        // let params = NSMutableDictionary()
        
        let boundaryConstant  = "----------14737809831466499882746641449"
        //api/wsupdateuserimg_ios.php
        let file1ParamConstant = "userfile"
        let requestUrl = NSURL(string:"http://13.126.3.151/job_stream/api/uploadimage_ios.php")
        
        let request = NSMutableURLRequest()
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.httpShouldHandleCookies=false
        //request.timeoutInterval = 30
        request.httpMethod = "POST"
        
        let contentType = "multipart/form-data; boundary=\(boundaryConstant)"
        
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        if img_data==nil
        {
            return
        }
        
        let body = NSMutableData()
        
        // image begin
        body.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("Content-Disposition: form-data; name=\"\(file1ParamConstant)\"; filename=\"image1.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
        
        body.append(img_data! as Data)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        // image end
        body.append("--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody  = body as Data
        let postLength = "\(body.length)"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.url = requestUrl as URL?
        
        var serverResponse = NSString()
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            Constant.GlobalConstants.theAppDelegate.stopSpinner()
            serverResponse = responseString!
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Print out dictionary
                    print(convertedJsonIntoDict)
                    let  strsucess1=(convertedJsonIntoDict .value(forKey: "status") as! NSNumber)
                    if strsucess1 == 1
                    {
                        self.str_profile_img=(convertedJsonIntoDict .value(forKey: "info") as! String as NSString)
                        print(self.str_profile_img)
                    }
                    else
                    {
                        let alertMessage = UIAlertController(title: "Error", message: "Failed to upload" as String, preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                let alertMessage = UIAlertController(title: "Error", message: error.localizedDescription as String, preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
        
        task.resume()
    }
    //MARK: update user Info
    func updateInfoAPI()
    {
        strAddrs=txtAddress.text!
        strWeb=txtWebSite.text!
        
        if self.str_profile_img.length == 0
        {
            self.str_profile_img = "no"
        }
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLName) , forKey: "name" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLEmail), forKey: "email" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLPhone), forKey: "phone" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLPassword), forKey: "password" as NSCopying)
        bodydata.setObject((strAddrs), forKey: "address" as NSCopying)
        bodydata.setObject(("0000"), forKey: "lat" as NSCopying)
        bodydata.setObject(("0000"), forKey: "lang" as NSCopying)
        bodydata.setObject((self.str_profile_img), forKey: "image" as NSCopying)
        bodydata.setObject((strWeb), forKey: "website" as NSCopying)
        bodydata.setObject(("Y"), forKey: "notification" as NSCopying)
        bodydata.setObject(("aaaa"), forKey: "about" as NSCopying)
      
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsemployerprofile_update.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Employer_Proflile(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Employer_Proflile(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                let paymt=PayPaymentScreen(nibName: "PayPaymentScreen", bundle: nil)
                self.navigationController?.pushViewController(paymt, animated: true)
            }
            
            
        }
        catch
        {
            print("Error")
        }
        
    }
    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -140
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
}
