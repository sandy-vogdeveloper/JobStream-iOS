//
//  DisplayAllJobList.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 06/06/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class DisplayAllJobList: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var tableView: UITableView  =   UITableView()
    
    var lblTitle = UILabel()
    var arrJobID:NSArray=[]
    var arrJobList:NSMutableArray=[]
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadInitialUI()
        getEmployerJobListAPI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: load InitialUI
    func loadInitialUI()
    {
        lblTitle=UILabel(frame: CGRect(x: 10, y: 0, width: Constant.GlobalConstants.screenWidth-20, height: 30))
        lblTitle.text="My Job List"
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblTitle.font =  UIFont(name:"Helvetica", size: 22)
        lblTitle.textAlignment = .center
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        self.view.addSubview(lblTitle)
    
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y: 0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        tableView.frame = CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height-200)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView=UIView()
        tableView.backgroundColor=UIColor.clear
        tableView.isScrollEnabled=false
        self.view.addSubview(tableView)
    }
    
    func btnBackClicked()
    {
        navigationController?.popViewController(animated:true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrJobList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let lblPostion=UILabel()
        let lblSubCat=UILabel()
        let lblCatgory=UILabel()
        let lblJobStatus=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tableView.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 80)
        viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        
        lblPostion.frame=CGRect(x: 5,y: 0,width: UIScreen.main.bounds.size.width-20,height:30)
        lblSubCat.frame=CGRect(x: 5,y: 30,width: UIScreen.main.bounds.size.width-20,height:30)
        lblCatgory.frame=CGRect(x: 5, y: lblSubCat.frame.maxY, width: 100, height: 20)
        lblJobStatus.frame=CGRect(x: Constant.GlobalConstants.screenWidth-110, y: 50, width: 100, height: 30)
        
        var dict = NSDictionary()
        
        dict = arrJobList[(indexPath as NSIndexPath).row] as! NSDictionary
        lblPostion.text = dict.value(forKey: "position") as! String?
        
        lblSubCat.text = dict.value(forKey: "subcatname") as! String?
        lblCatgory.text = dict.value(forKey: "catname") as! String?
        lblJobStatus.text = dict.value(forKey: "job_status") as! String?
        
        lblPostion.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        lblSubCat.textColor=UIColor.gray
        lblCatgory.textColor=UIColor.gray
        lblJobStatus.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        
        lblPostion.textAlignment=NSTextAlignment.left
        lblSubCat.textAlignment=NSTextAlignment.left
        lblCatgory.textAlignment=NSTextAlignment.left
        lblJobStatus.textAlignment=NSTextAlignment.right
        
        lblPostion.font =  UIFont(name:"Helvetica", size: 20)
        lblSubCat.font =  UIFont(name:"Helvetica-Light", size: 14)
        lblCatgory.font =  UIFont(name:"Helvetica", size: 14)
        lblJobStatus.font =  UIFont(name:"Helvetica", size: 10)
        
        viewForCell .addSubview(lblPostion)
        viewForCell .addSubview(lblSubCat)
        viewForCell .addSubview(lblCatgory)
        viewForCell .addSubview(lblJobStatus)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    //MARK: update user Info
    func getEmployerJobListAPI()
    {
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        print("BODY DATA for Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjoblist.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_Type(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_Type(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                self.arrJobList = (ResponceData.value(forKey: "info")as! NSMutableArray)
                
                //self.arrWorkTypeID = (ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "id")as! Array as NSArray
                
                tableView.reloadData()
            }
        }
        catch
        {
            print("Error")
        }
        
    }
}
