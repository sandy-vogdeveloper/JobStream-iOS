//
//  MainHomeScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 26/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class MainHomeScreen: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet var lblSubTitle: UILabel!
    var tableView: UITableView  =   UITableView()
    let arrFields : [String] = ["Looking for Employers","Looking for Employees","Non-Profit Organization"]
    var btnBtmNotification = UIButton()
    var imgDot = UIImageView()
    var imgChat = UIImageView()
    
    var app=UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.loadMainUI()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK: LoadUI
    func loadMainUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblSubTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
       
        tableView.frame = CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.size.height-250)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView=UIView()
        tableView.backgroundColor=UIColor.clear
        tableView.isScrollEnabled=false
        self.view.addSubview(tableView)
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-25, width: Constant.GlobalConstants.screenWidth, height: 25)
        btnBtmNotification.setImage(UIImage(named:"img_NotiBG"), for: UIControlState.normal)
        //btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 3)
        imgDot.backgroundColor = UIColor.white
        imgDot.layer.borderWidth = 0.5
        imgDot.layer.borderColor = UIColor.white.cgColor
        imgDot.layer.cornerRadius = 3.0
        btnBtmNotification.addSubview(imgDot)
        
        imgChat.frame=CGRect(x: 10, y: 2.5, width: 20, height: 20)
        imgChat.image=UIImage(named: "icon_ChatN")
        btnBtmNotification.addSubview(imgChat)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tableView.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 60)
        viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        imgBar.frame=CGRect(x: 5,y: 5,width:8,height: 60)
        imgBar.image = UIImage(named:"icon_Bar")
        //viewForCell .addSubview(imgBar)
        
        lblTitle.frame=CGRect(x: 5,y: 10,width: UIScreen.main.bounds.size.width-20,height:40)
        lblTitle.text = arrFields [(indexPath as NSIndexPath).row]
        lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        lblTitle.textAlignment=NSTextAlignment.left
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 20)
        viewForCell .addSubview(lblTitle)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        app.strSelectedUser=arrFields [(indexPath as NSIndexPath).row] as NSString
        
        if indexPath.row==0
        {
            Constant.GlobalConstants.theAppDelegate.strSelectedUser="employer"
        }
        else if indexPath.row==1
        {
            Constant.GlobalConstants.theAppDelegate.strSelectedUser="employee"
        }
        else
        {
            Constant.GlobalConstants.theAppDelegate.strSelectedUser="non-profit"
        }
       let LC = RegistrationScreen(nibName: "RegistrationScreen", bundle: nil)
       self.navigationController?.pushViewController(LC, animated: true)
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
}
