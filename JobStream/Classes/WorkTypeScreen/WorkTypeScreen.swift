//
//  WorkTypeScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 26/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class WorkTypeScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var tableView: UITableView  =   UITableView()
    
    @IBOutlet var lblTitle: UILabel!
    var arrWorkTypeID:NSArray=[]
    var arrWorkCatagory:NSMutableArray=[]
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadInitialUI()
        getWorkTypeAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }

    //MARK: load InitialUI
    func loadInitialUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        tableView.frame = CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height-200)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView=UIView()
        tableView.backgroundColor=UIColor.clear
        tableView.isScrollEnabled=false
        self.view.addSubview(tableView)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrWorkCatagory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        let lblSubTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tableView.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
     
        lblTitle.frame=CGRect(x: 5,y: 0,width: UIScreen.main.bounds.size.width-20,height:30)
        lblSubTitle.frame=CGRect(x: 5,y: 30,width: UIScreen.main.bounds.size.width-20,height:30)
         var dict = NSDictionary()
        
        dict = arrWorkCatagory[(indexPath as NSIndexPath).row] as! NSDictionary
        lblTitle.text = dict.value(forKey: "category") as! String?
        
        lblSubTitle.text = "Breif description of what this category has"
        
        lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        lblSubTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        
        lblTitle.textAlignment=NSTextAlignment.left
        lblSubTitle.textAlignment=NSTextAlignment.left
        
        lblTitle.font =  UIFont(name:"Helvetica", size: 20)
        lblSubTitle.font =  UIFont(name:"Helvetica-Light", size: 14)
        
        viewForCell .addSubview(lblTitle)
        viewForCell .addSubview(lblSubTitle)
        return cell;        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dict = NSDictionary()
        
        dict = arrWorkCatagory[(indexPath as NSIndexPath).row] as! NSDictionary
        
        
        
        let WTS = WorkSelectionScreen(nibName: "WorkSelectionScreen", bundle: nil)
        WTS.strCatID = dict.value(forKey: "id") as! NSString
        WTS.strSelectTitle = dict.value(forKey: "category") as! NSString
        
        self.navigationController?.pushViewController(WTS, animated: true)
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
     //MARK: update user Info
    func getWorkTypeAPI()
    {
        let bodydata = NSMutableDictionary()
        print("BODY DATA for Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsgetCategory.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_Type(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_Type(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                self.arrWorkCatagory = (ResponceData.value(forKey: "info")as! NSMutableArray)
                
                //self.arrWorkTypeID = (ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "id")as! Array as NSArray
                
                tableView.reloadData()
            }
        }
        catch
        {
            print("Error")
        }
        
    }
}
