//
//  LoginScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 25/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//  

import UIKit

class LoginScreen: UIViewController,UITextFieldDelegate
{
    var btnSignUp = UIButton()
    var btnSignUp1 = UIButton()
    var btnSkip = UIButton()
    var btnSkip1 = UIButton()
    @IBOutlet var lblTitle: UILabel!
    
    var txtEmail = UITextField()
    var txtPassword = UITextField()
    var btnSignIn = UIButton()
    var btnForgtPass = UIButton()
    var viewEmail = UIView()
    var viewPassword = UIView()
    var viewForAnimtion = UIView()
    var imgEmail = UIImageView()
    var imgPassword = UIImageView()
    var isopen = Bool()
    
    var stremail = ""
    var strpass = ""
    
    var app=UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadMainUI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    //MARK: Load MainUI
    func loadMainUI()
    {
        isopen=false
        
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)        
        viewEmail=UIView(frame: CGRect(x: 0, y: Constant.GlobalConstants.screenHeight/100*28, width: self.view.frame.size.width,height:75))
        
        viewPassword=UIView(frame: CGRect(x: 0, y: viewEmail.frame.maxY+10, width: self.view.frame.size.width,height:75))
        btnSignIn=UIButton(frame: CGRect(x: 0, y: viewPassword.frame.maxY+15, width:Constant.GlobalConstants.screenWidth, height: 40))
        
        btnForgtPass=UIButton(frame: CGRect(x: 40, y: btnSignIn.frame.maxY+10, width:Constant.GlobalConstants.screenWidth-80, height: 40))
        
        viewEmail.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        viewPassword.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        
        btnSignIn.backgroundColor=UIColor(red: 39.0/255.0, green: 83/255.0, blue: 126/255.0, alpha: 1.0)
        btnSignIn.setTitle("Sign In", for: .normal)
        btnForgtPass.setTitle("Forgot your password?", for: .normal)
        
        btnSignIn.setTitleColor(UIColor.white, for: .normal)
        btnForgtPass.setTitleColor(UIColor (red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        
        btnSignIn.titleLabel!.font =  UIFont(name:"Helvetica", size: 22)
        btnForgtPass.titleLabel!.font =  UIFont(name:"Helvetica_Light", size: 20)
        
        btnSignIn.addTarget(self, action:#selector(self.btnSignInPressed), for:.touchUpInside)
        btnForgtPass.addTarget(self, action:#selector(self.btnForgetPasswordPressed), for:.touchUpInside)
        
        btnSignUp.frame=CGRect(x: 10, y: Constant.GlobalConstants.screenHeight-35, width: 28, height: 30)
        btnSignUp1.frame=CGRect(x: btnSignUp.frame.maxX+5, y: Constant.GlobalConstants.screenHeight-35, width: 120, height: 30)
        btnSkip.frame=CGRect(x: Constant.GlobalConstants.screenWidth-140, y: Constant.GlobalConstants.screenHeight-35, width: 110, height: 30)
        btnSkip1.frame=CGRect(x: btnSkip.frame.maxX+7, y: Constant.GlobalConstants.screenHeight-28, width: 10, height: 18)
        
        btnSignUp1.setTitle("Sign Up", for: .normal)
        btnSkip.setTitle("Skip", for: .normal)
        
        btnSignUp1.setTitleColor(UIColor.white, for: .normal)
        btnSkip.setTitleColor(UIColor.white, for: .normal)
        
        btnSignUp1.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 16)
        btnSkip.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 16)
        
        let image = UIImage(named: "icon_CProf") as UIImage?
        let image1 = UIImage(named: "imgSkip") as UIImage?
        
        btnSignUp.setImage(image, for: .normal)
        btnSkip1.setImage(image1, for: .normal)
        
        btnSignUp.addTarget(self, action:#selector(self.btnSignUpPressed), for:.touchUpInside)
        btnSignUp1.addTarget(self, action:#selector(self.btnSignUpPressed), for:.touchUpInside)
        
        btnSignUp1.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btnSkip.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        
        self.view.addSubview(viewEmail)
        self.view.addSubview(viewPassword)
        self.view.addSubview(btnSignIn)
        self.view.addSubview(btnForgtPass)
        self.view.addSubview(btnSignUp)
        self.view.addSubview(btnSignUp1)
        self.view.addSubview(btnSkip)
        self.view.addSubview(btnSkip1)
        
        imgEmail=UIImageView(frame: CGRect(x: 10, y: 18, width:viewEmail.frame.size.height-20, height: viewEmail.frame.size.height-36))
        imgPassword=UIImageView(frame: CGRect(x: 15, y: 10, width:viewPassword.frame.size.height-30, height:viewPassword.frame.size.height-20))
        
        imgEmail.image = UIImage(named:"icon_Email")
        imgPassword.image = UIImage(named:"icon_Password")
        
        viewEmail.addSubview(imgEmail)
        viewPassword.addSubview(imgPassword)        
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-100, height: 40))
            txtPassword=UITextField(frame:CGRect(x: imgPassword.frame.maxX+10, y: 17, width: viewPassword.frame.size.width-imgPassword.frame.size.width-100, height: 40))
        }
        else
        {
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-40, height: 40))
            txtPassword=UITextField(frame:CGRect(x: imgPassword.frame.maxX+10, y: 17, width: viewPassword.frame.size.width-imgPassword.frame.size.width-40, height: 40))
        }
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtEmail.font =  UIFont(name:"Helvetica-Light", size: 22)
        txtPassword.font =  UIFont(name:"Helvetica-Light", size: 22)
        
        txtEmail.keyboardType=UIKeyboardType.emailAddress
        txtPassword.keyboardType=UIKeyboardType.default
        
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.done
        
        txtEmail.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtPassword.clearButtonMode = UITextFieldViewMode.whileEditing;
        
        txtEmail.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        txtPassword.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        txtEmail.keyboardType = .emailAddress
    
       viewEmail .addSubview(txtEmail)
        viewPassword .addSubview(txtPassword)
        
        txtEmail.autocorrectionType = .no
        txtPassword.autocorrectionType = .no
        txtPassword.isSecureTextEntry=true
        
        configureTextField(x: 0,y: txtEmail.frame.size.height-1.0, width: txtEmail.frame.size.width, height:1.0, textField: txtEmail)
        
        configureTextField(x: 0,y: txtPassword.frame.size.height-1.0, width: txtPassword.frame.size.width, height:1.0, textField: txtPassword)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: Add underline at textfield bottom
    func configureTextField(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,textField:UITextField)
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: x, y: y, width: width, height: height)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        textField.borderStyle = UITextBorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
    func btnSignInPressed()
    {
        if isValidTextField()
        {
            loadLoginAPI()
        }
    }
    
    func btnSignUpPressed()
    {
        let LC = MainHomeScreen(nibName: "MainHomeScreen", bundle: nil)
        self.navigationController?.pushViewController(LC, animated: true)
    }
    
    func btnForgetPasswordPressed()
    {
        let LC = ForgetPasswordScreen(nibName: "ForgetPasswordScreen", bundle: nil)
        self.navigationController?.pushViewController(LC, animated: true)
    }
    
    func isValidTextField() -> Bool
    {
        
        var strMsg = NSString()
        
        var intEmail = NSInteger()
        var intPass = NSInteger()
        
            intEmail=(txtEmail.text?.characters.count)!
            intPass=(txtPassword.text?.characters.count)!
        
        if intEmail > 0
        {
           if intPass > 0
           {
                 return true;
           }
           else
           {
            strMsg="Please enter Password."
           }
        }
        else
        {
            strMsg = "Please enter Email."
        }
        let alertMessage = UIAlertController(title: "Warning", message: strMsg as String, preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alertMessage, animated: true, completion: nil)
        
        return false
    }
    
    //MARK: Login ServerCall
    func loadLoginAPI()
    {
        stremail=txtEmail.text!
        strpass=txtPassword.text!
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((stremail), forKey: "email" as NSCopying)
        bodydata.setObject((strpass), forKey: "password" as NSCopying)
        
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wslogin.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_user_login(apiAlias:response:)), andDelegate: self)
    }
    
    func response_user_login(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
                strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {                
                Constant.GlobalConstants.theAppDelegate.strLUID=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "uid")as!NSString
                Constant.GlobalConstants.theAppDelegate.strLName=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "name")as!NSString
                Constant.GlobalConstants.theAppDelegate.strLPhone=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "phone")as!NSString
                Constant.GlobalConstants.theAppDelegate.strLEmail=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "email")as!NSString
                Constant.GlobalConstants.theAppDelegate.strLRole=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "role")as!NSString
                Constant.GlobalConstants.theAppDelegate.strPStatus=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "profile_status")as!NSString
                Constant.GlobalConstants.theAppDelegate.strLPassword=strpass as NSString
                
                Constant.GlobalConstants.theAppDelegate.strSelectedUser=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "role")as!NSString
                
                if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
                {
                    var strCat = NSString()
                    
                    strCat=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "category_sel")as!NSString
                    if strCat == "yes"
                    {
                        let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
                        self.navigationController?.pushViewController(UMD, animated: true)
                    }
                    else
                    {
                        let WTS = WorkTypeScreen(nibName: "WorkTypeScreen", bundle: nil)
                        self.navigationController?.pushViewController(WTS, animated: true)
                    }
                }
                else if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employer"
                {
                    //let LC = EmpoyerEditProfScreen(nibName: "EmpoyerEditProfScreen", bundle: nil)
                   // self.navigationController?.pushViewController(LC, animated: true)
                    
                     var strCat = NSString()
                    
                    strCat=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "category_sel")as!NSString
                    if strCat == "yes"
                    {
                        let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
                        self.navigationController?.pushViewController(UMD, animated: true)
                    }
                    else
                    {
                        let LC = EmpoyerEditProfScreen(nibName: "EmpoyerEditProfScreen", bundle: nil)
                        self.navigationController?.pushViewController(LC, animated: true)
                    }
                }
                else if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "non-profit"
                {
                    let LC = EmpoyerEditProfScreen(nibName: "EmpoyerEditProfScreen", bundle: nil)
                    self.navigationController?.pushViewController(LC, animated: true)
                }
            }
            else
            {
                let alertMessage = UIAlertController(title: "Error", message: "Invalid details" as String, preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }

          }
            catch
            {
                let alertMessage = UIAlertController(title: "Sorry!", message: "Invalid login details" as String, preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)

            }
        
    }
}
