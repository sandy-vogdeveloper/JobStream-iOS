//
//  ForgetPasswordScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 10/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class ForgetPasswordScreen: UIViewController,UITextFieldDelegate {

    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnSignUp1: UIButton!
     @IBOutlet var lblTitle: UILabel!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    
    var txtEmail = UITextField()
    var btnSubmit = UIButton()
    var viewEmail = UIView()
    var imgEmail = UIImageView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadMainUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Load MainUI
    func loadMainUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)   
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        viewEmail=UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height/100*25, width: self.view.frame.size.width,height:75))
        
         btnSubmit=UIButton(frame: CGRect(x: 0, y: viewEmail.frame.maxY+15, width:Constant.GlobalConstants.screenWidth, height: 40))
        
        let whte = UIColor.white
        viewEmail.backgroundColor = whte.withAlphaComponent(0.8)
        
        btnSubmit.backgroundColor=UIColor(red: 24.0/255.0, green: 72.0/255.0, blue: 117.0/255.0, alpha: 1.0).withAlphaComponent(0.7)
        btnSubmit.setTitle("Submit", for: .normal)
        btnSubmit.setTitleColor(UIColor.white, for: .normal)
        btnSubmit.titleLabel!.font =  UIFont(name:"Helvetica", size: 20)
        btnSubmit.addTarget(self, action:#selector(self.btnSubmitPressed), for:.touchUpInside)
        
        self.view.addSubview(viewEmail)       
        self.view.addSubview(btnSubmit)
        
        imgEmail=UIImageView(frame: CGRect(x: 10, y: 18, width:viewEmail.frame.size.height-20, height: viewEmail.frame.size.height-36))
        
        imgEmail.image = UIImage(named:"icon_Email")
        
        viewEmail.addSubview(imgEmail)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-100, height: 40))
        }
        else
        {
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-40, height: 40))
        }
        txtEmail.placeholder="Email"
        
        txtEmail.font =  UIFont(name:"Helvetica", size: 22)
       
        txtEmail.keyboardType=UIKeyboardType.default
        
        txtEmail.returnKeyType = UIReturnKeyType.done
        
        txtEmail.clearButtonMode = UITextFieldViewMode.whileEditing;
        
        txtEmail.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        txtEmail.delegate = self
        
        viewEmail .addSubview(txtEmail)
        
        configureTextField(x: 0,y: txtEmail.frame.size.height-1.0, width: txtEmail.frame.size.width, height:1.0, textField: txtEmail)
        }
    
    //MARK: Add underline at textfield bottom
    func configureTextField(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,textField:UITextField)
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: x, y: y, width: width, height: height)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        textField.borderStyle = UITextBorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }   

    func btnSubmitPressed(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }

}
