//
//  ViewResumeScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 02/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
// 0f79c76bcd09edb3f53401ef5ec7119fab4b1519
//4043bc420393f341e5030d6a61c32633ccf3cb04

import UIKit

class ViewResumeScreen: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var bgView:UIImageView!
    var topView:UIView!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var lblHeading:UILabel!
    
    var profileView:UIView!
    var line:UIView!
    
    var personImg:UIImageView!
    var bottomImg:UIImageView!
    
    var btnStar:UIButton!
    var btnNext:UIButton!
    
    var lblName:UILabel!
    var lblStreetName:UILabel!
    var lblCityName:UILabel!
    var lblPhone:UILabel!
    var lblEmail:UILabel!
    var lblShortList:UILabel!
    var lblContact:UILabel!
    
    var tblExperience:UITableView!
    
    var array_Experience:NSMutableArray!
    var array_Exp1:NSMutableArray!
    var array_Exp2:NSMutableArray!
    var viewBG = UIView()
    
    var app=UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()

        loadArray()
        loadInitialUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadArray()
    {
        array_Experience=["Work Experience:","Education:","Interests:","Skills:","References:"]
        array_Exp1=["Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame"]
        array_Exp2=["Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame","Lorem ipsum dolor sit ame"]
    }
    
    func loadInitialUI()
    {
        bgView=UIImageView(frame: CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight))
        bgView.image=UIImage(named: "img_BG")
        self.view.addSubview(bgView)
        
        topView=UIView(frame:CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: 30))
        topView.backgroundColor = .clear
        self.view.addSubview(topView)
        
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y: 0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        topView.addSubview(btnBack)
        topView.addSubview(btnBack1)
        
        
        lblHeading=UILabel(frame: CGRect(x: btnBack.frame.maxX+10, y: 0, width: Constant.GlobalConstants.screenWidth-70, height: 30))
        lblHeading.text="Resume"
        lblHeading.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblHeading.font =  UIFont(name:"Helvetica", size: 22)
        lblHeading.textAlignment = .center
        topView.addSubview(lblHeading)
        
        if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
        {
            viewBG.frame=CGRect(x: 5, y: topView.frame.maxY, width: Constant.GlobalConstants.screenWidth-10,height: Constant.GlobalConstants.screenHeight-50)
        }
        else
        {
            viewBG.frame=CGRect(x: 5, y: topView.frame.maxY, width: Constant.GlobalConstants.screenWidth-10,height: Constant.GlobalConstants.screenHeight-140)
        }
        
        viewBG.layer.borderWidth=1.0
        viewBG.layer.borderColor = UIColor.gray.withAlphaComponent(0.7).cgColor
        viewBG.layer.cornerRadius=5.0
        viewBG.backgroundColor = UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewBG)
        
        personImg=UIImageView(frame: CGRect(x: 5, y: 5, width: 100, height: 100))
        personImg.image=UIImage(named: "img_Profile")
        personImg.layer.cornerRadius=personImg.frame.size.height/2
        personImg.layer.masksToBounds=true
        viewBG.addSubview(personImg)
        
        lblName=UILabel(frame: CGRect(x: personImg.frame.maxX+20, y: 0, width: Constant.GlobalConstants.screenWidth-120, height: 30))
        lblName.text="Jay Garrick"
        lblName.textColor=UIColor.white
        lblName.font =  UIFont(name:"Helvetica", size: 20)
        viewBG.addSubview(lblName)
        
        
        lblStreetName=UILabel(frame: CGRect(x: personImg.frame.maxX+20, y: lblName.frame.maxY, width:Constant.GlobalConstants.screenWidth-120, height: 20))
        lblStreetName.text="Street Name(if given)"
        lblStreetName.textColor=UIColor.white
        lblStreetName.font =  UIFont(name:"Helvetica", size: 14)
        viewBG.addSubview(lblStreetName)
        
        lblCityName=UILabel(frame: CGRect(x: personImg.frame.maxX+20, y: lblStreetName.frame.maxY, width: Constant.GlobalConstants.screenWidth-120, height: 20))
        lblCityName.text="City,Province,Postal Code"
        lblCityName.textColor=UIColor.white
        lblCityName.font =  UIFont(name:"Helvetica", size: 14)
        viewBG.addSubview(lblCityName)
        
        lblPhone=UILabel(frame: CGRect(x: personImg.frame.maxX+20, y: lblCityName.frame.maxY, width: Constant.GlobalConstants.screenWidth-120, height: 20))
        lblPhone.text="Phone Number"
        lblPhone.textColor=UIColor.white
        lblPhone.font =  UIFont(name:"Helvetica", size: 14)
        viewBG.addSubview(lblPhone)
        
        lblEmail=UILabel(frame: CGRect(x: personImg.frame.maxX+20, y: lblPhone.frame.maxY, width: Constant.GlobalConstants.screenWidth-120, height: 20))
        lblEmail.text="Email"
        lblEmail.textColor=UIColor.white
        lblEmail.font =  UIFont(name:"Helvetica", size: 14)
        viewBG.addSubview(lblEmail)
        
        line=UIView(frame: CGRect(x: 0, y: lblEmail.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 1))
        line.backgroundColor = .white
        viewBG.addSubview(line)
        
        tblExperience=UITableView(frame: CGRect(x: 0, y: line.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: viewBG.frame.size.height-110))
        tblExperience.delegate=self
        tblExperience.dataSource=self
        tblExperience.separatorStyle=UITableViewCellSeparatorStyle.none
        tblExperience.backgroundColor=UIColor.clear
        viewBG.addSubview(tblExperience)
        
        
        if app.strSelectedUser == "employee"
        {
            tblExperience.frame=CGRect(x: 0, y: line.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-150)
        }
        else
        {
            bottomImg=UIImageView(frame: CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-80, width: Constant.GlobalConstants.screenWidth, height: 80))
            bottomImg.backgroundColor=UIColor.gray.withAlphaComponent(0.7)
            self.view.addSubview(bottomImg)
        
            if Constant.GlobalConstants.screenWidth<325
            {
                btnStar=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth/4+7, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            }
            else
            {
                btnStar=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth/4+17, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            }
        
            btnStar.setImage(UIImage(named:"icon_str"), for: .normal)
            btnStar.addTarget(self, action: #selector(btnStarClicked), for: .touchUpInside)
            btnStar.backgroundColor = .clear
            self.view.addSubview(btnStar)
        
            btnNext=UIButton(frame: CGRect(x: Constant.GlobalConstants.screenWidth/1.6, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            btnNext.setImage(UIImage(named:"icon_FArrow"), for: .normal)
            btnNext.addTarget(self, action: #selector(self.btnContactApplicntPressed), for: .touchUpInside)
            btnNext.backgroundColor = .clear
            self.view.addSubview(btnNext)
        
            lblShortList=UILabel(frame: CGRect(x:0, y: btnStar.frame.maxY, width: Constant.GlobalConstants.screenWidth/2-30, height: 30))
            lblShortList.text="ShortList"
            lblShortList.textColor=UIColor.white
            lblShortList.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblShortList.textAlignment=NSTextAlignment.right
            self.view.addSubview(lblShortList)
        
            lblContact=UILabel(frame: CGRect(x: Constant.GlobalConstants.screenWidth/2, y: btnNext.frame.maxY, width: Constant.GlobalConstants.screenWidth/2, height: 30))
            lblContact.text="  Contact Applicant"
            lblContact.textColor=UIColor.white
            lblContact.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblContact.textAlignment=NSTextAlignment.center
            lblContact.textAlignment = .left;
            self.view.addSubview(lblContact)
        }
    }
    
    func btnStarClicked()
    {
        ShortListResumeAPI()
    }
    
    func btnContactApplicntPressed()
    {
        let cntApplicnt=UserChatScreen()
        self.navigationController?.pushViewController(cntApplicnt, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return array_Experience.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        let lblTitle=UILabel(frame: CGRect(x: 20, y: 5, width: Constant.GlobalConstants.screenWidth-20, height: 30))
        lblTitle.text=array_Experience.object(at: indexPath.row) as? String
        lblTitle.backgroundColor=UIColor.clear
        lblTitle.textColor = .white
        lblTitle.font=UIFont.boldSystemFont(ofSize: 20)
        cell.contentView.addSubview(lblTitle)
        
        
        let imgCircle1=UIImageView(frame: CGRect(x: 25, y: lblTitle.frame.maxY+10, width: 10, height: 10))
        imgCircle1.image=UIImage(named:"icon_FCircle")
        imgCircle1.backgroundColor=UIColor.clear
        cell.contentView.addSubview(imgCircle1)
        
        let lblExp1=UILabel(frame: CGRect(x:imgCircle1.frame.maxX+5, y:  lblTitle.frame.maxY+5, width: Constant.GlobalConstants.screenWidth-25, height: 20))
        lblExp1.text=array_Exp1.object(at: indexPath.row) as? String
        lblExp1.backgroundColor=UIColor.clear
        lblExp1.textColor = .white
        lblExp1.font=UIFont.systemFont(ofSize: 15)
        cell.contentView.addSubview(lblExp1)
        
        let imgCircle2=UIImageView(frame: CGRect(x: 25, y: lblExp1.frame.maxY+10, width: 10, height: 10))
        imgCircle2.image=UIImage(named:"icon_FCircle")
        imgCircle2.backgroundColor=UIColor.clear
        cell.contentView.addSubview(imgCircle2)
        
        let lblExp2=UILabel(frame: CGRect(x: imgCircle1.frame.maxX+5, y: lblExp1.frame.maxY+5, width: Constant.GlobalConstants.screenWidth-25, height: 20))
        lblExp2.text=array_Exp2.object(at: indexPath.row) as? String
        lblExp2.backgroundColor=UIColor.clear
        lblExp2.textColor = .white
        lblExp2.font=UIFont.systemFont(ofSize: 15)
        cell.contentView.addSubview(lblExp2)
        
        cell.backgroundColor=UIColor.clear
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func btnBackClicked()
    {
        navigationController?.popViewController(animated:true)
    }

    //MARK: ShortList the user resume for job ServerCall
    func ShortListResumeAPI()
    {
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strSelectedJobAppliedID), forKey: "jobapplied_id" as NSCopying)
        
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsshortlist.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_user_login(apiAlias:response:)), andDelegate: self)
    }
    
    func response_user_login(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                let alertMessage = UIAlertController(title: "Success", message: "User Shortlisted" as String, preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
        catch
        {
            let alertMessage = UIAlertController(title: "Sorry!", message: "Failed to shortlist" as String, preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
            
        }
        
    }
}
