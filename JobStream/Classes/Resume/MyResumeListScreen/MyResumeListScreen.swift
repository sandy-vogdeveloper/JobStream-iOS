//
//  MyResumeListScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 02/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class MyResumeListScreen: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var bgView:UIImageView!
    var topView:UIView!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var btnBtmNotification = UIButton()
    var lblHeading:UILabel!
    
    var array_list:NSMutableArray!
    
    var tblList:UITableView!
    var bottomImg:UIImageView!
    var imgChat = UIImageView()
    var btnCircle:UIButton!
    var btnAdd:UIButton!
    
    var isopen = Bool()
    var viewForChooseResume = UIView()
    
    var viewForNotification = UIView()
    var btnNotiSetting = UIButton()
    var imgDot = UIImageView()
    var lblNoti = UILabel()
    var tblNotifications = UITableView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadArray()
        loadInitialUI()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadArray()
    {
        array_list=["Resume Name","Resume Name(Draft)","Resume Name"]
    }
    
    func loadInitialUI()
    {
        self.isopen=false
        bgView=UIImageView(frame: CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight))
        bgView.image=UIImage(named: "img_BG")
        self.view.addSubview(bgView)
        
        topView=UIView(frame:CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: 40))
        topView.backgroundColor = .clear
        self.view.addSubview(topView)
                
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(btnBackClicked), for:.touchUpInside)
        
        topView.addSubview(btnBack)
        topView.addSubview(btnBack1)
        
        lblHeading=UILabel(frame: CGRect(x: btnBack.frame.maxX+10, y: 0, width: Constant.GlobalConstants.screenWidth-70, height: 30))
        lblHeading.text="Resume"
        lblHeading.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblHeading.font =  UIFont(name:"Helvetica", size: 22)
        lblHeading.textAlignment = .center
        topView.addSubview(lblHeading)
        
        tblList=UITableView(frame: CGRect(x: 0, y: topView.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-100))
        tblList.delegate=self
        tblList.dataSource=self
        tblList.separatorStyle=UITableViewCellSeparatorStyle.none
        tblList.backgroundColor=UIColor.clear
        self.view.addSubview(tblList)
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-25, width: Constant.GlobalConstants.screenWidth, height: 25)
        btnBtmNotification.setImage(UIImage(named:"img_NotiBG"), for: UIControlState.normal)
        btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 3)
        imgDot.backgroundColor = UIColor.white
        imgDot.layer.borderWidth = 0.5
        imgDot.layer.borderColor = UIColor.white.cgColor
        imgDot.layer.cornerRadius = 3.0
        btnBtmNotification.addSubview(imgDot)
        
        imgChat.frame=CGRect(x: 10, y: 2.5, width: 20, height: 20)
        imgChat.image=UIImage(named: "icon_ChatN")
        btnBtmNotification.addSubview(imgChat)
        
        
        btnAdd=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth-52, y: Constant.GlobalConstants.screenHeight-80, width:40, height: 40))
        btnAdd.setImage(UIImage(named:"icon_PlusNew"), for: .normal)
        btnAdd.addTarget(self, action: #selector(btnAddClicked), for: .touchUpInside)
        btnAdd.backgroundColor = .clear
        self.view.addSubview(btnAdd)
        
        
    }
    
    //MARK: UIButton pressed delegate properties declarations here
    func btnAddClicked()
    {
        self.viewForChoseResumeUI()
    }
    
    func btnCircleClicked()
    {
        
    }
    
    func btnBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    func btncreateNewRPressed()
    {
        let NRCS=NewRCreateScreen(nibName: "NewRCreateScreen", bundle: nil)
        self.navigationController?.pushViewController(NRCS, animated: true)
        
    }
    //MARK: UIButton popUp Pressed
    func btnClosePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForChooseResume .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        frameViewForHelp=self.viewForChooseResume.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForChooseResume.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForChooseResume.frame=frameViewForHelp
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView==tblNotifications {
           return 5
        }
        else
        {
            return array_list.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        let subView=UIView(frame: CGRect(x: 0, y: 10, width: Constant.GlobalConstants.screenWidth, height: 50))
        subView.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.7)
        cell.contentView.addSubview(subView)
        
        if tableView==tblNotifications
        {
            subView.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: 59)
            let imgProfile = UIImageView()
            subView.backgroundColor=UIColor.init(red: 222/255.0, green: 230/255.0, blue: 236/255.0, alpha: 1)
            imgProfile .removeFromSuperview()
            imgProfile.frame=CGRect(x: 5, y: 25, width: 10, height: 10)
            imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
            imgProfile.backgroundColor = UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            subView.addSubview(imgProfile)
            
            let lblTitle = UILabel()
            lblTitle.frame=CGRect(x: 20, y: 5, width: Constant.GlobalConstants.screenWidth-20, height:50)
            lblTitle.text="A new job has been posted in \n [Homestream Category] Apply Now"
            lblTitle.textColor=UIColor.black
            lblTitle.font=UIFont(name:"Helvetica-Light", size: 14)
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            subView.addSubview(lblTitle)
            
            let lblTime = UILabel()
            lblTime.frame=CGRect(x: subView.frame.size.width-40, y: 0, width: 40, height: 25)
            lblTime.text="1.35 pm"
            lblTime.textAlignment = .left
            lblTime.font=UIFont(name:"Helvetica-Light", size: 10)
            lblTime.textColor=UIColor.lightGray
            subView.addSubview(lblTime)
        }
        else
        {
            let imgList=UIImageView(frame: CGRect(x: 10, y: 5, width: 33, height: 40))
            imgList.image=UIImage(named:"icon_CResume")
            imgList.backgroundColor=UIColor.clear
            subView.addSubview(imgList)
        
            let lblTitle=UILabel(frame: CGRect(x: imgList.frame.maxX+10, y: 10, width: Constant.GlobalConstants.screenWidth-20, height: 30))
            lblTitle.text=array_list.object(at: indexPath.row) as? String
            lblTitle.backgroundColor=UIColor.clear
            lblTitle.textColor = UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 18)
            subView.addSubview(lblTitle)
        
            let imgNext=UIImageView(frame: CGRect(x: Constant.GlobalConstants.screenWidth-20, y: subView.frame.size.height/2-7.5, width: 10, height: 15))
            imgNext.image=UIImage(named:"icon_SkipA")
            imgNext.backgroundColor=UIColor.clear
            subView.addSubview(imgNext)
        }
        cell.backgroundColor = .clear
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
      //MARK: ViewForHelpView
    func viewForChoseResumeUI()
    {
        viewForChooseResume=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForChooseResume.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForChooseResume)
        
        var frame1 = CGRect()
        frame1=viewForChooseResume.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForChooseResume.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let btnPict = UIButton()
                        let btnDropBox = UIButton()
                        let btnGDrive = UIButton()
                        let btnPDF = UIButton()
                        let btnCNew = UIButton()
                        let lblTitle = UILabel()
                        let lblPict = UILabel()
                        let lblPDF = UILabel()
                        let lblGDrive = UILabel()
                        let lblDBox = UILabel()
                        let lblCretNew = UILabel()
                        
                        viewforTip.frame=CGRect(x: 20, y:Constant.GlobalConstants.screenHeight/2-100
                            , width: Constant.GlobalConstants.screenWidth-40, height: 200)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        
                        self.viewForChooseResume.addSubview(viewforTip)
                        
                        lblTitle.frame=CGRect(x: 0, y: 0, width: viewforTip.frame.size.width, height: 30)
                        lblTitle.text="Add Resume"
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.font =  UIFont(name:"Helvetica", size: 18)
                        viewforTip .addSubview(lblTitle)
                        
                        btnClose.frame=CGRect(x: viewforTip.frame.size.width-30, y: 5, width: 25, height: 25)
                        btnClose.setBackgroundImage(UIImage(named: "icon_1_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnClosePressed), for:.touchUpInside)
                        viewforTip.addSubview(btnClose)
                        
                        btnPict .removeFromSuperview()
                        btnPict.frame=CGRect(x: 25, y: lblTitle.frame.maxY+10, width: 40, height: 35)
                        btnPict.setImage(UIImage(named:"img_Picture"), for: .normal)
                        btnPict.backgroundColor = .clear
                        viewforTip.addSubview(btnPict)

                        btnPDF .removeFromSuperview()
                        btnPDF.frame=CGRect(x: viewforTip.frame.size.width/2-27, y: lblTitle.frame.maxY+10, width: 40, height: 40)
                        btnPDF.setImage(UIImage(named:"icon_PDF"), for: .normal)
                        btnPDF.backgroundColor = .clear
                        viewforTip.addSubview(btnPDF)
                        
                        btnGDrive .removeFromSuperview()
                        btnGDrive.frame=CGRect(x: viewforTip.frame.size.width-68, y: lblTitle.frame.maxY+10, width: 40, height: 40)
                        btnGDrive.setImage(UIImage(named:"icon_GDrive"), for: .normal)
                        btnGDrive.backgroundColor = .clear
                        viewforTip.addSubview(btnGDrive)
                        
                        btnDropBox.removeFromSuperview()
                        btnDropBox.frame=CGRect(x: viewforTip.frame.size.width/2-50, y: btnGDrive.frame.maxY+27, width: 35, height: 35)
                        btnDropBox.setImage(UIImage(named:"icon_DrpBox"), for: .normal)
                        btnDropBox.backgroundColor = .clear
                        viewforTip.addSubview(btnDropBox)
                        
                        btnCNew .removeFromSuperview()
                        btnCNew.frame=CGRect(x: viewforTip.frame.size.width/2+30, y: btnGDrive.frame.maxY+27, width: 30, height: 35)
                        btnCNew.setImage(UIImage(named:"icon_B_Frame"), for: .normal)
                        btnCNew.addTarget(self, action:#selector(self.btncreateNewRPressed), for:.touchUpInside)
                        btnCNew.backgroundColor = .clear
                        viewforTip.addSubview(btnCNew)
                        
                        let imgAdd=UIImageView()
                        imgAdd.frame=CGRect(x: btnCNew.frame.size.width/2-10, y: btnCNew.frame.size.height/2-10, width: 20, height: 20)
                        imgAdd.image = UIImage(named:"icon_P+")
                        btnCNew.addSubview(imgAdd)
                        
                        lblPict.frame=CGRect(x: 0, y: btnPict.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblPict.text="Picture"
                        lblPict.textAlignment=NSTextAlignment.center
                        lblPict.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip.addSubview(lblPict)
                        
                        lblPDF.frame=CGRect(x: lblPict.frame.maxX, y: btnPict.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblPDF.text="PDF"
                        lblPDF.textAlignment=NSTextAlignment.center
                        lblPDF.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip.addSubview(lblPDF)
                        
                        lblGDrive.frame=CGRect(x: lblPDF.frame.maxX, y: btnPict.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblGDrive.text="Drive"
                        lblGDrive.textAlignment=NSTextAlignment.center
                        lblGDrive.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip.addSubview(lblGDrive)
                        
                        lblDBox.frame=CGRect(x: btnDropBox.frame.maxX-50, y: btnDropBox.frame.maxY, width: 75, height: 20)
                        lblDBox.text="DropBox"
                        lblDBox.textAlignment=NSTextAlignment.center
                        lblDBox.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip.addSubview(lblDBox)
                        
                        lblCretNew.frame=CGRect(x: btnCNew.frame.maxX-55, y: btnCNew.frame.maxY, width: 80, height: 20)
                        lblCretNew.text="Create New"
                        lblCretNew.textAlignment=NSTextAlignment.center
                        lblCretNew.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip.addSubview(lblCretNew)
        })
    }

    //MARK: ViewFor Bottom NotificationUI
    func viewforNotificationUI()
    {
        if isopen
        {
            isopen=false
            var frameViewForHelp = CGRect()
            frameViewForHelp=self.viewForNotification.frame;
            frameViewForHelp=CGRect(x: 0,y: Constant.GlobalConstants.screenHeight,width: Constant.GlobalConstants.screenWidth,height: 0);
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseOut,
                           animations: {
                            self.viewForNotification.frame=frameViewForHelp
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                            
                            
            },completion: { finished in
                self.viewForNotification.frame=frameViewForHelp
                self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                self.imgDot.frame=CGRect(x: self.btnBtmNotification.frame.size.width/2-5, y: 10, width: 10, height: 10)
                self.tblNotifications .removeFromSuperview()
                self.lblNoti.removeFromSuperview()
                self.btnNotiSetting.removeFromSuperview()
            })
        }
        else
        {
            isopen=true
            self.viewForNotification.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
            self.viewForNotification.backgroundColor=UIColor.clear
            var frame1 = CGRect()
            
            frame1=viewForNotification.frame
            frame1=CGRect(x:0,y:self.view.frame.size.height/2,width:self.view.frame.size.width ,height: self.view.frame.size.height/2)
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseIn,
                           animations: {
                            self.viewForNotification.frame=frame1
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight/2-40, width:Constant.GlobalConstants.screenWidth, height: 25)
                            
            },completion: { finished in
                
                self.tblNotifications .removeFromSuperview()
                self.tblNotifications.frame=CGRect(x: 0, y: 0, width: self.viewForNotification.frame.size.width, height: self.viewForNotification.frame.size.height)
                self.tblNotifications.dataSource=self
                self.tblNotifications.delegate=self
                self.tblNotifications.tableFooterView=UIView()
                self.tblNotifications.backgroundColor=UIColor.clear
                self.viewForNotification.addSubview(self.tblNotifications)
            })
            
            self.view.addSubview(viewForNotification)
        }
    }
}
