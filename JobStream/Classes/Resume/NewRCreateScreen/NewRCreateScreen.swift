//
//  NewRCreateScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 02/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class NewRCreateScreen: UIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    
    var app=UIApplication.shared.delegate as! AppDelegate
    
    var bgView:UIImageView!
    var topView:UIView!
    var btnBack = UIButton()
    var btnQuestion = UIButton()
    var btnSave = UIButton()
    var btnPreview = UIButton()
    var lblHeading:UILabel!
    var scrollDetails:UIScrollView!
    var txtResumeName:UITextField!
    var viewForAlert = UIView()
    
    //************ Personal View button ********
    var btnPersonalDetail:UIButton!
    var personalDetailView=UIView()
    var imgPersonalDetail:UIImageView!
    var txtName:UITextField!
    var txtPhone:UITextField!
    var txtEmail:UITextField!
    var txtAddress:UITextField!
    
    var isPersonal : Bool!
    
    //************ Objective button ********
    var btnObjective:UIButton!
    var objectiveView=UIView()
    var imgObjective:UIImageView!
    var lblSelect:UILabel!
    var tblObjective:UITableView!
    var subView:UIView!
    var lblHeight:CGFloat!
    
    var isObjective: Bool!
    
    //************ Work Experience button ********
    var btnWorkExperience:UIButton!
    var workExperienceView=UIView()
    var imgWorkExperience:UIImageView!
    var txtDate:UITextField!
    var txtCompany:UITextField!
    var txtJobTitle:UITextField!
    var txtCity:UITextField!
    var txtWorkDetails:UITextView!
    
    var isWorkExperience:Bool!
    
    var imgBottom = UIImageView()
    
    //************ Education button ********
    var btnEducation:UIButton!
    var educationView=UIView()
    var imgEducation:UIImageView!
    var txtEducationDate:UITextField!
    var txtSchool:UITextField!
    var txtDegree:UITextField!
    var txtEducationCity:UITextField!
    var txtEducationDetails:UITextView!
    
    var isEducation :Bool!
    
    //************ Interests button ********
    var btnInterests:UIButton!
    var interestsView=UIView()
    var imgInterests:UIImageView!
    
    var isInterests : Bool!
    
    //************ Skills button ********
    var btnSkills:UIButton!
    var skillsView=UIView()
    var imgSkills:UIImageView!
    var txtSkills:UITextField!
    
    var isSkills : Bool!
    
    //************ References button ********
    var btnReferences:UIButton!
    var referencesView=UIView()
    var viewForResumeInfo = UIView()
    var imgReferences:UIImageView!
    
    var isReferences : Bool!
    
    //************ Additional Documents button ********
    var btnAdditional:UIButton!
    var additionalView=UIView()
    var imgAdditional:UIImageView!
    var lblAdditionalSelect:UILabel!
    var lblAdditionalRef:UILabel!
    var btnAddRef:UIButton!
    
    var isAdditional : Bool!
    
    var strTitle=""
    var strDesc=""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden=true
        
        loadInitialUI()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: Load InitialUI
    func loadInitialUI()
    {
        isPersonal=false
        isObjective=false
        isWorkExperience=false
        isEducation=false
        isInterests=false
        isSkills=false
        isReferences=false
        isAdditional=false
        
        topView=UIView(frame:CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: 50))
        topView.backgroundColor = .clear
        self.view.addSubview(topView)
        
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: UIControlEvents.touchUpInside)
        btnBack.backgroundColor=UIColor.clear
        topView.addSubview(btnBack)
        
        lblHeading=UILabel(frame: CGRect(x: btnBack.frame.maxX+10, y: 0, width: Constant.GlobalConstants.screenWidth-70, height: 30))
        lblHeading.text="New Resume"
        lblHeading.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblHeading.font=UIFont(name: "Helvetica", size: 22)
        lblHeading.textAlignment = .center
        topView.addSubview(lblHeading)
        
        btnQuestion=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth-30, y: 10, width: 20, height: 20))
        btnQuestion.setImage(UIImage(named:"icon_Help"), for: UIControlState.normal)
        btnQuestion.addTarget(self, action: #selector(btnQuestionClicked), for: UIControlEvents.touchUpInside)
        btnQuestion.backgroundColor=UIColor.clear
        topView.addSubview(btnQuestion)
        
        scrollDetails=UIScrollView(frame: CGRect(x: 0, y: topView.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-110))
        scrollDetails.isUserInteractionEnabled=true
        scrollDetails.backgroundColor=UIColor.clear
        scrollDetails.contentSize = CGSize(width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight+30)
        scrollDetails.delegate=self;
        self.view.addSubview(scrollDetails)
        
        txtResumeName=UITextField(frame: CGRect(x:0, y:0, width: Constant.GlobalConstants.screenWidth, height: 40))
        txtResumeName.backgroundColor=UIColor.white.withAlphaComponent(0.7)
        let paddStreet5=UIView(frame: CGRect(x:0, y: 0, width:0, height: 40))
        txtResumeName.leftView=paddStreet5
        txtResumeName.leftViewMode=UITextFieldViewMode.always
        txtResumeName.addSubview(paddStreet5)
        txtResumeName.attributedPlaceholder = NSAttributedString(string:"  Resume Name", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        txtResumeName.delegate=self
        scrollDetails.addSubview(txtResumeName)
        
        btnPersonalDetail=UIButton(frame: CGRect(x:0, y: txtResumeName.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnPersonalDetail.setTitle("  Personal Details", for: .normal)
        btnPersonalDetail.setTitleColor(UIColor.black, for: .normal)
        btnPersonalDetail.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnPersonalDetail.addTarget(self, action: #selector(btnPersonalDetailClicked), for: UIControlEvents.touchUpInside)
        btnPersonalDetail.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnPersonalDetail.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,205)
        btnPersonalDetail.contentHorizontalAlignment = .left;
        imgPersonalDetail=UIImageView(frame: CGRect(x: btnPersonalDetail.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
        btnPersonalDetail.addSubview(imgPersonalDetail)
        scrollDetails.addSubview(btnPersonalDetail)
        
        //Mark:Objective button
        
        btnObjective=UIButton(frame: CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnObjective.setTitle("  Objective", for: .normal)
        btnObjective.setTitleColor(UIColor.black, for: .normal)
        btnObjective.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnObjective.addTarget(self, action: #selector(btnObjectiveClicked), for: UIControlEvents.touchUpInside)
        btnObjective.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnObjective.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,270)
        btnObjective.contentHorizontalAlignment = .left;
        imgObjective=UIImageView(frame: CGRect(x: btnPersonalDetail.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgObjective.image=UIImage(named: "right-arrow (1)")
        btnObjective.addSubview(imgObjective)
        
        let imgQuestion=UIImageView(frame: CGRect(x: 95, y: 7.5, width: 25, height: 25))
        imgQuestion.image=UIImage(named: "ic_Question")
        btnObjective.addSubview(imgQuestion)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewForResumeHelpUI))
        imgQuestion.addGestureRecognizer(tap)
        imgQuestion.isUserInteractionEnabled = true
        
        
        scrollDetails.addSubview(btnObjective)
        
        //Mark:WorkExperience button
        
        btnWorkExperience=UIButton(frame: CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnWorkExperience.setTitle("  Work Experience", for: .normal)
        btnWorkExperience.setTitleColor(UIColor.black, for: .normal)
        btnWorkExperience.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnWorkExperience.addTarget(self, action: #selector(btnWorkExperienceClicked), for: UIControlEvents.touchUpInside)
        btnWorkExperience.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnWorkExperience.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,210)
        btnWorkExperience.contentHorizontalAlignment = .left;
        imgWorkExperience=UIImageView(frame: CGRect(x: btnWorkExperience.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgWorkExperience.image=UIImage(named: "right-arrow (1)")
        btnWorkExperience.addSubview(imgWorkExperience)
        
        let imgQuestion2=UIImageView(frame: CGRect(x: 158, y: 7.5, width: 25, height: 25))
        imgQuestion2.image=UIImage(named: "ic_Question")
        btnWorkExperience.addSubview(imgQuestion2)
        scrollDetails.addSubview(btnWorkExperience)
       
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpWorkExp))
        imgQuestion2.addGestureRecognizer(tap1)
        imgQuestion2.isUserInteractionEnabled = true
        
        //Mark:Education button
        
        btnEducation=UIButton(frame: CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnEducation.setTitle("  Education", for: .normal)
        btnEducation.setTitleColor(UIColor.black, for: .normal)
        btnEducation.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnEducation.addTarget(self, action: #selector(btnEducationClicked), for: UIControlEvents.touchUpInside)
        btnEducation.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnEducation.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,260)
        btnEducation.contentHorizontalAlignment = .left;
        imgEducation=UIImageView(frame: CGRect(x: btnEducation.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgEducation.image=UIImage(named: "right-arrow (1)")
        btnEducation.addSubview(imgEducation)
        
        let imgQuestion3=UIImageView(frame: CGRect(x: 100, y: 7.5, width: 25, height: 25))
        imgQuestion3.image=UIImage(named: "ic_Question")
        btnEducation.addSubview(imgQuestion3)
        scrollDetails.addSubview(btnEducation)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpEducation))
        imgQuestion3.addGestureRecognizer(tap3)
        imgQuestion3.isUserInteractionEnabled = true
        
        //Mark:Interest button
        
        btnInterests=UIButton(frame: CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnInterests.setTitle("  Interests", for: .normal)
        btnInterests.setTitleColor(UIColor.black, for: .normal)
        btnInterests.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnInterests.addTarget(self, action: #selector(btnInterestsClicked), for: UIControlEvents.touchUpInside)
        btnInterests.backgroundColor=UIColor.white.withAlphaComponent(0.8)
       // btnInterests.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,270)
        btnInterests.contentHorizontalAlignment = .left;
        imgInterests=UIImageView(frame: CGRect(x: btnInterests.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgInterests.image=UIImage(named: "right-arrow (1)")
        btnInterests.addSubview(imgInterests)
        
        let imgQuestion4=UIImageView(frame: CGRect(x: 90, y: 7.5, width: 25, height: 25))
        imgQuestion4.image=UIImage(named: "ic_Question")
        btnInterests.addSubview(imgQuestion4)
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpInterests))
        imgQuestion4.addGestureRecognizer(tap4)
        imgQuestion4.isUserInteractionEnabled = true
        
        scrollDetails.addSubview(btnInterests)
        
        //Mark:Skills button
        
        btnSkills=UIButton(frame: CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnSkills.setTitle("  Skills", for: .normal)
        btnSkills.setTitleColor(UIColor.black, for: .normal)
        btnSkills.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnSkills.addTarget(self, action: #selector(btnSkillsClicked), for: UIControlEvents.touchUpInside)
        btnSkills.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnSkills.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,295)
        btnSkills.contentHorizontalAlignment = .left;
        imgSkills=UIImageView(frame: CGRect(x: btnSkills.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgSkills.image=UIImage(named: "right-arrow (1)")
        btnSkills.addSubview(imgSkills)
        
        let imgQuestion5=UIImageView(frame: CGRect(x: 60, y: 7.5, width: 25, height: 25))
        imgQuestion5.image=UIImage(named: "ic_Question")
        btnSkills.addSubview(imgQuestion5)
        
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpSkills))
        imgQuestion5.addGestureRecognizer(tap5)
        imgQuestion5.isUserInteractionEnabled = true
        
        //Mark:References button
        
        btnReferences=UIButton(frame: CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnReferences.setTitle("  References", for: .normal)
        btnReferences.setTitleColor(UIColor.black, for: .normal)
        btnReferences.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnReferences.addTarget(self, action: #selector(btnReferencesClicked), for: UIControlEvents.touchUpInside)
        btnReferences.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //btnReferences.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,250)
        btnReferences.contentHorizontalAlignment = .left;
        imgReferences=UIImageView(frame: CGRect(x: btnReferences.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgReferences.image=UIImage(named: "right-arrow (1)")
        btnReferences.addSubview(imgReferences)
        
        let imgQuestion6=UIImageView(frame: CGRect(x: 110, y: 7.5, width: 25, height: 25))
        imgQuestion6.image=UIImage(named: "ic_Question")
        btnReferences.addSubview(imgQuestion6)
        scrollDetails.addSubview(btnReferences)
        
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpReferences))
        imgQuestion6.addGestureRecognizer(tap6)
        imgQuestion6.isUserInteractionEnabled = true
       
        //Mark:Additional button
        
        btnAdditional=UIButton(frame: CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40))
        btnAdditional.setTitle("  Additional Documents", for: .normal)
        btnAdditional.setTitleColor(UIColor.black, for: .normal)
        btnAdditional.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnAdditional.addTarget(self, action: #selector(btnAdditionalClicked), for: UIControlEvents.touchUpInside)
        btnAdditional.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        // btnAdditional.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,170)
        btnAdditional.contentHorizontalAlignment = .left;
        imgAdditional=UIImageView(frame: CGRect(x: btnAdditional.frame.size.width-35, y: 7.5, width: 30, height: 25))
        imgAdditional.image=UIImage(named: "right-arrow (1)")
        btnAdditional.addSubview(imgAdditional)
        
        let imgQuestion7=UIImageView(frame: CGRect(x: 190, y: 7.5, width: 25, height: 25))
        imgQuestion7.image=UIImage(named: "ic_Question")
        btnAdditional.addSubview(imgQuestion7)
        scrollDetails.addSubview(btnAdditional)
        
        let tap7 = UITapGestureRecognizer(target: self, action: #selector(self.viwHelpAdditional))
        imgQuestion7.addGestureRecognizer(tap7)
        imgQuestion7.isUserInteractionEnabled = true
        
        imgBottom.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-60, width: Constant.GlobalConstants.screenWidth, height: 60)
        imgBottom.backgroundColor=UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view .addSubview(imgBottom)
        
        btnPreview.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-70, y: Constant.GlobalConstants.screenHeight-55, width: 35, height: 35)
        btnPreview.setImage(UIImage(named:"icon_Preview"), for: UIControlState.normal)
        btnPreview.addTarget(self, action: #selector(btnPreviewPressed), for: UIControlEvents.touchUpInside)
        btnPreview.backgroundColor=UIColor.clear
        self.view.addSubview(btnPreview)
        imgBottom .bringSubview(toFront: btnPreview)
        
        let lblPreview = UILabel()
        lblPreview.frame=CGRect(x: 20, y: btnPreview.frame.maxY, width: Constant.GlobalConstants.screenWidth/2-40, height: 20)
        lblPreview.text="Preview"
        lblPreview.textAlignment = .right
        lblPreview.font=UIFont(name: "Helvetica-Light", size: 14)
        lblPreview.textColor=UIColor.white
        self.view.addSubview(lblPreview)
        
        btnSave.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2+25, y: Constant.GlobalConstants.screenHeight-55, width: 30, height: 35)
        btnSave.setImage(UIImage(named:"icon_Save"), for: UIControlState.normal)
        btnSave.addTarget(self, action: #selector(btnSavePressed), for: UIControlEvents.touchUpInside)
        btnSave.backgroundColor=UIColor.clear
        self.view.addSubview(btnSave)
        imgBottom .bringSubview(toFront: btnSave)
        
        let lblSave = UILabel()
        lblSave.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2+22, y: btnSave.frame.maxY, width: 100, height: 20)
        lblSave.text="Save"
        lblSave.textAlignment = .left
        lblSave.textColor=UIColor.white
        lblSave.font=UIFont(name: "Helvetica-Light", size: 14)        
        self.view.addSubview(lblSave)
    }
    
    //MARK: Button Pressed delegate here
    
    func btnPreviewPressed()
    {
        let PRS=PreviewResumeScreen(nibName: "PreviewResumeScreen", bundle: nil)
        self.navigationController?.pushViewController(PRS, animated: true)
    }
    
    func btnSavePressed()
    {
        viewForAlertUI()
    }
   
    
    func viwHelpWorkExp()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    func viwHelpObjective()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    
    func viwHelpEducation()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    func viwHelpInterests()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    func viwHelpSkills()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    
    func viwHelpReferences()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    func viwHelpAdditional()
    {
        strTitle=""
        strDesc=""
        viewForResumeHelpUI()
    }
    
    //MARK: Button Personal pressed
    func btnPersonalDetailClicked()
    {
        if isPersonal==true
        {
            isPersonal=false
            personalDetailView.removeFromSuperview()
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame = CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame = CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
        }
        else
        {
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isInterests=false
            isSkills=false
            isReferences=false
            isAdditional=false
            
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isPersonal=true
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            
            personalDetailView=UIView(frame: CGRect(x: 0, y: btnPersonalDetail.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:130))
            personalDetailView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            personalDetailView.layer.borderWidth=1
            personalDetailView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(personalDetailView)
            
            btnObjective.frame = CGRect(x:0, y: personalDetailView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame = CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
            txtName=UITextField(frame: CGRect(x:0, y:0, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtName.backgroundColor=UIColor.clear
            let paddName=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtName.leftView=paddName
            txtName.leftViewMode=UITextFieldViewMode.always
            txtName.addSubview(paddName)
            txtName.attributedPlaceholder = NSAttributedString(string:"Name", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            personalDetailView.addSubview(txtName)
            
            let lineName=UIView(frame: CGRect(x: 10, y: txtName.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineName.backgroundColor = .black
            personalDetailView.addSubview(lineName)
            
            txtPhone=UITextField(frame: CGRect(x:0, y:lineName.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtPhone.backgroundColor=UIColor.clear
            let paddPhone=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtPhone.leftView=paddPhone
            txtPhone.leftViewMode=UITextFieldViewMode.always
            txtPhone.addSubview(paddPhone)
            txtPhone.attributedPlaceholder = NSAttributedString(string:"Phone Number", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            personalDetailView.addSubview(txtPhone)
            
            let linePhone=UIView(frame: CGRect(x: 10, y: txtPhone.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            linePhone.backgroundColor = .black
            personalDetailView.addSubview(linePhone)
            
            txtEmail=UITextField(frame: CGRect(x:0, y:linePhone.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtEmail.backgroundColor=UIColor.clear
            let paddEmail=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtEmail.leftView=paddEmail
            txtEmail.leftViewMode=UITextFieldViewMode.always
            txtEmail.addSubview(paddEmail)
            txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            personalDetailView.addSubview(txtEmail)
            
            let lineEmail=UIView(frame: CGRect(x: 10, y: txtEmail.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineEmail.backgroundColor = .black
            personalDetailView.addSubview(lineEmail)
            
            txtAddress=UITextField(frame: CGRect(x:0, y:lineEmail.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtAddress.backgroundColor=UIColor.clear
            let paddAddress=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtAddress.leftView=paddAddress
            txtAddress.leftViewMode=UITextFieldViewMode.always
            txtAddress.addSubview(paddAddress)
            txtAddress.attributedPlaceholder = NSAttributedString(string:"Address(Optional)", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            personalDetailView.addSubview(txtAddress)
            
            let lineAddress=UIView(frame: CGRect(x: 10, y: txtAddress.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineAddress.backgroundColor = .black
            personalDetailView.addSubview(lineAddress)
            
            txtName.autocorrectionType = .no
            txtPhone.autocorrectionType = .no
            txtEmail.autocorrectionType = .no
            txtAddress.autocorrectionType = .no
            
            txtName.delegate = self
            txtPhone.delegate = self
            txtEmail.delegate = self
            txtAddress.delegate = self
        }
    }
    
    //MARK: Button Objective pressed
    
    func btnObjectiveClicked()
    {
        
        if isObjective==true
        {
            isObjective=false
            imgObjective.image=UIImage(named: "right-arrow (1)")
            objectiveView.removeFromSuperview()
            
            btnWorkExperience.frame = CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
        }
        else
        {
            isPersonal=false
            isWorkExperience=false
            isEducation=false
            isInterests=false
            isSkills=false
            isReferences=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isObjective=true
            imgObjective.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame = CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            objectiveView=UIView(frame: CGRect(x: 0, y: btnObjective.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:350))
            objectiveView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            objectiveView.layer.borderWidth=1
            objectiveView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(objectiveView)
            
            
            
            btnWorkExperience.frame = CGRect(x:0, y: objectiveView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
            lblSelect=UILabel(frame: CGRect(x: 0 , y: 0, width: Constant.GlobalConstants.screenWidth, height: 20))
            lblSelect.text="Select statement that apply to you or create your own"
            lblSelect.textColor=UIColor.black
            lblSelect.font=UIFont(name: "Helvetica-Light", size: 14)
            lblSelect.textAlignment = .center
            objectiveView.addSubview(lblSelect)
            
            let lineSelect=UIView(frame: CGRect(x: 5, y: lblSelect.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-10, height: 1))
            lineSelect.backgroundColor = .black
            objectiveView.addSubview(lineSelect)
            
            tblObjective=UITableView(frame: CGRect(x: 0, y: lineSelect.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 310))
            tblObjective.backgroundColor=UIColor.clear
            tblObjective.delegate=self;
            tblObjective.dataSource=self;
            tblObjective.separatorStyle=UITableViewCellSeparatorStyle .none
            objectiveView.addSubview(tblObjective)
        }
    }
    
    //MARK: Button Work Experience pressed
    func btnWorkExperienceClicked()
    {
        
        if isWorkExperience==true
        {
            
            isWorkExperience=false
            workExperienceView.removeFromSuperview()
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            
            btnWorkExperience.frame = CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame=CGRect(x: 0, y: btnWorkExperience.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame=CGRect(x: 0, y: btnEducation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame=CGRect(x: 0, y: btnInterests.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame=CGRect(x: 0, y: btnSkills.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame=CGRect(x: 0, y: btnReferences.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            isPersonal=false
            isObjective=false
            isEducation=false
            isInterests=false
            isSkills=false
            isReferences=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            
            isWorkExperience=true
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame = CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame = CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
            workExperienceView=UIView(frame: CGRect(x: 0, y: btnWorkExperience.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:250))
            workExperienceView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            workExperienceView.layer.borderWidth=1
            workExperienceView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(workExperienceView)
            
            btnEducation.frame = CGRect(x:0, y: workExperienceView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            txtDate=UITextField(frame: CGRect(x:0, y:0, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtDate.backgroundColor=UIColor.clear
            let paddName=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtDate.leftView=paddName
            txtDate.leftViewMode=UITextFieldViewMode.always
            txtDate.addSubview(paddName)
            txtDate.attributedPlaceholder = NSAttributedString(string:"Date", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            workExperienceView.addSubview(txtDate)
            
            let lineDate=UIView(frame: CGRect(x: 10, y: txtDate.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineDate.backgroundColor = .black
            workExperienceView.addSubview(lineDate)
            
            txtCompany=UITextField(frame: CGRect(x:0, y:lineDate.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtCompany.backgroundColor=UIColor.clear
            let paddCompany=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtCompany.leftView=paddCompany
            txtCompany.leftViewMode=UITextFieldViewMode.always
            txtCompany.addSubview(paddCompany)
            txtCompany.attributedPlaceholder = NSAttributedString(string:"Company", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            workExperienceView.addSubview(txtCompany)
            
            let lineCompany=UIView(frame: CGRect(x: 10, y: txtCompany.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineCompany.backgroundColor = .black
            workExperienceView.addSubview(lineCompany)
            
            txtJobTitle=UITextField(frame: CGRect(x:0, y:lineCompany.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtJobTitle.backgroundColor=UIColor.clear
            let paddJobTitle=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtJobTitle.leftView=paddJobTitle
            txtJobTitle.leftViewMode=UITextFieldViewMode.always
            txtJobTitle.addSubview(paddJobTitle)
            txtJobTitle.attributedPlaceholder = NSAttributedString(string:"JobTitle", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            workExperienceView.addSubview(txtJobTitle)
            
            let lineJobTitle=UIView(frame: CGRect(x: 10, y: txtJobTitle.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineJobTitle.backgroundColor = .black
            workExperienceView.addSubview(lineJobTitle)
            
            txtCity=UITextField(frame: CGRect(x:0, y:lineJobTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtCity.backgroundColor=UIColor.clear
            let paddCity=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtCity.leftView=paddCity
            txtCity.leftViewMode=UITextFieldViewMode.always
            txtCity.addSubview(paddCity)
            txtCity.attributedPlaceholder = NSAttributedString(string:"City/State", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            workExperienceView.addSubview(txtCity)
            
            let lineCity=UIView(frame: CGRect(x: 10, y: txtCity.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineCity.backgroundColor = .black
            workExperienceView.addSubview(lineCity)
            
            txtWorkDetails=UITextView(frame: CGRect(x: 10, y: lineCity.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-20, height: 100))
            txtWorkDetails.backgroundColor = .white
            txtWorkDetails.text="Details of Position"
            txtWorkDetails.textColor = .black
            txtWorkDetails.font=UIFont(name: "Helvetica-Light", size: 17)
            workExperienceView.addSubview(txtWorkDetails)
            
            txtDate.autocorrectionType = .no
            txtCompany.autocorrectionType = .no
            txtJobTitle.autocorrectionType = .no
            txtCity.autocorrectionType = .no
            
            txtDate.delegate=self
            txtCompany.delegate=self
            txtJobTitle.delegate=self
            txtCity.delegate=self
        }
    }
    
    //MARK: Button Education pressed
    func btnEducationClicked()
    {
        
        if isEducation==true
        {
            
            isEducation=false
            educationView.removeFromSuperview()
            imgEducation.image=UIImage(named: "right-arrow (1)")
            
            btnEducation.frame=CGRect(x: 0, y: btnWorkExperience.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame=CGRect(x: 0, y: btnEducation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame=CGRect(x: 0, y: btnInterests.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame=CGRect(x: 0, y: btnSkills.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame=CGRect(x: 0, y: btnReferences.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            isPersonal=false
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isSkills=false
            isReferences=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isEducation=true
            imgEducation.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame=CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame=CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            educationView=UIView(frame: CGRect(x: 0, y: btnEducation.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:250))
            educationView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            educationView.layer.borderWidth=1
            educationView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(educationView)
            
            
            btnInterests.frame = CGRect(x:0, y: educationView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            txtEducationDate=UITextField(frame: CGRect(x:0, y:0, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtEducationDate.backgroundColor=UIColor.clear
            let paddEducationDate=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtEducationDate.leftView=paddEducationDate
            txtEducationDate.leftViewMode=UITextFieldViewMode.always
            txtEducationDate.addSubview(paddEducationDate)
            txtEducationDate.attributedPlaceholder = NSAttributedString(string:"Date", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            educationView.addSubview(txtEducationDate)
            
            let lineEducationDate=UIView(frame: CGRect(x: 10, y: txtEducationDate.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineEducationDate.backgroundColor = .black
            educationView.addSubview(lineEducationDate)
            
            txtSchool=UITextField(frame: CGRect(x:0, y:lineEducationDate.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtSchool.backgroundColor=UIColor.clear
            let paddPhone=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtSchool.leftView=paddPhone
            txtSchool.leftViewMode=UITextFieldViewMode.always
            txtSchool.addSubview(paddPhone)
            txtSchool.attributedPlaceholder = NSAttributedString(string:"School/Institution", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            educationView.addSubview(txtSchool)
            
            let lineSchool=UIView(frame: CGRect(x: 10, y: txtSchool.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineSchool.backgroundColor = .black
            educationView.addSubview(lineSchool)
            
            txtDegree=UITextField(frame: CGRect(x:0, y:lineSchool.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtDegree.backgroundColor=UIColor.clear
            let paddEmail=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtDegree.leftView=paddEmail
            txtDegree.leftViewMode=UITextFieldViewMode.always
            txtDegree.addSubview(paddEmail)
            txtDegree.attributedPlaceholder = NSAttributedString(string:"Degree/Education", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            educationView.addSubview(txtDegree)
            
            let lineDegree=UIView(frame: CGRect(x: 10, y: txtDegree.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineDegree.backgroundColor = .black
            educationView.addSubview(lineDegree)
            
            txtEducationCity=UITextField(frame: CGRect(x:0, y:lineDegree.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtEducationCity.backgroundColor=UIColor.clear
            let paddEducationCity=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtEducationCity.leftView=paddEducationCity
            txtEducationCity.leftViewMode=UITextFieldViewMode.always
            txtEducationCity.addSubview(paddEducationCity)
            txtEducationCity.attributedPlaceholder = NSAttributedString(string:"City/State", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            
            educationView.addSubview(txtEducationCity)
            
            let lineEducationCity=UIView(frame: CGRect(x: 10, y: txtEducationCity.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineEducationCity.backgroundColor = .black
            educationView.addSubview(lineEducationCity)
            
            txtEducationDetails=UITextView(frame: CGRect(x: 10, y: lineEducationCity.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-20, height: 100))
            txtEducationDetails.backgroundColor = .white
            txtEducationDetails.text="Details of Position"
            txtEducationDetails.textColor = .black
            txtEducationDetails.font=UIFont(name: "Helvetica-Light", size: 17)
            educationView.addSubview(txtEducationDetails)
            
            txtDate.delegate=self
            txtSchool.delegate=self
            txtDegree.delegate=self
            txtCity.delegate=self
        }
    }
    
    //MARK: button Interest pressed
    
    func btnInterestsClicked()
    {
        if isInterests==true
        {
            isInterests=false
            interestsView.removeFromSuperview()
            imgInterests.image=UIImage(named: "right-arrow (1)")
            
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            isPersonal=false
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isSkills=false
            isReferences=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isInterests=true
            imgInterests.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame=CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame=CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
            interestsView=UIView(frame: CGRect(x: 0, y: btnInterests.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:100))
            interestsView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            interestsView.layer.borderWidth=1
            interestsView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(interestsView)
            
            btnSkills.frame = CGRect(x:0, y: interestsView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
        }
    }
    
    func btnSkillsClicked()
    {
        if isSkills==true
        {
            
            isSkills=false
            skillsView.removeFromSuperview()
            imgSkills.image=UIImage(named: "right-arrow (1)")
            
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            isPersonal=false
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isInterests=false
            isReferences=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isSkills=true
            imgSkills.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame=CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame=CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            skillsView=UIView(frame: CGRect(x: 0, y: btnSkills.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:40))
            skillsView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            skillsView.layer.borderWidth=1
            skillsView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(skillsView)
            
            btnReferences.frame = CGRect(x:0, y: skillsView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            txtSkills=UITextField(frame: CGRect(x:0, y:0, width: Constant.GlobalConstants.screenWidth, height: 30))
            txtSkills.backgroundColor=UIColor.clear
            let paddSkills=UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
            txtSkills.leftView=paddSkills
            txtSkills.leftViewMode=UITextFieldViewMode.always
            txtSkills.addSubview(paddSkills)
            txtSkills.autocorrectionType = .no
            txtSkills.delegate=self
            
            txtSkills.attributedPlaceholder = NSAttributedString(string:"Enter Your Skills", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 17)!])
            skillsView.addSubview(txtSkills)
            
            let lineSkills=UIView(frame: CGRect(x: 10, y: txtSkills.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-20, height: 1))
            lineSkills.backgroundColor = .black
            skillsView.addSubview(lineSkills)
        }
    }
    
    func btnReferencesClicked()
    {
        if isReferences==true
        {
            
            isReferences=false
            referencesView.removeFromSuperview()
            imgReferences.image=UIImage(named: "right-arrow (1)")
            
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            
            isPersonal=false
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isInterests=false
            isSkills=false
            isAdditional=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            skillsView.removeFromSuperview()
            additionalView.removeFromSuperview()
            
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            isReferences=true
            imgReferences.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame=CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame=CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            referencesView=UIView(frame: CGRect(x: 0, y: btnReferences.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:150))
            referencesView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            referencesView.layer.borderWidth=1
            referencesView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(referencesView)
            
            btnAdditional.frame = CGRect(x:0, y: referencesView.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            
            lblAdditionalSelect=UILabel(frame: CGRect(x: 0 , y: 0, width: Constant.GlobalConstants.screenWidth, height: 20))
            lblAdditionalSelect.text="Please select an Option"
            lblAdditionalSelect.textColor=UIColor.black
            lblAdditionalSelect.font=UIFont(name: "Helvetica-Light", size: 14)
            lblAdditionalSelect.textAlignment = .center
            referencesView.addSubview(lblAdditionalSelect)
            
            let lineAdditionalSelect=UIView(frame: CGRect(x: 5, y: lblAdditionalSelect.frame.maxY+1, width: Constant.GlobalConstants.screenWidth-10, height: 1))
            lineAdditionalSelect.backgroundColor = .black
            referencesView.addSubview(lineAdditionalSelect)
            
            lblAdditionalRef=UILabel(frame: CGRect(x: 10 , y:lineAdditionalSelect.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-20, height: 40))
            lblAdditionalRef.text="References available upon request"
            lblAdditionalRef.textColor=UIColor.white
            lblAdditionalRef.backgroundColor=UIColor.init(red: 61/255.0, green: 103/255.0, blue: 133/255.0, alpha: 1.0)
            lblAdditionalRef.layer.cornerRadius=5.0
            lblAdditionalRef.layer.masksToBounds=true
            lblAdditionalRef.font=UIFont(name: "Helvetica-Light", size: 17)
            lblAdditionalRef.textAlignment = .center
            referencesView.addSubview(lblAdditionalRef)
            
            btnAddRef=UIButton(frame: CGRect(x: 10, y: lblAdditionalRef.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-20, height: 55))
            btnAddRef.backgroundColor=UIColor.white
            btnAddRef.setTitle("Add References", for: .normal)
            btnAddRef.setTitleColor(UIColor.black, for: .normal)
            btnAddRef.titleLabel?.font=UIFont(name: "Helvetica-Light", size: 17)
            btnAddRef.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,200)
            
            let imgAdd=UIImageView(frame: CGRect(x: btnAddRef.frame.size.width-35, y: 15, width: 25, height: 25))
            imgAdd.image=UIImage(named: "plus")
            btnAddRef.addSubview(imgAdd)
            
            referencesView.addSubview(btnAddRef)
            
            
        }
    }
    
    func btnAdditionalClicked()
    {
        if isAdditional==true
        {
            
            isAdditional=false
            additionalView.removeFromSuperview()
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
        }
        else
        {
            
            isPersonal=false
            isObjective=false
            isWorkExperience=false
            isEducation=false
            isInterests=false
            isSkills=false
            isReferences=false
            
            personalDetailView.removeFromSuperview()
            objectiveView.removeFromSuperview()
            workExperienceView.removeFromSuperview()
            educationView.removeFromSuperview()
            interestsView.removeFromSuperview()
            skillsView.removeFromSuperview()
            referencesView.removeFromSuperview()
            
            imgPersonalDetail.image=UIImage(named: "right-arrow (1)")
            imgObjective.image=UIImage(named: "right-arrow (1)")
            imgWorkExperience.image=UIImage(named: "right-arrow (1)")
            imgEducation.image=UIImage(named: "right-arrow (1)")
            imgInterests.image=UIImage(named: "right-arrow (1)")
            imgSkills.image=UIImage(named: "right-arrow (1)")
            imgReferences.image=UIImage(named: "right-arrow (1)")
            
            
            isAdditional=true
            imgAdditional.image=UIImage(named: "right-arrow (1)")
            
            btnObjective.frame=CGRect(x:0, y: btnPersonalDetail.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnWorkExperience.frame=CGRect(x:0, y: btnObjective.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnEducation.frame = CGRect(x:0, y: btnWorkExperience.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnInterests.frame = CGRect(x:0, y: btnEducation.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnSkills.frame = CGRect(x:0, y: btnInterests.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnReferences.frame = CGRect(x:0, y: btnSkills.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            btnAdditional.frame = CGRect(x:0, y: btnReferences.frame.maxY+10, width:Constant.GlobalConstants.screenWidth, height: 40)
            
            additionalView=UIView(frame: CGRect(x: 0, y: btnAdditional.frame.maxY, width: Constant.GlobalConstants.screenWidth, height:100))
            additionalView.backgroundColor=UIColor.white.withAlphaComponent(0.8)
            additionalView.layer.borderWidth=1
            additionalView.layer.borderColor=UIColor.lightGray.cgColor
            scrollDetails.addSubview(additionalView)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        subView=UIView(frame:CGRect(x: 5, y: 10, width: Constant.GlobalConstants.screenWidth-10, height:20))
        subView.backgroundColor=UIColor.white
        subView.clipsToBounds = false
        subView.layer.shadowOpacity=0.4
        subView.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.contentView.addSubview(subView)
        
        
        let lblData=UILabel(frame: CGRect(x: 10, y: 5, width: Constant.GlobalConstants.screenWidth-50, height: 0))
        lblData.text = "A table view displays a list of items in a single column. UITableView is a subclass of UIScrollView, which allows users to scroll through the table, although UITableView allows vertical scrolling only."
        lblData.textColor=UIColor.gray
        lblData.numberOfLines=0
        lblData.font=UIFont(name: "Helvetica", size: 12)
        lblData.lineBreakMode=NSLineBreakMode.byWordWrapping
        lblData.sizeToFit()
        
        //MARK: Here we calculate the label height
        
        let labelWidth = lblData.frame.width
        let maxLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        let actualLabelSize = lblData.text!.boundingRect(with: maxLabelSize, options: [.usesLineFragmentOrigin], attributes: [NSFontAttributeName: lblData.font], context: nil)
        lblHeight = actualLabelSize.height
        
        view.layer.masksToBounds = true;
        subView.addSubview(lblData)
        subView.frame=CGRect(x: 5, y: 5, width: Constant.GlobalConstants.screenWidth-10, height:lblData.frame.maxY+10)
        
        //MARK: plus image added
        
        let imgNext=UIImageView(frame: CGRect(x: Constant.GlobalConstants.screenWidth-50, y: subView.frame.size.height/2-12.5, width: 35, height: 35))
        imgNext.image=UIImage(named: "icon_Circle")
        subView.addSubview(imgNext)
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        cell.backgroundColor = .clear
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if lblHeight == nil
        {
            return 40
        }
        else
        {
            return subView.frame.size.height+10
        }
        
    }
    
    
    func btnQuestionClicked()
    {
        
    }
    
    func btnBackClicked()
    {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: ViewForHelpView
    func viewForAlertUI()
    {
        viewForAlert=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForAlert.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForAlert)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseSavePressed))
        viewForAlert.addGestureRecognizer(tap)
        viewForAlert.isUserInteractionEnabled = true

        var frame1 = CGRect()
        
        frame1=viewForAlert.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-60
                            , width: Constant.GlobalConstants.screenWidth-60, height: 100)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 5, y: 0, width: viewforTip.frame.size.width-10, height: 30)
                        lblTitle.text="Saved as a Draft"
                        lblTitle.font=UIFont(name:"Helvetica", size: 16)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        
                        lblSubTitle.frame=CGRect(x: 5, y: lblTitle.frame.maxY+3, width: viewforTip.frame.size.width, height: 50)
                        lblSubTitle.text="Your resume has been saved as a draft. You \n can continue editing or come back later \n to finish your resume"
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 12)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        lblSubTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblSubTitle.numberOfLines = 0
                        
                        self.viewForAlert.addSubview(viewforTip)
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)
        })
    }
    
    //MARK: ViewForHelpView
    func viewForResumeHelpUI()
    {
        viewForResumeInfo=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForResumeInfo.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForResumeInfo)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseSavePressed))
        viewForResumeInfo.addGestureRecognizer(tap)
        viewForResumeInfo.isUserInteractionEnabled = true
        
        var frame1 = CGRect()
        
        frame1=viewForResumeInfo.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForResumeInfo.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-60
                            , width: Constant.GlobalConstants.screenWidth-60, height: 100)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 5, y: 0, width: viewforTip.frame.size.width-10, height: 30)
                        lblTitle.text="Saved as a Draft"
                        lblTitle.font=UIFont(name:"Helvetica", size: 16)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        
                        lblSubTitle.frame=CGRect(x: 5, y: lblTitle.frame.maxY+3, width: viewforTip.frame.size.width, height: 50)
                        lblSubTitle.text="Your resume has been saved as a draft. You \n can continue editing or come back later \n to finish your resume"
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 12)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        lblSubTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblSubTitle.numberOfLines = 0
                        
                        self.viewForResumeInfo.addSubview(viewforTip)
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)
        })
    }

    //MARK: UIButton Help Pressed
    func btnCloseSavePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForAlert .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForAlert.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForAlert.frame=frameViewForHelp
        })
    }
    


    
}
