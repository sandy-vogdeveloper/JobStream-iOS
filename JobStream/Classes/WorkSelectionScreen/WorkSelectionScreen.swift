//
//  WorkSelectionScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 26/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class WorkSelectionScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var lblTitle: UILabel!
    var tableView: UITableView  =   UITableView()
    let arrType : [String] = ["Bartender","Waitess","Cleaner","Dancer"]
    //var arrImages : [String] = ["icon_Plus","icon_Plus","icon_Plus","icon_Plus"]
    var viewForHelp = UIView()
    var viewForJobTitle = UIView()
    
    var arrCatgryList:NSMutableArray=[]
    var arrCatgryID:NSMutableArray=[]
    
    var strAlertTitle = NSString()
    @IBOutlet var btnHelp: UIButton!
    @IBOutlet var btnHelp1: UIButton!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnMenu1: UIButton!
    var strCatID = NSString()
    var strSelectTitle = NSString()
    var btnDone = UIButton()
    var btnSearch = UIButton()
    var isSubmitList = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadInitialUI()
        isSubmitList=false
        getWorkTypeListAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    //MARK: load InitialUI
    func loadInitialUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblTitle.text = strSelectTitle as String
        btnHelp.addTarget(self, action:#selector(self.viewForHelpView), for:.touchUpInside)
        btnHelp1.addTarget(self, action:#selector(self.viewForHelpView), for:.touchUpInside)
        
        btnSearch.frame=CGRect(x: 40, y: 40, width: Constant.GlobalConstants.screenWidth-80, height: 35)
        btnSearch .setTitle("   Search", for: .normal)
        btnSearch.titleLabel?.font=UIFont(name:"Helvetica", size: 16)
        btnSearch .setTitleColor(UIColor.gray, for: .normal)
        btnSearch.backgroundColor=UIColor.white
        btnSearch.layer.cornerRadius=5.0
        btnSearch.layer.borderColor=UIColor.lightGray.cgColor
        btnSearch.layer.borderWidth=1.0
        
        if Constant.GlobalConstants.screenWidth<325
        {
            btnSearch.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,120)
        }
        else
        {
            btnSearch.titleEdgeInsets=UIEdgeInsetsMake(0, 0, 0,180)
        }
        btnSearch.addTarget(self, action:#selector(self.btnSearchPressed), for: .touchUpInside)
        self.view.addSubview(btnSearch)
        
        let imgSearch = UIImageView()
        imgSearch.frame=CGRect(x: 10, y: 10, width: 15, height: 15)
        imgSearch.image=UIImage.init(named: "icon_search")
        btnSearch.addSubview(imgSearch)
        
        tableView.frame = CGRect(x: 0, y: 85, width: self.view.frame.size.width, height: self.view.frame.size.height-200)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView=UIView()
        tableView.backgroundColor=UIColor.clear
        tableView.isScrollEnabled=false
        self.view.addSubview(tableView)
        
        if Constant.GlobalConstants.theAppDelegate.isPostAJobPressed
        {
        }
        else
        {
            btnDone.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-60, y: Constant.GlobalConstants.screenHeight-40, width: 120, height: 35)
            btnDone .setTitle("Done", for: .normal)
            btnDone.titleLabel?.font=UIFont(name:"Helvetica", size: 16)
            btnDone .setTitleColor(UIColor.white, for: .normal)
            btnDone.layer.cornerRadius=3.0
            btnDone.backgroundColor=UIColor(red: 24.0/255.0, green: 72.0/255.0, blue: 117.0/255.0, alpha: 1.0).withAlphaComponent(0.7)
            btnDone.addTarget(self, action: #selector(self.btnNextPressedPressed), for: .touchUpInside)
        
            self.view.addSubview(btnDone)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrCatgryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        let lblSubTitle=UILabel()
        let btnAdd=UIButton()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tableView.separatorStyle = .none
        
        imgBar.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.8)
        cell.contentView.addSubview(imgBar)
        
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.8)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        
        
        
        lblTitle.frame=CGRect(x: 5,y: 0,width: UIScreen.main.bounds.size.width-50,height:30)
        lblSubTitle.frame=CGRect(x: 5,y: 25,width: UIScreen.main.bounds.size.width-50,height:40)
        btnAdd.frame=CGRect(x: viewForCell.frame.size.width-40, y:17.5, width: 35, height: 35)
        
        var dict = NSDictionary()
        
        dict = arrCatgryList[(indexPath as NSIndexPath).row] as! NSDictionary
        lblTitle.text = dict.value(forKey: "sub_category") as! String?
        
        lblSubTitle.text = "Brif description of what this \n category has"
       
        lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
        lblSubTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)

        
        lblTitle.textAlignment=NSTextAlignment.left
        lblSubTitle.textAlignment=NSTextAlignment.left
        
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 20)
        lblSubTitle.font =  UIFont(name:"Helvetica-Light", size: 14)
        
        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblSubTitle.numberOfLines = 0
        
        btnAdd.tag = indexPath.row
        var strID1 = NSString()
        
        strID1 = dict.value(forKey: "sid") as! NSString
        
        print(arrCatgryID)
        
        viewForCell .addSubview(lblTitle)
        viewForCell .addSubview(lblSubTitle)
        
        if Constant.GlobalConstants.theAppDelegate.isPostAJobPressed
        {
        }
        else
        {
            if arrCatgryID .contains(strID1)
            {
                btnAdd.frame=CGRect(x: viewForCell.frame.size.width-40, y: viewForCell.frame.size.height/2-2.2, width: 35, height: 3)
                btnAdd.setBackgroundImage(UIImage(named: "icon_MinusNew"), for: UIControlState.normal)
                viewForCell.backgroundColor = UIColor.clear
                imgBar.image=UIImage(named: "img_NotiBG")
                
                lblTitle.textColor=UIColor.white
                lblSubTitle.textColor=UIColor.white
            }
            else
            {
                btnAdd.setBackgroundImage(UIImage(named: "icon_PlusNew"), for: UIControlState.normal)
                lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
                lblSubTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
                imgBar.image=UIImage(named: "")
                
                viewForCell.backgroundColor = UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0).withAlphaComponent(0.8)
                
            }
        
            //btnAdd.addTarget(self, action:#selector(self.btnAddJobPressed), for:.touchUpInside)
            viewForCell .addSubview(btnAdd)
        }
            btnAdd.addTarget(self, action:#selector(self.btnPM), for:.touchUpInside)
            
            return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if Constant.GlobalConstants.theAppDelegate.isPostAJobPressed
        {
            var dict = NSDictionary()
            dict = arrCatgryList[(indexPath as NSIndexPath).row] as! NSDictionary
            
            let PJS = PostAJobScreen(nibName: "PostAJobScreen", bundle: nil)
            PJS.strCID=strCatID
            PJS.strSID=dict.value(forKey: "sid") as! NSString
            PJS.strCategory=strSelectTitle
            PJS.strSubCategory=dict.value(forKey: "sub_category") as! NSString
            self.navigationController?.pushViewController(PJS, animated: true)
        }
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    //MARK: UIButton pressed Delegate
    //MARK: Add button pressed
    func btnAddJobPressed(sender: UIButton!)
    {        
        let row = sender.tag
        print(row)
        self.viewForJobTitlePopUp()
    }
    
    func btnSearchPressed(sender: UIButton!)
    {
        let srchScrn=SearchScreen(nibName: "SearchScreen", bundle: nil)
        self.navigationController?.pushViewController(srchScrn, animated: true)
    }
    func btnNextPressedPressed(sender: UIButton!)
    {
        
        //let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
        //self.navigationController?.pushViewController(UMD, animated: true)

        if arrCatgryID.count != 0
        {
            SubmitTypeListAPI()
        }
    }
    
    func btnPM(sender: UIButton!)
    {
        let row = sender.tag
        print(row)
        
        var strImg=NSString()
        var strSelected = NSString()
        var strC_SID = NSString()
        
        //strImg = arrImages [row] as NSString
        
        //print(strImg)
        
        var dict = NSDictionary()
        dict = arrCatgryList[row] as! NSDictionary
        strC_SID = dict.value(forKey: "sid") as! NSString
         strAlertTitle = dict.value(forKey: "sub_category") as! NSString
        
        print(strSelected)
        if arrCatgryID .contains(strC_SID)
        {
            arrCatgryID.remove(strC_SID)
            //arrImages .remove(at: row)
            //arrImages.insert("icon_Minus", at: row)
            
           strSelected=" has been \n removed from your Homestream"
           strAlertTitle = strAlertTitle.appending(strSelected as String) as (String) as NSString
        }
        else
        {
           // arrImages .remove(at: row)
            //arrImages.insert("icon_Plus", at: row)
            arrCatgryID.add(strC_SID)
            
            strSelected=" has been \n added to your Homestream"
            strAlertTitle = strAlertTitle.appending(strSelected as String) as (String) as NSString
        }
        self.tableView.reloadData()
        self.viewForJobTitlePopUp()
    }
    
    func btnCloseHelpViewPressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForHelp .subviews {
            subview .removeFromSuperview()
        }
//        for(UIView *subview in [_viewForPopUp subviews])
//        {
//            [subview removeFromSuperview];
//        }
        var frameViewForHelp = CGRect()
        
         frameViewForHelp=self.viewForHelp.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForHelp.frame=frameViewForHelp
        },completion: { finished in
                       
            self.viewForHelp.frame=frameViewForHelp
        })
    }
    
    func btnCloseJobTitleViewPressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForJobTitle .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForJobTitle.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForJobTitle.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForJobTitle.frame=frameViewForHelp
        })
    }    
    
    //MARK: ViewForHelpView
    func viewForHelpView()
    {
        viewForHelp=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForHelp.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForHelp)
  
        var frame1 = CGRect()
        
        frame1=viewForHelp.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
 
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseHelpViewPressed))
        viewForHelp.addGestureRecognizer(tap)
        viewForHelp.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.8,
                animations: {
    self.viewForHelp.frame=frame1
    },
    completion: { finished in
        
        let viewforTip = UIView()
        let btnClose = UIButton()
        let lblTitle = UILabel()
        let lblSubTitle = UILabel()
        
        btnClose.frame=CGRect(x: self.viewForHelp.frame.size.width-40, y: 2, width: 35, height: 35)
        //btnClose.setBackgroundImage(UIImage(named: "icon_Close"), for: UIControlState.normal)
        btnClose.addTarget(self, action:#selector(self.btnCloseHelpViewPressed), for:.touchUpInside)
        
        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-60
            , width: Constant.GlobalConstants.screenWidth-60, height: 120)
        viewforTip.backgroundColor=UIColor.white
        viewforTip.layer.cornerRadius=10.0
        viewforTip.layer.borderWidth=1.0
        viewforTip.layer.borderColor=UIColor.black.cgColor
        
        lblTitle.frame=CGRect(x: 0, y: 0, width: viewforTip.frame.size.width, height: 30)
        lblTitle.text="Help Tip"
        lblTitle.font=UIFont(name:"Helvetica", size: 20)
        lblTitle.textAlignment=NSTextAlignment.center
        lblTitle.textColor=UIColor(red: 28.0/255.0, green: 71.0/255.0, blue: 144.0/255.0, alpha: 1.0)

        lblSubTitle.frame=CGRect(x: 5, y: lblTitle.frame.maxY, width: viewforTip.frame.size.width, height: 60)
        lblSubTitle.text="Eelect one or more job titles you \n would like to recieve updates on \n newly posted positions"
        lblSubTitle.font=UIFont(name:"Helvetica", size: 16)
        lblSubTitle.textAlignment=NSTextAlignment.center
        lblSubTitle.textColor=UIColor(red: 28.0/255.0, green: 71.0/255.0, blue: 144.0/255.0, alpha: 1.0)
        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblSubTitle.numberOfLines = 0
        
        self.viewForHelp.addSubview(btnClose)
        self.viewForHelp.addSubview(viewforTip)
        viewforTip.addSubview(lblTitle)
        viewforTip.addSubview(lblSubTitle)
    })
    }
    
    //MARK: ViewForHelpView
    func viewForJobTitlePopUp()
    {
        viewForJobTitle=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForJobTitle.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForJobTitle)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseJobTitleViewPressed))
        viewForJobTitle.addGestureRecognizer(tap)
        viewForJobTitle.isUserInteractionEnabled = true
        
        var frame1 = CGRect()
        
        frame1=viewForJobTitle.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForJobTitle.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let lblTitle = UILabel()
                        
                        btnClose.frame=CGRect(x: self.viewForJobTitle.frame.size.width-40, y: 2, width: 35, height: 35)
                        //btnClose.setBackgroundImage(UIImage(named: "icon_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnCloseJobTitleViewPressed), for:.touchUpInside)
                        
                        if Constant.GlobalConstants.screenWidth < 325
                        {
                            viewforTip.frame=CGRect(x: 40, y:Constant.GlobalConstants.screenHeight/2-30
                            , width: Constant.GlobalConstants.screenWidth-80, height: 60)
                        }
                        else
                        {
                            viewforTip.frame=CGRect(x: 55, y:Constant.GlobalConstants.screenHeight/2-30
                                , width: Constant.GlobalConstants.screenWidth-110, height: 60)
                            
                        }
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 0, y: 5, width: viewforTip.frame.size.width, height: 50)
                        lblTitle.text=self.self.strAlertTitle as String
                        lblTitle.font=UIFont(name:"Helvetica", size: 16)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor(red: 28.0/255.0, green: 71.0/255.0, blue: 144.0/255.0, alpha: 1.0)
                        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblTitle.numberOfLines = 0
                        
                        self.viewForJobTitle.addSubview(btnClose)
                        self.viewForJobTitle.addSubview(viewforTip)
                        viewforTip.addSubview(lblTitle)
        })
    }

    
    func SubmitTypeListAPI()
    {
        isSubmitList=true
        var strSID = NSString()
        strSID = arrCatgryID.componentsJoined(by: ",") as NSString
        print(strSID)
        
        let bodydata = NSMutableDictionary()
        
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((strCatID), forKey: "cid" as NSCopying)
        bodydata.setObject((strSID), forKey: "sid" as NSCopying)
        
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsaddusersubcategory.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    //MARK: update user Info
    func getWorkTypeListAPI()
    {
        isSubmitList=false
        let bodydata = NSMutableDictionary()
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsgetsubCategory.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                if isSubmitList
                {
                    let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
                    self.navigationController?.pushViewController(UMD, animated: true)
                }
                else
                {
                    self.arrCatgryList = (ResponceData.value(forKey: "info")as! NSMutableArray)
                    tableView.reloadData()
                }
            }
        }
        catch
        {
            print("Error")
        }
        
    }
}
