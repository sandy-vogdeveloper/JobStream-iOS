//
//  UserSettingScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 29/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class UserSettingScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {
     var btnBack = UIButton()
     var btnBack1 = UIButton()
    @IBOutlet var lblTitle: UILabel!
    
    var viewForNotiSetting = UIView()
    var tblNotiAlert = UITableView()
    var tblList = UITableView()
    
    var app=UIApplication.shared.delegate as! AppDelegate
    var arrFields : [String] = ["When an employer sends a message \n or request","My resume is viewed","A new job is posted on my Homestream"]
    var arrType : [String] = ["Profile","Resume","Notification"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
    
        if app.strSelectedUser == "employee"
        {
            arrFields = ["When an employer sends a message \n or request","My resume is viewed","A new job is posted on my Homestream"]
            arrType = ["Profile","Resume","Notification"]
        }
        else
        {
            arrFields = ["When an employer sends a message \n or request","A new job is posted on my Homestream"]
            arrType =  ["Profile","Notification"]
        }
        
        // Do any additional setup after loading the view.
        loadInitialUI()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: LoadInitial UI
    func loadInitialUI()
    {
        tblList.frame = CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: self.view.frame.size.height-200)
        tblList.dataSource = self
        tblList.delegate = self
        tblList.tableFooterView=UIView()
        tblList.backgroundColor=UIColor.clear
        self.view.addSubview(tblList)
    }

    //MARK: Button pressed delegate declarations here
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    
    func btnCloseNotiViewPressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForNotiSetting .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForNotiSetting.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForNotiSetting.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForNotiSetting.frame=frameViewForHelp
        })
    }

    
    
    //MARK: ViewForNotiSetting
    func viewForNotiSettingUI()
    {
        viewForNotiSetting=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForNotiSetting.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForNotiSetting)
        
        
        var frame1 = CGRect()
        
        frame1=viewForNotiSetting.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForNotiSetting.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let lblTitle = UILabel()
                        let btnEmail = UIButton()
                        let btnSMS = UIButton()
                        let btnApp = UIButton()
                        
                        viewforTip.frame=CGRect(x: 20, y:Constant.GlobalConstants.screenHeight/2-125
                            , width: Constant.GlobalConstants.screenWidth-40, height: 250)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        self.viewForNotiSetting.addSubview(viewforTip)
                        
                        lblTitle.frame=CGRect(x: 30, y: 0, width: viewforTip.frame.size.width-60, height: 30)
                        lblTitle.text="Notification Settings"
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.font =  UIFont(name:"Helvetica", size: 18)
                        lblTitle.textColor=UIColor.black
                        viewforTip .addSubview(lblTitle)
                        
                        btnClose.frame=CGRect(x: viewforTip.frame.size.width-30, y: 5, width: 20, height: 20)
                        btnClose.setBackgroundImage(UIImage(named: "icon_1_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnCloseNotiViewPressed), for:.touchUpInside)
                        viewforTip.addSubview(btnClose)
                        
                        let lblBy = UILabel()
                        lblBy.frame=CGRect(x: 10, y: lblTitle.frame.maxY-5, width: viewforTip.frame.size.width-20, height: 30)
                        lblBy.text="Recieve Notifications by:"
                        lblBy.textColor = UIColor.black
                        lblBy.textAlignment=NSTextAlignment.left
                        lblBy.font =  UIFont(name:"Helvetica-Light", size: 14)
                        viewforTip .addSubview(lblBy)
                        
                        var X1 = CGFloat()
                        
                        X1 = viewforTip.frame.size.width/3
                        
                        btnEmail.frame=CGRect(x: X1-(X1/2)-12.5, y: lblBy.frame.maxY+5, width: 25, height: 20)
                        btnEmail.setBackgroundImage(UIImage(named: "icon_N_Email"), for: UIControlState.normal)
                        viewforTip .addSubview(btnEmail)
                        
                        
                        btnSMS.frame=CGRect(x: viewforTip.frame.size.width/2-17.5, y: lblBy.frame.maxY+5, width: 30, height: 25)
                        btnSMS.setBackgroundImage(UIImage(named: "icon_N_SMS"), for: UIControlState.normal)
                        viewforTip .addSubview(btnSMS)
                        
                        btnApp.frame=CGRect(x: viewforTip.frame.size.width-55, y: lblBy.frame.maxY+5, width: 20, height: 25)
                        btnApp.setBackgroundImage(UIImage(named: "icon_N_App"), for: UIControlState.normal)
                        viewforTip .addSubview(btnApp)
                        
                        let lblEmail = UILabel()
                        lblEmail.frame=CGRect(x: 0, y: btnEmail.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblEmail.text="Email"
                        lblEmail.textAlignment=NSTextAlignment.center
                        lblEmail.font =  UIFont(name:"Helvetica-Light", size: 12)
                        viewforTip.addSubview(lblEmail)
                        
                        let lblSMS = UILabel()
                        lblSMS.frame=CGRect(x: lblEmail.frame.maxX, y: btnEmail.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblSMS.text="SMS"
                        lblSMS.textAlignment=NSTextAlignment.center
                        lblSMS.font =  UIFont(name:"Helvetica-Light", size: 12)
                        viewforTip.addSubview(lblSMS)
                        
                        let lblApp = UILabel()
                        lblApp.frame=CGRect(x: lblSMS.frame.maxX, y: btnEmail.frame.maxY+2, width: viewforTip.frame.size.width/3, height: 30)
                        lblApp.text="App \n (default)"
                        lblApp.textAlignment=NSTextAlignment.center
                        lblApp.font =  UIFont(name:"Helvetica-Light", size: 12)
                        viewforTip.addSubview(lblApp)
                        
                        let lblAlert = UILabel()
                        lblAlert.frame=CGRect(x: 10, y: lblEmail.frame.maxY, width: viewforTip.frame.size.width, height: 30)
                        lblAlert.text="Alert me when:"
                        lblAlert.textAlignment=NSTextAlignment.left
                        lblAlert.font =  UIFont(name:"Helvetica-Light", size: 12)
                        viewforTip.addSubview(lblAlert)
                        
                        self.tblNotiAlert .removeFromSuperview()
                        self.tblNotiAlert.frame=CGRect(x: 0, y: lblAlert.frame.maxY-5, width: viewforTip.frame.size.width, height: 100)
                        self.tblNotiAlert.delegate=self
                        self.tblNotiAlert.dataSource=self
                        self.tblNotiAlert.tableFooterView=UIView()
                        self.tblNotiAlert.backgroundColor=UIColor.clear
                        self.tblNotiAlert.isScrollEnabled=false
                        viewforTip .addSubview(self.tblNotiAlert)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if app.strSelectedUser == "employee"
        {
            return 3
        }
        else
        {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblNotiAlert.separatorStyle = .none
        
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        
        if tableView == tblList
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
            imgBar.frame=CGRect(x: 7,y: 10,width:18,height: 20)
            
            if indexPath.row==0
            {
                imgBar.frame=CGRect(x: 7,y: 10,width:18,height: 18)
                imgBar.image = UIImage(named:"img_User")
            }
            else if indexPath.row==1
            {
                imgBar.frame=CGRect(x: 7,y: 10,width:18,height: 20)
                imgBar.image = UIImage(named:"icon_Rus")
            }
            else
            {
                imgBar.image = UIImage(named:"icon_noti_1")
            }
            viewForCell.addSubview(imgBar)
            lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 5,width: UIScreen.main.bounds.size.width-20,height:29)
            lblTitle.text = arrType [(indexPath as NSIndexPath).row]
            lblTitle.textColor=UIColor.black
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
            viewForCell .addSubview(lblTitle)
        }
        else
        {
            imgBar.frame=CGRect(x: 5,y: 5,width:30,height: 30)
            if indexPath.row==0 {
                viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
                imgBar.frame=CGRect(x: 5,y: 5,width:30,height: 25)
                lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:40)
                imgBar.image = UIImage(named:"icon_Selted")
            }
            else
            {
                viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 29)
                imgBar.frame=CGRect(x: 5,y: 5,width:25,height: 25)
                lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:30)
                imgBar.image = UIImage(named:"icon_DSelted")
            }
        
            viewForCell .addSubview(imgBar)
        
            lblTitle.text = arrFields [(indexPath as NSIndexPath).row]
            lblTitle.textColor=UIColor.gray
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 12)
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            viewForCell .addSubview(lblTitle)
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == tblList)
        {
            if indexPath.row==0
            {
                if app.strSelectedUser == "employee"
                {
                    let UPS = UserProfileScreen(nibName: "UserProfileScreen", bundle: nil)
                    self.navigationController?.pushViewController(UPS, animated: true)
                }
                else
                {
                    let EPS = EmployerProfileScreen(nibName: "EmployerProfileScreen", bundle: nil)
                    self.navigationController?.pushViewController(EPS, animated: true)
                }
            }
            else if indexPath.row==1
            {
                if app.strSelectedUser == "employee"
                {
                    let MRS = MyResumeListScreen(nibName: "MyResumeListScreen", bundle: nil)
                    self.navigationController?.pushViewController(MRS, animated: true)
            
                }
                else
                {
                   self.viewForNotiSettingUI() 
                }
            }
            else
            {
               self.viewForNotiSettingUI()
            }
        }
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblList
        {
            return 40
        }
        else
        {
            if indexPath.row==0
            {
                return 40
            }
            else
            {
                return 30
            }
        }
    }
}
