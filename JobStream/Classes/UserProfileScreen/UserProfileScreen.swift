
//
//  UserProfileScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 29/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class UserProfileScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    var imgProfileBG = UIImageView()
    var imgProfile = UIImageView()
    var btnResume = UIButton()
    var btnCalagry = UIButton()
    var btnEdit = UIButton()
    @IBOutlet var btnRJ: UIButton!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var viewForAlert = UIView()
    var lblUName = UILabel()
    var lblResume = UILabel()
    var lblCalagry = UILabel()
    var lblApplied = UILabel()
    var lblTitle = UILabel()
    var lblView = UILabel()
    var lblVTitle = UILabel()
    var viewForAV = UIView()
    var tblProfileInfo: UITableView  =   UITableView()
    let arrDetails : [String] = ["","Email","Password","Number",""]
    var app=UIApplication.shared.delegate as! AppDelegate
    var arrProfInfo:NSMutableArray=[]
    var strSelectedUID = NSString()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadInitialUI()
        getProfileInfoAPI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    //MARK: load InitialUI
    func loadInitialUI()
    {
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*35)
        }
        else
        {
            imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*28)
        }
        
        imgProfileBG.image=UIImage(named: "img_PBG")
        //imgProfileBG.alpha=0.5
        self.view.addSubview(imgProfileBG)
        
        var viewForAlpha = UIView()
        viewForAlpha=UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width,height:imgProfileBG.frame.size.height))
        let whte = UIColor.black
        viewForAlpha.backgroundColor = whte.withAlphaComponent(0.7)
        imgProfileBG.addSubview(viewForAlpha)
        
        btnBack.frame=CGRect(x: 3, y: 5, width: 18,height: 18)
        let image = UIImage(named: "icon_BackMove") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btnBackClicked), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btnBackClicked), for:.touchUpInside)
        //btnEdit.addTarget(self, action:#selector(self.btnEditProfPressed), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        
        imgProfile.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-60, y: Constant.GlobalConstants.screenHeight/100*5, width: 120, height: 120)
        imgProfile.image=UIImage(named: "icon_NProfile")
        imgProfile.layer.cornerRadius=imgProfile.frame.size.width/2
        imgProfile.clipsToBounds=true
        imgProfile.layer.borderWidth=2.0
        imgProfile.layer.borderColor=UIColor.white.cgColor
        self.view.addSubview(imgProfile)
        
        //let tap = UITapGestureRecognizer(target: self, action: #selector(showActionSheet))
        //imgProfile.addGestureRecognizer(tap)
        imgProfile.isUserInteractionEnabled = true
        
        lblUName.frame=CGRect(x: 0, y: imgProfile.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblUName.text=""
        lblUName.textAlignment=NSTextAlignment.center
        lblUName.font=UIFont(name: "Helvetica", size: 16)
        lblUName.textColor=UIColor.white
        self.view.addSubview(lblUName)
        
        if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
        {
            btnEdit.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: 0, width: 80, height: 30)
            btnEdit .setTitle("Edit", for: .normal)
            btnEdit.titleLabel?.font = UIFont(name: "Helvetica", size: 18)
            btnEdit .setTitleColor(UIColor.white, for: .normal)
            btnEdit.addTarget(self, action:#selector(self.btnEditProfPressed), for:.touchUpInside)
            self.view.addSubview(btnEdit)
        }
        btnResume.frame=CGRect(x: 35, y: imgProfileBG.frame.size.height/2-20, width: 25, height: 27)
        self.view.addSubview(btnResume)
         btnResume .setBackgroundImage(UIImage(named:"icon_NResume"), for: .normal)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            lblResume.frame=CGRect(x: 20, y: btnResume.frame.maxY-3, width: 70, height: 20)
        }
        else
        {
            lblResume.frame=CGRect(x: 21, y: btnResume.frame.maxY-3, width: 70, height: 20)
        }
        
        lblResume.text="Resume"
        lblResume.textColor=UIColor.white
        lblResume.font=UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblResume)
        
        btnCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-53, y: imgProfileBG.frame.size.height/2-15, width: 18, height: 23)
        let image_1 = UIImage(named: "icon_calagry") as UIImage?
        btnCalagry.setImage(image_1, for: .normal)
        self.view.addSubview(btnCalagry)
        
        lblCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-67, y: btnCalagry.frame.maxY, width: 70, height: 20)
        lblCalagry.text="Calgray"
        lblCalagry.textColor=UIColor.white
        lblCalagry.font=UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblCalagry)
        
        viewForAV.frame=CGRect(x: 0, y: imgProfileBG.frame.maxY, width:Constant.GlobalConstants.screenWidth, height: 40)
        
        viewForAV.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        self.view.addSubview(viewForAV)
        
        lblApplied.frame=CGRect(x: 10, y: 2, width: 60, height: 20)
        lblApplied.text="0"
        lblApplied.textAlignment=NSTextAlignment.center
        lblApplied.font =  UIFont(name:"Helvetica", size: 18)
        lblApplied.textColor = UIColor.black
        viewForAV.addSubview(lblApplied)
        
        lblTitle.frame=CGRect(x: 10, y: lblApplied.frame.maxY, width: 60, height: 20)
        lblTitle.text="Applied"
        lblTitle.textAlignment=NSTextAlignment.center
        lblTitle.textColor = UIColor.init(red: 59/255.0, green: 113/255.0, blue: 175/255.0, alpha: 1)
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
        viewForAV .addSubview(lblTitle)
        
        
        lblView.frame=CGRect(x: Constant.GlobalConstants.screenWidth-80, y: 0, width: 80, height: 20)
        lblView.text="0"
        lblView.textColor=UIColor.black
        lblView.textAlignment=NSTextAlignment.center
        lblView.font =  UIFont(name:"Helvetica", size: 18)
        viewForAV.addSubview(lblView)
        
        lblVTitle.frame=CGRect(x: Constant.GlobalConstants.screenWidth-80, y: lblView.frame.maxY, width: 80, height: 20)
        lblVTitle.text="Views"
        lblVTitle.textAlignment=NSTextAlignment.center
        lblVTitle.textColor=UIColor.init(red: 59/255.0, green: 113/255.0, blue: 175/255.0, alpha: 1)
        lblVTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
        viewForAV .addSubview(lblVTitle)
        
        tblProfileInfo.frame = CGRect(x: 0, y: viewForAV.frame.maxY+10, width: self.view.frame.size.width, height: self.view.frame.size.height-250)
        tblProfileInfo.dataSource = self
        tblProfileInfo.delegate = self
        tblProfileInfo.tableFooterView=UIView()
        tblProfileInfo.backgroundColor=UIColor.clear
        tblProfileInfo.isScrollEnabled=false
        self.view.addSubview(tblProfileInfo)
        
        
        btnResume.addTarget(self, action:#selector(self.btnResumePressed), for:.touchUpInside)
        
       
        btnResume.addTarget(self, action:#selector(self.btnResumePressed), for:.touchUpInside)
        
        btnCalagry.addTarget(self, action:#selector(self.btnCalgryPressed), for:.touchUpInside)
        btnCalagry.addTarget(self, action:#selector(self.btnCalgryPressed), for:.touchUpInside)
      
    }
    
    func btnBackClicked()
    {
        navigationController?.popViewController(animated:true)
    }
    func btnResumePressed()
    {
        let VRS = ViewResumeScreen(nibName: "ViewResumeScreen", bundle: nil)
        self.navigationController?.pushViewController(VRS, animated: true)
    }
    func btnCalgryPressed()
    {
        self.viewForAlertUI()
    }
    
    func btnEditProfPressed()
    {
        let VRS = EditProfileScreen(nibName: "EditProfileScreen", bundle: nil)
        self.present(VRS, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return arrProfInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let lblTitle=UILabel()
        let lblDessc=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblProfileInfo.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
      
        if indexPath.row == 3
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 69)
            
            lblDessc.frame=CGRect(x: 10, y: 0, width: 120, height: 20)
            lblDessc.text="Description"
            lblDessc.textAlignment=NSTextAlignment.left
            lblDessc.font =  UIFont(name:"Helvetica-Light", size: 16)
            lblDessc.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
            
            lblTitle.frame=CGRect(x: 10, y: lblDessc.frame.maxY, width: viewForCell.frame.size.width-10, height: 50)
            lblTitle.text="Add a description of who you are. This will \n  be seen by employers so make sure you \n showcase your best."
            lblTitle.textAlignment=NSTextAlignment.center
            lblTitle.textColor=UIColor.gray
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment=NSTextAlignment.left
            
            viewForCell.addSubview(lblDessc)
            viewForCell .addSubview(lblTitle)
        }
        else
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
            lblTitle.frame=CGRect(x: 10, y: 5, width: Constant.GlobalConstants.screenWidth, height: 30)
            lblTitle.text = (arrProfInfo [(indexPath as NSIndexPath).row]as! NSString) as String
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 16)

            lblTitle.textColor=UIColor(red: 19.0/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1.0)
            viewForCell .addSubview(lblTitle)
        }        
       
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 55
        }
        else if indexPath.row == 4
        {
            return 75
        }
        else
        {
            return 45
        }
    }

    
    //MARK: ViewForHelpView
    func viewForAlertUI()
    {
        viewForAlert.frame=CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0)
        let whte = UIColor.black
        viewForAlert.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForAlert)
        
        
        var frame1 = CGRect()
        
        frame1=viewForAlert.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        let lblSubTitle1 = UILabel()
                        let imgCheck = UIImageView()
                        
                        btnClose.frame=CGRect(x: self.viewForAlert.frame.size.width-40, y: 2, width: 35, height: 35)
                        btnClose.setBackgroundImage(UIImage(named: "icon_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnCloseSavePressed), for:.touchUpInside)
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-70
                            , width: Constant.GlobalConstants.screenWidth-60, height: 160)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 5, y: imgCheck.frame.maxY, width: viewforTip.frame.size.width-10, height: 30)
                        lblTitle.text="Contact"
                        lblTitle.font=UIFont(name:"Helvetica", size: 18)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.frame=CGRect(x: 5, y: lblTitle.frame.maxY+5, width: viewforTip.frame.size.width-10, height: 70)
                        lblSubTitle1.frame=CGRect(x: 5, y: lblSubTitle.frame.maxY+10, width: viewforTip.frame.size.width-10, height: 40)
                        
                        lblSubTitle.text="You are about to contact this person without \n viewing their resume. If you choose to \n contact this person by email or phone \n without viewing their resume a charge of \n $0.50 will be applied to your account."
                        
                        lblSubTitle1.text="Viewinng this persons resume will avoid additional charges listed above."
                        
                        lblSubTitle.font=UIFont(name:"Helvetica-Light", size: 12)
                        lblSubTitle1.font=UIFont(name:"Helvetica-Light", size: 12)
                        
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        lblSubTitle1.textAlignment=NSTextAlignment.center
                        
                        lblSubTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        lblSubTitle1.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblSubTitle1.lineBreakMode = NSLineBreakMode.byWordWrapping
                        
                        lblSubTitle.numberOfLines = 0
                        lblSubTitle1.numberOfLines = 0
                        
                        self.viewForAlert.addSubview(btnClose)
                        self.viewForAlert.addSubview(viewforTip)
                        
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)
                        viewforTip.addSubview(lblSubTitle1)
                        
        })
    }
    
    //MARK: UIButton Help Pressed
    func btnCloseSavePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForAlert .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForAlert.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForAlert.frame=frameViewForHelp
        })
    }

    //MARK: update user Info
    
    func getProfileInfoAPI()
    {
        let bodydata = NSMutableDictionary()
        
        if strSelectedUID.length>0 {
            bodydata.setObject((strSelectedUID), forKey: "uid" as NSCopying)
        }
        else
        {
            bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        }
        
         print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsuserprofile_edit.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                var strEmail = NSString()
                var strPhone = NSString()
                var strPass = NSString()
                var strUName = NSString()
                var strView = NSString()
                var strApplied = NSString()
                
                strView=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "viewlist")as!NSString
                strApplied=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "jobappliedlist")as!NSString
                
                lblView.text=strView as String
                lblApplied.text=strApplied as String
                
                strUName=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "name")as!NSString
                lblUName.text=strUName as String
                
                strEmail=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "email")as!NSString
                
                strPhone=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "phone")as!NSString
                
                strPass=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "password")as!NSString
                
                 arrProfInfo .add(strEmail)
                 arrProfInfo .add(strPhone)
                 arrProfInfo .add(strPass)
                 arrProfInfo .add("")
                
                print(arrProfInfo)
                tblProfileInfo.reloadData()
            }
        }
        catch
        {
            print("Error")
        }
    }

}
