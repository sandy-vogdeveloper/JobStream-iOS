//
//  RegistrationScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 25/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class RegistrationScreen: UIViewController,UITextFieldDelegate
{
     var btnBack = UIButton()
     var btnBack1 = UIButton()

    var btnSignUp = UIButton()
    
    var btnLogin = UIButton()
    var btnLogin1 = UIButton()
    var btnSkip = UIButton()
    var btnSkip1 = UIButton()
    
    var txtName = UITextField()
    var txtPhone = UITextField()
    var txtEmail = UITextField()
    var txtPassword = UITextField()
    
    var viewName = UIView()
    var viewPhone = UIView()
    var viewEmail = UIView()
    var viewPassword = UIView()
    
    var imgName = UIImageView()
    var imgMob = UIImageView()
    var imgEmail = UIImageView()
    var imgPassword = UIImageView()
    
    var streName=""
    var strPhone=""
    var strEmail=""
    var strPass=""
    
    @IBOutlet var lblTitle: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.loadUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Load Main UI
    func loadUI()
    {
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
    
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)        
        
        viewName=UIView(frame: CGRect(x: 0, y: btnBack1.frame.maxY+25, width: self.view.frame.size.width,height:75))
        
        viewPhone=UIView(frame: CGRect(x: 0, y: viewName.frame.maxY+15, width: self.view.frame.size.width,height:75))
        
        viewEmail=UIView(frame: CGRect(x: 0, y: viewPhone.frame.maxY+15, width: self.view.frame.size.width,height:75))
        
        viewPassword=UIView(frame: CGRect(x: 0, y: viewEmail.frame.maxY+15, width: self.view.frame.size.width,height:75))
        
        btnSignUp=UIButton(frame: CGRect(x: 0, y: viewPassword.frame.maxY+20, width:Constant.GlobalConstants.screenWidth, height: 45))
        
        let whte = UIColor.white
        viewName.backgroundColor = whte.withAlphaComponent(0.8)
        viewPhone.backgroundColor = whte.withAlphaComponent(0.8)
        viewEmail.backgroundColor = whte.withAlphaComponent(0.8)
        viewPassword.backgroundColor = whte.withAlphaComponent(0.8)
        
        btnSignUp.backgroundColor=UIColor(red: 42.0/255.0, green: 87.0/255.0, blue: 157.0/255.0, alpha: 1.0).withAlphaComponent(0.7)
        btnSignUp.setTitle("Sign Up", for: .normal)
        btnSignUp.setTitleColor(UIColor.white, for: .normal)
        btnSignUp.titleLabel!.font =  UIFont(name:"Helvetica", size: 22)
        btnSignUp.addTarget(self, action: #selector(self.btnSignUpPressed), for: .touchUpInside)
        
        self.view.addSubview(viewName)
        self.view.addSubview(viewPhone)
        self.view.addSubview(viewEmail)
        self.view.addSubview(viewPassword)
        self.view.addSubview(btnSignUp)
        
        imgName=UIImageView(frame: CGRect(x: 16, y: 15, width:viewName.frame.size.height-33, height: viewName.frame.size.height-30))
        imgMob=UIImageView(frame: CGRect(x: 15, y: 15, width:viewPhone.frame.size.height-30, height: viewPhone.frame.size.height-30))
        imgEmail=UIImageView(frame: CGRect(x: 10, y: 18, width:viewEmail.frame.size.height-20, height: viewEmail.frame.size.height-36))
        imgPassword=UIImageView(frame: CGRect(x: 15, y: 10, width:viewPassword.frame.size.height-30, height:viewPassword.frame.size.height-20))
        
        imgName.image = UIImage(named:"img_1.1")
        imgMob.image = UIImage(named:"icon_Phone")
        imgEmail.image = UIImage(named:"icon_Email")
        imgPassword.image = UIImage(named:"icon_Password")
        
        viewName.addSubview(imgName)
        viewPhone.addSubview(imgMob)
        
        viewEmail.addSubview(imgEmail)
        viewPassword.addSubview(imgPassword)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            txtName=UITextField(frame:CGRect(x: imgName.frame.maxX+10, y: 17, width: viewName.frame.size.width-imgName.frame.size.width-100, height: 40))
            txtPhone=UITextField(frame:CGRect(x: imgMob.frame.maxX+10, y: 17, width: viewPhone.frame.size.width-imgMob.frame.size.width-100, height: 40))
            
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-100, height: 40))
            txtPassword=UITextField(frame:CGRect(x: imgPassword.frame.maxX+10, y: 17, width: viewPassword.frame.size.width-imgPassword.frame.size.width-100, height: 40))
        }
        else
        {
            txtName=UITextField(frame:CGRect(x: imgName.frame.maxX+10, y: 17, width: viewName.frame.size.width-imgName.frame.size.width-40, height: 40))
            txtPhone=UITextField(frame:CGRect(x: imgMob.frame.maxX+10, y: 17, width: viewPhone.frame.size.width-imgMob.frame.size.width-40, height: 40))
            
            txtEmail=UITextField(frame:CGRect(x: imgEmail.frame.maxX+10, y: 17, width: viewEmail.frame.size.width-imgEmail.frame.size.width-40, height: 40))
            txtPassword=UITextField(frame:CGRect(x: imgPassword.frame.maxX+10, y: 17, width: viewPassword.frame.size.width-imgPassword.frame.size.width-40, height: 40))
        }
        txtName.placeholder="Full Name"
        txtPhone.placeholder="Phone Number"
        txtEmail.placeholder="Email"
        txtPassword.placeholder="Password"
        
        txtName.attributedPlaceholder = NSAttributedString(string:"Full Name", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtPhone.attributedPlaceholder = NSAttributedString(string:"Phone Number", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica-Light", size: 18)!])
        
        txtName.font =  UIFont(name:"Helvetica-Light", size: 22)
        txtPhone.font =  UIFont(name:"Helvetica-Light", size: 22)
        txtEmail.font =  UIFont(name:"Helvetica-Light", size: 22)
        txtPassword.font =  UIFont(name:"Helvetica-Light", size: 22)
        
        txtName.keyboardType=UIKeyboardType.default
        txtPhone.keyboardType=UIKeyboardType.numberPad
        txtEmail.keyboardType=UIKeyboardType.emailAddress
        txtPassword.keyboardType=UIKeyboardType.default
        
        txtName.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.done
        
        txtName.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtPhone.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtEmail.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtPassword.clearButtonMode = UITextFieldViewMode.whileEditing;
        
        txtName.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        txtPhone.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        txtEmail.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        txtPassword.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        txtName.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtPassword.isSecureTextEntry=true
        
        viewName .addSubview(txtName)
        viewPhone .addSubview(txtPhone)
        viewEmail .addSubview(txtEmail)
        viewPassword .addSubview(txtPassword)
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtPhone.inputAccessoryView = toolBar
        
        configureTextField(x: 0,y: txtName.frame.size.height-1.0, width: txtName.frame.size.width, height:1.0, textField: txtName)
        
        configureTextField(x: 0,y: txtPhone.frame.size.height-1.0, width: txtPhone.frame.size.width, height:1.0, textField: txtPhone)
        
        configureTextField(x: 0,y: txtEmail.frame.size.height-1.0, width: txtEmail.frame.size.width, height:1.0, textField: txtEmail)
        
        configureTextField(x: 0,y: txtPassword.frame.size.height-1.0, width: txtPassword.frame.size.width, height:1.0, textField: txtPassword)
        
        btnLogin1.frame=CGRect(x: 10, y: Constant.GlobalConstants.screenHeight-35, width: 100, height: 30)
        btnLogin.frame=CGRect(x: 18, y: Constant.GlobalConstants.screenHeight-50, width: 18, height: 20)
        btnSkip.frame=CGRect(x: Constant.GlobalConstants.screenWidth-105, y: Constant.GlobalConstants.screenHeight-35, width: 80, height: 30)
        btnSkip1.frame=CGRect(x: btnSkip.frame.maxX+7, y: Constant.GlobalConstants.screenHeight-29, width: 10, height: 18)
        
        btnLogin1.setTitle("Login", for: .normal)
        btnSkip.setTitle("Skip", for: .normal)
        
        btnLogin1.setTitleColor(UIColor.white, for: .normal)
        btnSkip.setTitleColor(UIColor.white, for: .normal)
        
        btnLogin1.setTitleColor(UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1), for: .normal)
        btnSkip.setTitleColor(UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1), for: .normal)
        
        btnLogin1.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 16)
        btnSkip.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 16)
        
        let image_1 = UIImage(named: "icon_BProfile") as UIImage?
        let image_2 = UIImage(named: "icon_SkipA") as UIImage?
        
        btnLogin.setImage(image_1, for: .normal)
        btnSkip1.setImage(image_2, for: .normal)
        
        btnLogin1.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btnSkip.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        
        
        btnLogin.addTarget(self, action:#selector(self.btnLoginPressed), for: .touchUpInside)
        btnLogin1.addTarget(self, action:#selector(self.btnLoginPressed), for: .touchUpInside)
        self.view.addSubview(btnLogin)
        self.view.addSubview(btnLogin1)
        self.view.addSubview(btnSkip)
        self.view.addSubview(btnSkip1)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.txtName {
            self.txtPhone.becomeFirstResponder()
        }
        else if textField == self.txtPhone
        {
            self.txtEmail.becomeFirstResponder()
        }
        else if textField == self.txtEmail
        {
            self.txtPassword.becomeFirstResponder()
        }
        else if textField == self.txtPassword
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func doneClick() {
       
        txtPhone.resignFirstResponder()
    }
    //MARK: Add underline at textfield bottom
    func configureTextField(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat,textField:UITextField)
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: x, y: y, width: width, height: height)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        textField.borderStyle = UITextBorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
    func isValidTextField() -> Bool
    {
        
        var strMsg = NSString()
        var intName = NSInteger()
        var intPhone = NSInteger()
        var intEmail = NSInteger()
        var intPass = NSInteger()
        
        intName=(txtName.text?.characters.count)!
        intPhone=(txtPhone.text?.characters.count)!
        intEmail=(txtEmail.text?.characters.count)!
        intPass=(txtPassword.text?.characters.count)!
        
        if intName > 0
        {
            if intPhone > 0
            {
            
                if intEmail > 0
                {
                    if intPass > 0
                    {
                        return true;
                    }
                    else
                    {
                        strMsg="Please enter Password."
                    }
                }
                else
                {
                    strMsg = "Please enter Email."
                }
            }
            else
            {
                strMsg = "Please enter Phone."
            }
        }
        else
        {
            strMsg="Please enter Name."
        }
    
        let alertMessage = UIAlertController(title: "Warning", message: strMsg as String, preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alertMessage, animated: true, completion: nil)
        
        return false
    }
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
   
    func btnSignUpPressed()
    {
        if isValidTextField()
        {
            loadRegistrationAPI()
            //let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
            //self.navigationController?.pushViewController(LC, animated: true)
        }
    }
    
    //MARK: Button Pressed delegate declarations here
    func btnLoginPressed()
    {
        let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
        self.navigationController?.pushViewController(LC, animated: true)
    }
    //MARK: Login ServerCall
    func loadRegistrationAPI()
    {
        streName=txtName.text!
        strPhone=txtPhone.text!
        strEmail=txtEmail.text!
        strPass=txtPassword.text!
        
        let bodydata = NSMutableDictionary()
     
        bodydata.setObject((streName), forKey: "name" as NSCopying)
        bodydata.setObject((strPhone), forKey: "phone" as NSCopying)
        bodydata.setObject((strEmail), forKey: "email" as NSCopying)
        bodydata.setObject((strPass), forKey: "password" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strSelectedUser), forKey: "role" as NSCopying)
     
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsuser_signup.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_user_login(apiAlias:response:)), andDelegate: self)
    }
    
    func response_user_login(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
             print(ResponceData)
            
             let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            var strMSG = NSString()
            strMSG=ResponceData.value(forKey: "message") as! NSString
             print(strMSG)
            if strMSG == "sus"
            {
                let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
                self.navigationController?.pushViewController(LC, animated: true)
            }
            else if strMSG == "already Registered"
            {
                
            }
            print(strsucess1)
        }
        
        catch
        {
            print("Error")
        }
    }
    
    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -70
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
}
