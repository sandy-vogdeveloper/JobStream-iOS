//
//  EditProfileScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 10/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class EditProfileScreen: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate {

    /*---------   ImageView Group     -----------*/
    var imgProfile = UIImageView()
    var imgProfileBG = UIImageView()
    
        /*-------   UIView Group     -----------*/
    var viewForAlpha = UIView()
    var viewForName = UIView()
    var viewForEmail = UIView()
    var viewForPassword = UIView()
    var viewForNumber = UIView()
    var viewForDesc = UIView()
    var viewForAV = UIView()
    var viewForCall = UIView()
    
    /*-----------  UITextField Group     -----------*/
    var txtName = UITextField()
    var txtEmail = UITextField()
    var txtPassword = UITextField()
    var txtNumber = UITextField()
    var txtDesc = UITextView()
    
    /*----------   UIButton Group     -----------*/
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var btnResume = UIButton()
    var btnCalagry = UIButton()
    var btnSave = UIButton()
    
    /*------------   UILabel Group     -----------*/
    var lblUName = UILabel()
    var lblResume = UILabel()
    var lblCalagry = UILabel()
    var lblDescTit = UILabel()
    var lblDescPHolder = UILabel()
    var lblEmail = UILabel()
    var lblCall = UILabel()
    
    var txtVDesc = UITextView()
    
    let imageController = UIImagePickerController()
    var str_profile_img = NSString()
    var img_data:NSData!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadInitialUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Load InitialUI
    func loadInitialUI()
    {        
        if Constant.GlobalConstants.screenWidth < 325
        {
            imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*35)
        }
        else
        {
            imgProfileBG.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight/100*28)
        }
        
        imgProfileBG.image=UIImage(named: "img_PBG")
        //imgProfileBG.alpha=0.5
        self.view.addSubview(imgProfileBG)
        
        var viewForAlpha = UIView()
        viewForAlpha=UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width,height:imgProfileBG.frame.size.height))
        //let whte = UIColor.black
        //viewForAlpha.backgroundColor = whte.withAlphaComponent(0.7)
        viewForAlpha.backgroundColor = UIColor(red: 65.0/255.0, green: 98.0/255.0, blue: 129.0/255.0, alpha: 1.0).withAlphaComponent(0.8)
        imgProfileBG.addSubview(viewForAlpha)
        
        btnBack.frame=CGRect(x: 3, y: 5, width: 18,height: 18)
        let image = UIImage(named: "icon_BackMove") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        //btnEdit.addTarget(self, action:#selector(self.btnEditProfPressed), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        
        imgProfile.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-60, y: Constant.GlobalConstants.screenHeight/100*5, width: 120, height: 120)
        imgProfile.image=UIImage(named: "icon_NProfile")
        imgProfile.layer.cornerRadius=imgProfile.frame.size.width/2
        imgProfile.clipsToBounds=true
        imgProfile.layer.borderWidth=2.0
        imgProfile.layer.borderColor=UIColor.white.cgColor
        self.view.addSubview(imgProfile)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(showActionSheet))
        imgProfile.addGestureRecognizer(tap)
        imgProfile.isUserInteractionEnabled = true
        
        lblUName.frame=CGRect(x: 0, y: imgProfile.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblUName.text=""
        lblUName.textAlignment=NSTextAlignment.center
        lblUName.font=UIFont(name: "Helvetica", size: 16)
        lblUName.textColor=UIColor.white
        self.view.addSubview(lblUName)
        
        btnSave.frame=CGRect(x: Constant.GlobalConstants.screenWidth-70, y: 0, width: 80, height: 30)
        btnSave .setTitle("Save", for: .normal)
        btnSave.titleLabel?.font = UIFont(name: "Helvetica", size: 18)
        btnSave .setTitleColor(UIColor.white, for: .normal)
        //btnEdit.addTarget(self, action:#selector(self.btnEditProfPressed), for:.touchUpInside)
        self.view.addSubview(btnSave)
        
        btnResume.frame=CGRect(x: 35, y: imgProfileBG.frame.size.height/2-20, width: 25, height: 27)
        self.view.addSubview(btnResume)
        btnResume .setBackgroundImage(UIImage(named:"icon_NResume"), for: .normal)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            lblResume.frame=CGRect(x: 20, y: btnResume.frame.maxY-10, width: 70, height: 20)
        }
        else
        {
            lblResume.frame=CGRect(x: 21, y: btnResume.frame.maxY-3, width: 70, height: 20)
        }
        
        lblResume.text="Resume"
        lblResume.textColor=UIColor.white
        lblResume.font=UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblResume)
        
        btnCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-53, y: imgProfileBG.frame.size.height/2-20, width: 18, height: 23)
        let image_1 = UIImage(named: "icon_calagry") as UIImage?
        btnCalagry.setImage(image_1, for: .normal)
        self.view.addSubview(btnCalagry)
        
        lblCalagry.frame=CGRect(x: Constant.GlobalConstants.screenWidth-67, y: btnCalagry.frame.maxY, width: 70, height: 20)
        lblCalagry.text="Calgray"
        lblCalagry.textColor=UIColor.white
        lblCalagry.font=UIFont(name: "Helvetica", size: 14)
        self.view.addSubview(lblCalagry)
        
        
        viewForAV.frame=CGRect(x: 0, y: imgProfileBG.frame.maxY, width:Constant.GlobalConstants.screenWidth, height: 80)
        viewForAV.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        self.view.addSubview(viewForAV)
        
        lblDescTit.frame=CGRect(x: 3, y: 0, width: 100, height: 20)
        lblDescTit.text="Description"
        lblDescTit.textColor=UIColor.black
        lblDescTit.textAlignment=NSTextAlignment.left
        lblDescTit.font=UIFont(name: "Helvetica", size: 14)
        viewForAV.addSubview(lblDescTit)
        
        txtVDesc.frame=CGRect(x: 3, y: lblDescTit.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 60)
        txtVDesc.textAlignment = .left
        txtVDesc.font=UIFont(name: "Helvetica", size: 14)
        txtVDesc.delegate = self
        txtVDesc.textColor=UIColor.lightGray
        txtVDesc.font=UIFont(name:"Helvetica", size: 14)
        txtVDesc.backgroundColor=UIColor.clear
        txtVDesc.autocorrectionType = .no
        viewForAV.addSubview(txtVDesc)
        
        lblDescPHolder.frame=CGRect(x: 3, y: 0, width: txtVDesc.frame.size.width, height: 60)
        lblDescPHolder.text="Add a description of who you are. This will \n be seen by employers so make sure you \n showcase your best."
        lblDescPHolder.font=UIFont(name:"Helvetica", size: 14)
        lblDescPHolder.textAlignment = .left
        lblDescPHolder.textColor=UIColor.lightGray
        lblDescPHolder.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDescPHolder.numberOfLines = 0
        txtVDesc.addSubview(lblDescPHolder)
        
        
        viewForEmail.frame=CGRect(x: 0, y: viewForAV.frame.maxY+5, width:Constant.GlobalConstants.screenWidth, height: 65)
        viewForEmail.backgroundColor=UIColor(red: 234.0/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        self.view.addSubview(viewForEmail)
        
        txtEmail.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        txtEmail.backgroundColor=UIColor.clear
        viewForEmail.addSubview(txtEmail)
        
        txtNumber.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2 , y: 0, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        txtNumber.backgroundColor=UIColor.clear
        viewForEmail.addSubview(txtNumber)
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"  hireme@jobs.com", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtNumber.attributedPlaceholder = NSAttributedString(string:"  403-555-5555", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        lblEmail.frame=CGRect(x: 0, y: txtEmail.frame.maxY, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        lblCall.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2 , y: txtNumber.frame.maxY, width: Constant.GlobalConstants.screenWidth/2, height: 40)
        
        lblEmail.text="Email"
        lblCall.text="Call"
        
        lblEmail.textAlignment = .center
        lblCall.textAlignment = .center
        
        lblEmail.textColor=UIColor.lightGray
        lblCall.textColor=UIColor.lightGray
        
        viewForEmail.addSubview(lblEmail)
        viewForEmail.addSubview(lblCall)
        
        /* viewForName.frame=CGRect(x: 0, y: imgProfile.frame.maxY+15, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForName.backgroundColor=UIColor.white
        self.view.addSubview(viewForName)
        
        viewForEmail.frame=CGRect(x: 0, y: viewForName.frame.maxY+5, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForEmail.backgroundColor=UIColor.white
        self.view.addSubview(viewForEmail)
        
        viewForPassword.frame=CGRect(x: 0, y: viewForEmail.frame.maxY+5, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForPassword.backgroundColor=UIColor.white
        self.view.addSubview(viewForPassword)
        
        viewForNumber.frame=CGRect(x: 0, y: viewForPassword.frame.maxY+5, width: Constant.GlobalConstants.screenWidth, height: 40)
        viewForNumber.backgroundColor=UIColor.white
        self.view.addSubview(viewForNumber)
        
        
        txtName.frame=CGRect(x: 10, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtName.backgroundColor=UIColor.clear
        viewForName.addSubview(txtName)
        
        txtEmail.frame=CGRect(x: 10, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtEmail.backgroundColor=UIColor.clear
        viewForEmail.addSubview(txtEmail)
        
        txtPassword.frame=CGRect(x: 10, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtPassword.backgroundColor=UIColor.clear
        viewForPassword.addSubview(txtPassword)
        
        txtNumber.frame=CGRect(x: 10, y: 0, width: Constant.GlobalConstants.screenWidth-10, height: 40)
        txtNumber.backgroundColor=UIColor.clear
        viewForNumber.addSubview(txtNumber)
        
        
        txtName.attributedPlaceholder = NSAttributedString(string:"  Name", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"  Email", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string:"  Password", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
        
        txtNumber.attributedPlaceholder = NSAttributedString(string:"  Number", attributes:[NSForegroundColorAttributeName: UIColor.black,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])*/
    }
    
    //MARK: UIButton Pressed delegate
    
    //MARK: Button save pressed
    func btnSavePressed(sender: UIButton!)
    {
        self.dismiss(animated: true, completion: nil)
        
    }

    
    //MARK: UIButton Back delegate
    func btn_back_action(sender: UIButton!) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- textFiled Delegate
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtVDesc
        {
            lblDescPHolder .isHidden=true
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        var intCount1 = NSInteger()
        
        if textView == txtVDesc
        {
            intCount1=(txtVDesc.text?.characters.count)!
            if intCount1>0
            {
                self.lblDescPHolder.isHidden=true
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }

    
    func showActionSheet(sender: AnyObject) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        imageController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imageController.allowsEditing = true
        imageController.mediaTypes = UIImagePickerController.availableMediaTypes(for: UIImagePickerControllerSourceType.photoLibrary)!
        imageController.delegate = self;
        
        present(imageController, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imageController.allowsEditing = false
            imageController.sourceType = UIImagePickerControllerSourceType.camera
            imageController.cameraCaptureMode = .photo
            imageController.delegate=self
            present(imageController, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("picker cancel.")
    }
    
    //MARK: - Imagpicker Delegates
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        print(chosenImage)
        imgProfile.image=chosenImage
        imgProfileBG.image=chosenImage
        let size = CGSize(width: 200.0, height: 200.0)
        chosenImage = self.resizeImage(image: chosenImage,targetSize: size)
        
        img_data = UIImageJPEGRepresentation(chosenImage, 1) as NSData!
        
        self.dismiss(animated: true, completion: { () -> Void in
            Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
            self.uploadimagefile1()
        })
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func uploadimagefile1()  {
        //print(img_data)
        // let params = NSMutableDictionary()
        
        let boundaryConstant  = "----------14737809831466499882746641449"
        //api/wsupdateuserimg_ios.php
        let file1ParamConstant = "userfile"
        let requestUrl = NSURL(string:"http://13.126.3.151/job_stream/api/uploadimage_ios.php")
        
        let request = NSMutableURLRequest()
        
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.httpShouldHandleCookies=false
        //request.timeoutInterval = 30
        request.httpMethod = "POST"
        
        let contentType = "multipart/form-data; boundary=\(boundaryConstant)"
        
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        if img_data==nil
        {
            return
        }
        
        let body = NSMutableData()
        
        // image begin
        body.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("Content-Disposition: form-data; name=\"\(file1ParamConstant)\"; filename=\"image1.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
        
        body.append(img_data! as Data)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        // image end
        body.append("--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody  = body as Data
        let postLength = "\(body.length)"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.url = requestUrl as URL?
        
        var serverResponse = NSString()
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            Constant.GlobalConstants.theAppDelegate.stopSpinner()
            serverResponse = responseString!
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    // Print out dictionary
                    print(convertedJsonIntoDict)
                    let  strsucess1=(convertedJsonIntoDict .value(forKey: "status") as! NSNumber)
                    if strsucess1 == 1
                    {
                        self.str_profile_img=(convertedJsonIntoDict .value(forKey: "info") as! String as NSString)
                        print(self.str_profile_img)
                    }
                    else
                    {
                        let alertMessage = UIAlertController(title: "Error", message: "Failed to upload" as String, preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                let alertMessage = UIAlertController(title: "Error", message: error.localizedDescription as String, preferredStyle: .alert)
                alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
        
        task.resume()
    }

    //MARK: Login ServerCall
    func SaveUserInfoAPI()
    {
        var strEmail = ""
        var strCall = ""
        var strDesc = ""
        
        strEmail=txtEmail.text!
        strCall=txtNumber.text!
        strDesc=txtVDesc.text!
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((strEmail), forKey: "email" as NSCopying)
        bodydata.setObject((strCall), forKey: "password" as NSCopying)
        
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wslogin.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_user_login(apiAlias:response:)), andDelegate: self)
    }
    
    func response_user_login(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
            }
        }
        catch
        {
            let alertMessage = UIAlertController(title: "Sorry!", message: "Invalid login details" as String, preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
            
        }
    }
}
