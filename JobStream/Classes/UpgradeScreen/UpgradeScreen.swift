//
//  UpgradeScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 27/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class UpgradeScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {

    let arrType : [String] = ["","Send more than one document \n per resume: \n 1-3 docs = 3.99 \n 4-5 docs = 4.99 \n 5 and up = 6.99","Add additional resumes:","See who has viewedyour resume:"]
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnHelp: UIButton!
    @IBOutlet var btnHelp1: UIButton!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    
    var viewForHelp = UIView()
    var tableView: UITableView  = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadInitialUI()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: load InitialUI
    func loadInitialUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
                
        btnHelp.addTarget(self, action:#selector(self.viewForHelpView), for:.touchUpInside)
        
        btnHelp1.addTarget(self, action:#selector(self.viewForHelpView), for:.touchUpInside)
                
        tableView.frame = CGRect(x: 0, y: 45, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView=UIView()
        tableView.backgroundColor=UIColor.clear
        tableView.isScrollEnabled=false
        self.view.addSubview(tableView)
    }
    
    
    //MARK: UIButton Pressed delegate declarations here
    
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    //MARK: UIButton Help Pressed
    func btnCloseHelpViewPressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForHelp .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForHelp.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForHelp.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForHelp.frame=frameViewForHelp
        })
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let lblTitle=UILabel()
        let lblSubTitle=UILabel()
        let lblNumber=UILabel()
        
        cell.backgroundColor=UIColor.clear
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tableView.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 5,width: UIScreen.main.bounds.size.width,height: 120)
        viewForCell.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        cell.contentView .addSubview(viewForCell)
        
        
        if indexPath.row==0
        {
            lblTitle.frame=CGRect(x: 10,y: 0,width: UIScreen.main.bounds.size.width-20,height:120)
        lblTitle.text="Get notifications on who is viewing your \n resume and learn about which employers \n are looking at your application. Add \n unlimited documents and resumes to \n your profile"
            
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblTitle.textColor=UIColor.gray
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            lblTitle.textAlignment=NSTextAlignment.center
            viewForCell .addSubview(lblTitle)
        }
        else
        {
            lblNumber.frame=CGRect(x: 5, y: 0, width: 30, height: 30)
            lblNumber.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
            lblNumber.textAlignment = .left
            lblNumber.font=UIFont(name:"Helvetica", size: 16)
            
            if indexPath.row==1 {
                lblNumber.text="1)"
            }
            else if indexPath.row==2
            {
                lblNumber.text="2)"
            }
            else
            {
                lblNumber.text="3)"
            }
            
            
            if indexPath.row==1
            {
                lblTitle.frame=CGRect(x: lblNumber.frame.maxX+5,y: 0,width: UIScreen.main.bounds.size.width-40,height:100)
                lblTitle.text = arrType [(indexPath as NSIndexPath).row]
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.numberOfLines = 0
            }
            else
            {
                lblTitle.frame=CGRect(x: lblNumber.frame.maxX+5,y: 0,width: UIScreen.main.bounds.size.width-40,height:40)
                lblTitle.text = arrType [(indexPath as NSIndexPath).row]
                
                lblSubTitle.frame=CGRect(x: lblNumber.frame.maxX+5,y: lblTitle.frame.maxY+10,width: UIScreen.main.bounds.size.width-40,height:30)
                if indexPath.row==2
                {
                    lblSubTitle.text = "Add more resumes for $5.99"
                }
                else
                {
                    lblSubTitle.frame=CGRect(x: lblNumber.frame.maxX+5,y: lblTitle.frame.maxY+10,width: UIScreen.main.bounds.size.width-40,height:40)
                   lblSubTitle.text = "Activate the ability to see who has \n viewed your resume for only $5.99"
                    lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                    lblSubTitle.numberOfLines = 0
                }
            }
            
            
            lblTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
            lblSubTitle.font =  UIFont(name:"Helvetica-Light", size: 16)
            
            lblTitle.textColor=UIColor.gray
            lblSubTitle.textColor=UIColor.gray
           
            lblTitle.textAlignment=NSTextAlignment.left
            lblSubTitle.textAlignment=NSTextAlignment.left
            
            viewForCell.addSubview(lblNumber)
            viewForCell .addSubview(lblTitle)
            viewForCell .addSubview(lblSubTitle)
            
            let imgForward=UIImageView()
            imgForward.frame=CGRect(x: viewForCell.frame.size.width-30,y: viewForCell.frame.size.height/2-15,width: 10,height: 13)
            imgForward.image = UIImage(named:"icon_Forward")
            viewForCell .addSubview(imgForward)
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row==0
        {
            
        }
        else
        {
            let paymt=PayPaymentScreen(nibName: "PayPaymentScreen", bundle: nil)
            self.navigationController?.pushViewController(paymt, animated: true)
        }
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 130
    }
    
    //MARK: ViewForHelpView
    func viewForHelpView()
    {
        viewForHelp=UIView(frame: CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0))
        let whte = UIColor.black
        viewForHelp.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForHelp)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseHelpViewPressed))
        viewForHelp.addGestureRecognizer(tap)
        viewForHelp.isUserInteractionEnabled = true
        
        var frame1 = CGRect()
        
        frame1=viewForHelp.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForHelp.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        
                        
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-60
                            , width: Constant.GlobalConstants.screenWidth-60, height: 120)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 5, y: 0, width: viewforTip.frame.size.width-10, height: 60)
                        lblTitle.text="To add more resume's to your account, \n please upgrade your account."
                        lblTitle.font=UIFont(name:"Helvetica", size: 12)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.gray
                        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblTitle.numberOfLines = 0
                        
                        let imgForward=UIImageView()
                        imgForward.frame=CGRect(x: viewforTip.frame.size.width/2-12,y: lblTitle.frame.maxY,width: 24,height: 30)
                        imgForward.image = UIImage(named:"img_Upgrde")
                        
                        lblSubTitle.frame=CGRect(x: 5, y: imgForward.frame.maxY+3, width: viewforTip.frame.size.width, height: 30)
                        lblSubTitle.text="Upgrade"
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 12)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        lblSubTitle.textColor=UIColor.gray
                        
                        self.viewForHelp.addSubview(viewforTip)
                        
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(imgForward)
                        viewforTip.addSubview(lblSubTitle)
        })
    }
}
