//
//  UserChatScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 05/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class UserChatScreen: UIViewController ,UITableViewDataSource,UITableViewDelegate
{
    
    var app=UIApplication.shared.delegate as! AppDelegate
    
    var lblHeight:CGFloat!
    var subView:UIView!
    var detailView:UIView!
    
    var imgPerson:UIImageView!
    var txtSearch:UITextView!
    
    var btnBack = UIButton()
    var btnQuestion:UIButton!
    var btnSend:UIButton!
    
    var lblTitle:UILabel!
    var lblDetail:UILabel!
    var lblName:UILabel!
    var lblTime:UILabel!
    
    var arr_Data:NSMutableArray!
    var arr_Name:NSMutableArray!
    
    var tblData:UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden=true
        self.view.backgroundColor=UIColor.init(red: 63/255.0, green: 63/255.0, blue: 63/255.0, alpha: 1.0)
        self.loadArray()
        self.loadInitialUI()
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadArray()
    {
        arr_Data=["This method is called after the view controller has loaded its view hierarchy into memory."," You usually override this method to perform additional initialization on views that were loaded from nib files."]
        arr_Name=["Jay Garrick","Mike Brown"]
    }
    
    //MARK: LoadInitial UI
    func loadInitialUI()
    {
        lblTitle=UILabel(frame: CGRect(x:30, y: 5, width: Constant.GlobalConstants.screenWidth-60, height: 20))
        lblTitle.backgroundColor=UIColor.clear
        lblTitle.text="Chat"
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        lblTitle.textAlignment = .center
        lblTitle.font=UIFont(name: "Helvetica", size: 22)
        
        self.view.addSubview(lblTitle)
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack.backgroundColor=UIColor.clear
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        self.view.addSubview(btnBack)
        
        btnQuestion=UIButton(frame: CGRect(x: Constant.GlobalConstants.screenWidth-30, y: 10, width: 20, height: 20))
        btnQuestion.setImage(UIImage(named:"icon_Help"), for: UIControlState.normal)
        btnQuestion.backgroundColor=UIColor.clear
        // btnBack.addTarget(self, action: #selector(btnBackClicked), for: UIControlEvents.touchUpInside)
        self.view.addSubview(btnQuestion)
        
        tblData=UITableView(frame: CGRect(x: 0, y: 40, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-100))
        tblData.backgroundColor=UIColor.clear
        tblData.delegate=self
        tblData.dataSource=self
        tblData.separatorStyle=UITableViewCellSeparatorStyle.none
        self.view.addSubview(tblData)
        
        txtSearch=UITextView(frame: CGRect(x: 10, y: Constant.GlobalConstants.screenHeight-40, width: Constant.GlobalConstants.screenWidth-100, height: 30))
        txtSearch.backgroundColor=UIColor.white
        txtSearch.layer.cornerRadius=txtSearch.frame.size.height/2
        txtSearch.layer.borderWidth = 1.0
        txtSearch.layer.borderColor = UIColor.lightGray.cgColor
        self.view.addSubview(txtSearch)
        
        btnSend=UIButton(frame: CGRect(x: Constant.GlobalConstants.screenWidth-80, y: Constant.GlobalConstants.screenHeight-40, width: 70, height: 30))
        btnSend.setTitle("Send", for: .normal)
        btnSend.backgroundColor=UIColor.init(red: 0/255.0, green: 70/255.0, blue: 117/255.0, alpha: 1.0)
        btnSend.titleLabel?.font=UIFont(name: "Helvetica-Light", size: 15)
        btnSend.layer.cornerRadius=5
        btnSend.setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(btnSend)
    }
    
    //MARK: Button Pressed delegate declarations here
    func btn_back_action(sender: UIButton!)
    {
        navigationController?.popViewController(animated:true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        
        subView=UIView(frame:CGRect(x: 0, y: 50, width: Constant.GlobalConstants.screenWidth, height:50))
        subView.backgroundColor=UIColor.clear
        subView.layer.cornerRadius=10
        subView.layer.masksToBounds = true
        cell.addSubview(subView)
        
        lblName=UILabel(frame: CGRect(x: 20, y: 5, width: Constant.GlobalConstants.screenWidth-20, height: 20))
        lblName.backgroundColor = .white
        lblName.text=arr_Name.object(at: indexPath.row)as? String
        lblName.textColor = .black
        lblName.textAlignment = .center
        lblName.font=UIFont(name: "Helvetica-Light", size: 15)
        subView.addSubview(lblName)
        
        lblTime=UILabel(frame: CGRect(x: lblName.frame.maxX, y: 5, width: Constant.GlobalConstants.screenWidth-20, height: 20))
        lblTime.text="13min ago"
        lblTime.font=UIFont(name: "Helvetica-Light", size: 12)
        lblTime.backgroundColor=UIColor.clear
        lblTime.textAlignment = .center
        lblTime.textColor=UIColor.white
        subView.addSubview(lblTime)
        
        detailView=UIView(frame: CGRect(x: 10, y: lblName.frame.maxY, width: subView.frame.size.width-10, height: 50))
        detailView.layer.cornerRadius=7
        subView.addSubview(detailView)
        
        lblDetail = UILabel(frame: CGRect(x: 20, y:lblName.frame.maxY, width:detailView.frame.size.width-50, height:0))
        lblDetail.text = arr_Data.object(at: indexPath.row)as? String
        lblDetail.numberOfLines=0
        lblDetail.font=UIFont(name: "Helvetica-Light", size: 15)
        lblDetail.textColor=UIColor.white
        lblDetail.sizeToFit()
        
        //Here we calculate the label height
        
        let labelWidth = lblDetail.frame.width
        let maxLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        let actualLabelSize = lblDetail.text!.boundingRect(with: maxLabelSize, options: [.usesLineFragmentOrigin], attributes: [NSFontAttributeName: lblDetail.font], context: nil)
        lblHeight = actualLabelSize.height
        view.layer.masksToBounds = true;
        detailView.addSubview(lblDetail)
        
        imgPerson=UIImageView(frame: CGRect(x: 10, y: 5, width: 80, height: 80))
        imgPerson.image=UIImage(named: "img_Profile")
        imgPerson.layer.cornerRadius=imgPerson.frame.size.height/2-15
        imgPerson.clipsToBounds=true
        subView.addSubview(imgPerson)
        
        if indexPath.row == 0 || indexPath.row == 2
        {
            subView.frame=CGRect(x: 0, y: 50, width: Constant.GlobalConstants.screenWidth, height:50)
            lblName.frame=CGRect(x: 20, y: 5, width: subView.frame.size.width/2, height: 20)
            lblTime.frame=CGRect(x: lblName.frame.maxX, y: 5, width: subView.frame.size.width/2-30, height: 20)
            lblDetail.backgroundColor=UIColor.clear
            imgPerson.frame=CGRect(x: 10, y: 0, width: 50, height: 50)
            detailView.frame=CGRect(x: 20, y: lblName.frame.maxY, width: subView.frame.size.width-40, height:lblDetail.frame.maxY+10)
            detailView.backgroundColor = .gray
            subView.frame=CGRect(x: 0, y: 50, width: Constant.GlobalConstants.screenWidth, height:detailView.frame.maxY+10)
        }
        else
        {
            subView.frame=CGRect(x: 0, y:50, width: Constant.GlobalConstants.screenWidth, height:50)
            lblName.frame=CGRect(x: subView.frame.size.width/2-30, y: 5, width: subView.frame.size.width/2, height: 20)
            lblTime.frame=CGRect(x: 20, y: 5, width: subView.frame.size.width/2-30, height: 20)
            lblDetail.backgroundColor=UIColor.clear
            imgPerson.frame=CGRect(x: subView.frame.size.width-60, y: 0, width: 50, height: 50)
            detailView.frame=CGRect(x: 20, y: lblName.frame.maxY, width: subView.frame.size.width-40, height:lblDetail.frame.maxY+10)
            detailView.backgroundColor=UIColor.init(red: 0/255.0, green: 70/255.0, blue: 117/255.0, alpha: 1.0)
            subView.frame=CGRect(x: 0, y: 50, width: Constant.GlobalConstants.screenWidth, height:detailView.frame.maxY+10)
        }
        
        cell.backgroundColor=UIColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if lblHeight == nil
        {
            return 40
        }
        else
        {
            return subView.frame.size.height+50
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
