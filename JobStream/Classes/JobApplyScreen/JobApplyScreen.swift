//
//  JobApplyScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 29/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class JobApplyScreen: UIViewController {

    var imgJob = UIImageView()
    var viewAVBG = UIView()
    var lblTitle = UILabel()
    var lblSubTitle = UILabel()
    var lblCompName = UILabel()
    var viewBG = UIView()
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var lblAvailable = UILabel()
    var lblStatus = UILabel()
    var btnApply = UIButton()
    
    var lblAbout = UILabel()
    var lblAbtDtls = UILabel()
    
    var lblTDesc = UILabel()
    var lblDesc = UILabel()
    
    var lblPosition = UILabel()
    var lblPostDesc = UILabel()
    
    var lblCompensation = UILabel()
    var lblComptionDesc = UILabel()

    var SelectedJobID = NSString()
    var strPosition = NSString()
    var strCompName = NSString()
    var strImg = NSString()
    var strCatName = NSString()
    var strSubName = NSString()
    
    var viewForAlert = UIView()
    var isgetJInfo:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadInitialUI()
        getJobInfoAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadInitialUI()
    {
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        lblTitle.frame=CGRect(x: 30, y: 3, width: Constant.GlobalConstants.screenWidth-60,height: 30)
        lblTitle.text=""
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont(name:"Helvetica", size: 22)
        lblTitle.textColor = UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        self.view.addSubview(lblTitle)
        
        lblSubTitle.frame=CGRect(x: 30, y: lblTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth-60,height: 30)
        lblSubTitle.text=""
        lblSubTitle.textAlignment = .center
        lblSubTitle.font = UIFont(name:"Helvetica-Light", size: 18)
        lblSubTitle.textColor = UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        self.view.addSubview(lblSubTitle)
        
        viewBG.frame=CGRect(x: 5, y: lblSubTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth-10,height: Constant.GlobalConstants.screenHeight-70)
        viewBG.layer.borderWidth=1.0
        viewBG.layer.borderColor = UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7).cgColor
        viewBG.layer.cornerRadius=5.0
        viewBG.backgroundColor = UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewBG)
        
        imgJob.frame=CGRect(x: 5, y: 5, width: Constant.GlobalConstants.screenWidth/2-30,height: 100)
        imgJob.image = UIImage(named: "icom_job")
        imgJob.layer.cornerRadius = 5.0
        imgJob.layer.borderColor = UIColor.white.cgColor
        imgJob.layer.borderWidth = 1.0
        viewBG.addSubview(imgJob)
        
        lblCompName.frame=CGRect(x: imgJob.frame.maxX+5, y: 5, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        lblCompName.text="Company Name"
        lblCompName.textColor = UIColor.white
        lblCompName.textAlignment = .left
        lblCompName.font = UIFont(name:"Helvetica", size: 16)
        viewBG.addSubview(lblCompName)
        
        
        lblStatus.frame=CGRect(x: imgJob.frame.maxX+5, y: lblCompName.frame.maxY-5, width: 150, height: 30)
        lblStatus.text="Position"
        lblStatus.font =  UIFont(name:"Helvetica", size: 18)
        lblStatus.textAlignment = .left
        lblStatus.textColor=UIColor.white
        viewBG.addSubview(lblStatus)
        
       
        viewAVBG.frame=CGRect(x: imgJob.frame.maxX, y: lblStatus.frame.maxY, width: (viewBG.frame.size.width-imgJob.frame.size.width)-5, height: 30)
        viewAVBG.backgroundColor=UIColor(red: 26.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 1.0)
        viewBG.addSubview(viewAVBG)
        
        lblAvailable.frame=CGRect(x: 5, y: 0, width: viewAVBG.frame.size.width-5, height: 30)
        lblAvailable.text=""
        lblAvailable.textColor=UIColor.white
        lblAvailable.font = UIFont(name:"Helvetica-Bold", size: 16)
        viewAVBG.addSubview(lblAvailable)
        
        lblAvailable.backgroundColor = UIColor(red: 26.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 1.0)
        
        btnApply.frame=CGRect(x: Constant.GlobalConstants.screenWidth-100, y: 5, width: 90, height: 35)
        btnApply .setTitle("Apply", for: .normal)
        btnApply.layer.cornerRadius=4.0
        btnApply.backgroundColor = UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        lblAbout.font=UIFont(name:"Helvetica", size: 14)
        btnApply.addTarget(self, action:#selector(self.btnApplyPressed), for:.touchUpInside)
        self.view .addSubview(btnApply)
        
        /*------------------        About Title      -----------------------*/
        lblAbout.frame=CGRect(x: 5, y: imgJob.frame.maxY, width: 100, height: 30)
        lblAbout.text="About:"
        lblAbout.textColor=UIColor.white
        lblAbout.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblAbout)
        
        lblAbtDtls.frame=CGRect(x: 5, y: lblAbout.frame.maxY-5, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        lblAbtDtls.text=""
        lblAbtDtls.textColor=UIColor.white
        lblAbtDtls.font=UIFont(name:"Helvetica", size: 12)
        lblAbtDtls.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblAbtDtls.numberOfLines = 0
        viewBG.addSubview(lblAbtDtls)
        
                /*------------------        Description Title      -----------------------*/
        
        lblTDesc.frame=CGRect(x: 5, y: lblAbtDtls.frame.maxY, width: 100, height: 30)
        lblTDesc.text="Description:"
        lblTDesc.textColor=UIColor.white
        lblTDesc.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblTDesc)
        
        lblDesc.frame=CGRect(x: 5, y: lblTDesc.frame.maxY-5, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        lblDesc.text=""
        
        lblDesc.textColor=UIColor.white
        lblDesc.font=UIFont(name:"Helvetica", size: 12)
        lblDesc.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDesc.numberOfLines = 0
        viewBG.addSubview(lblDesc)
                    /*------------------        Poistion Title      -----------------------*/
        
        lblPosition.frame=CGRect(x: 5, y: lblDesc.frame.maxY, width: 100, height: 30)
        lblPosition.text="Position:"
        lblPosition.textColor=UIColor.white
        lblPosition.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblPosition)
        
        lblPostDesc.frame=CGRect(x: 5, y: lblPosition.frame.maxY-5, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        lblPostDesc.text=""
        
        lblPostDesc.textColor=UIColor.white
        lblPostDesc.font=UIFont(name:"Helvetica", size: 12)
        lblPostDesc.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblPostDesc.numberOfLines = 0
        viewBG.addSubview(lblPostDesc)
        
        /*------------------        Compensation Title      -----------------------*/
        lblCompensation.frame=CGRect(x: 5, y: lblPostDesc.frame.maxY, width: 130, height: 30)
        lblCompensation.text="Compensation:"
        lblCompensation.textColor=UIColor.white
        lblCompensation.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblCompensation)
        
        lblComptionDesc.frame=CGRect(x: 5, y: lblCompensation.frame.maxY-5, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        lblComptionDesc.text=""
        
        lblComptionDesc.textColor=UIColor.white
        lblComptionDesc.font=UIFont(name:"Helvetica", size: 12)
        lblComptionDesc.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblComptionDesc.numberOfLines = 0
        viewBG.addSubview(lblComptionDesc)
    }
    
    //MARK: UIButton Pressed delegate declarations here
    
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    
    func btnApplyPressed(sender: UIButton!)
    {
        ApplyToJobAPI()
    }
    //MARK: ViewForHelpView
    func viewForAlertUI()
    {
        viewForAlert.frame=CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0)
        let whte = UIColor.black
        viewForAlert.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForAlert)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseSavePressed))
        viewForAlert.addGestureRecognizer(tap)
        viewForAlert.isUserInteractionEnabled = true
        
        var frame1 = CGRect()
        
        frame1=viewForAlert.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        let lblRTitle = UILabel()
                        let imgLine = UIImageView()
                        let imgRIcon = UIImageView()
                        let imgCIcon = UIImageView()
                        
                       
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-100
                            , width: Constant.GlobalConstants.screenWidth-60, height: 200)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 0, y: 15, width: viewforTip.frame.size.width, height: 30)
                        lblTitle.text="Resume Sent"
                        lblTitle.font=UIFont(name:"Helvetica", size: 18)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.frame=CGRect(x: 0, y: lblTitle.frame.maxY-5, width: viewforTip.frame.size.width, height: 30)
                        lblSubTitle.text="Your resume has been sent"
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 14)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        
                        imgLine .removeFromSuperview()
                        imgLine.frame=CGRect(x: 20, y: lblSubTitle.frame.maxY+25, width: viewforTip.frame.size.width-40, height: 1)
                        imgLine.backgroundColor=UIColor.black
                        
                        imgRIcon.removeFromSuperview()
                        imgRIcon.frame=CGRect(x: 30, y: imgLine.frame.maxY+20, width: 30, height: 35)
                        imgRIcon.image=UIImage(named: "icon_GResume")
                        
                        lblRTitle.frame=CGRect(x: 0, y: imgLine.frame.maxY+25, width: viewforTip.frame.size.width, height: 30)
                        lblRTitle.text="Resume Name"
                        lblRTitle.font=UIFont(name:"Helvetica", size: 14)
                        lblRTitle.textAlignment=NSTextAlignment.center
                        
                        imgCIcon .removeFromSuperview()
                        imgCIcon.frame=CGRect(x: viewforTip.frame.size.width-60, y: imgLine.frame.maxY+27, width: 27, height: 20)
                        imgCIcon.image=UIImage(named: "icon_CheckMark")
                        
                       self.viewForAlert.addSubview(viewforTip)
                        
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)
                        viewforTip.addSubview(imgLine)
                        viewforTip.addSubview(imgRIcon)
                        viewforTip.addSubview(lblRTitle)
                        viewforTip.addSubview(imgCIcon)
        })
    }
    
    //MARK: UIButton Help Pressed
    func btnCloseSavePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForAlert .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForAlert.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForAlert.frame=frameViewForHelp
            self.navigationController?.popViewController(animated:true)
        })
    }

    //MARK: Apply to specific job API
    func ApplyToJobAPI()
    {
        isgetJInfo=false
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((SelectedJobID), forKey: "jid" as NSCopying)
        
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjobapplied.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    func getJobInfoAPI()
    {
        isgetJInfo=true
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
       
        let bodydata = NSMutableDictionary()
        bodydata.setObject((SelectedJobID), forKey: "jid" as NSCopying)
        
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        let objc = WebApiController()
        objc.callAPI_POST("api/wsgetjob_info.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                if isgetJInfo
                {
                    var strStatus = NSString()
                    strStatus=(ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "job_status")as!NSString
                    
                    lblAbtDtls.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "about")as!NSString) as String
                    
                    lblDesc.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "description")as!NSString) as String
                    
                    lblPostDesc.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "position")as!NSString) as String
                    
                    lblComptionDesc.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "compensation")as!NSString) as String
                    
                      lblTitle.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "catname")as!NSString) as String
                      lblSubTitle.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "subcatname")as!NSString) as String
                      lblCompName.text=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "company_name")as!NSString) as String                    
                    
                    lblAvailable.text=strStatus as String
                    
                    var strImage = ""
                    var strImgURL = Constant.GlobalConstants.Imgbase_URL
                    
                    imgJob.layer.borderColor=UIColor.white.cgColor
                    imgJob.layer.borderWidth=1.0
                    
                    strImage=((ResponceData.value(forKey: "info") as!NSDictionary).value(forKey: "logo")as!NSString) as String
                    if strImage == "no"
                    {
                        imgJob.image=UIImage(named: "icom_job")
                        
                    }
                    else
                    {
                        strImgURL = strImgURL+strImage
                        imgJob.setImageWith(NSURL(string: strImgURL) as URL!, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.white)
                        
                    }
                }
                else
                {
                    viewForAlertUI()
                }
                
            }
            else
            {
                if isgetJInfo
                {
                
                }
                else
                {
                    if strMSG1 == "profileIncomplete"
                    {
                        let alertMessage = UIAlertController(title: "Warning!", message: "Please complete your profile" as String, preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                    else if strMSG1 == "already_applied"
                    {
                        let alertMessage = UIAlertController(title: "Sorry!", message: "Already applied " as String, preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                    else
                    {
                        let alertMessage = UIAlertController(title: "Sorry!", message: "Failed to apply" as String, preferredStyle: .alert)
                        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                
                }
            }
            
        }
        catch
        {
            print("Error")
        }
        
    }
}
