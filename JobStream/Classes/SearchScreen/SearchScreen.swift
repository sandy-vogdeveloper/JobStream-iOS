//
//  SearchScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 04/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class SearchScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var btnBack = UIButton()
    var btnBack1 = UIButton()

    var btnNerByResume = UIButton()
    var btnSBLocation = UIButton()
    var btnSBName = UIButton()
    var btnSBCategory = UIButton()
    var btnFindMore = UIButton()
    var btnBtmNotification = UIButton()
    
    var imgNerByResume = UIImageView()
    var imgSBLocation = UIImageView()
    var imgSBName = UIImageView()
    var imgSBCategory = UIImageView()
    
    var viewForControls = UIView()
    var viewForCategory = UIView()
    
    var isResumePressed = Bool()
    var isCategoryPressed = Bool()
    var isNamePressed = Bool()
    
    var tblResume = UITableView()
    
    var viewForNotification = UIView()
    var btnNotiSetting = UIButton()
    var imgDot = UIImageView()
    var lblNoti = UILabel()
    var tblNotifications = UITableView()
    var isopen = Bool()
    
    var app=UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadMainUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Load MainUI
    func loadMainUI()
    {
        isResumePressed = false
        isCategoryPressed = false
        isNamePressed = false
        
        btnBack.frame=CGRect(x: 3, y: 5, width: 18,height: 18)
        let image = UIImage(named: "icon_BackMove") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        btnNerByResume .removeFromSuperview()
        btnNerByResume .removeFromSuperview()
        btnSBLocation .removeFromSuperview()
        btnSBName .removeFromSuperview()
        btnSBCategory .removeFromSuperview()
        
        btnNerByResume.frame=CGRect(x: 0, y: UIScreen.main.bounds.size.height/100*20, width: Constant.GlobalConstants.screenWidth, height: 40)
        btnSBLocation.frame=CGRect(x: 0, y: btnNerByResume.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        btnSBName.frame=CGRect(x: 0, y: btnSBLocation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        btnSBCategory.frame=CGRect(x: 0, y: btnSBName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        
        
        if app.strSelectedUser == "employee"
        {
            btnNerByResume .setTitle("  Near by Jobs", for: .normal)
        }
        else
        {
            btnNerByResume .setTitle("  Near by Resume", for: .normal)
            btnNerByResume.addTarget(self, action:#selector(self.btnNBResumePressed), for:.touchUpInside)
        }
        
        btnSBLocation .setTitle("  Search by Location", for: .normal)
        btnSBName .setTitle("  Search by Name", for: .normal)
        btnSBCategory .setTitle("  Search by Category", for: .normal)
        
        btnNerByResume .setTitleColor(UIColor.black, for: .normal)
        btnSBLocation.setTitleColor(UIColor.black, for: .normal)
        btnSBName.setTitleColor(UIColor.black, for: .normal)
        btnSBCategory.setTitleColor(UIColor.black, for: .normal)

        btnNerByResume.titleLabel?.font=UIFont(name:"Helvetica-Light", size: 18)
        btnSBLocation.titleLabel?.font=UIFont(name:"Helvetica-Light", size: 18)
        btnSBName.titleLabel?.font=UIFont(name:"Helvetica-Light", size: 18)
        btnSBCategory.titleLabel?.font=UIFont(name:"Helvetica-Light", size: 18)
        
        btnNerByResume.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        btnSBLocation.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        btnSBName.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        btnSBCategory.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        
        btnNerByResume.contentHorizontalAlignment = .left;
        btnSBLocation.contentHorizontalAlignment = .left;
        btnSBName.contentHorizontalAlignment = .left;
        btnSBCategory.contentHorizontalAlignment = .left;
        
        self.view.addSubview(btnNerByResume)
        self.view.addSubview(btnSBLocation)
        self.view.addSubview(btnSBName)
        self.view.addSubview(btnSBCategory)
        
        imgNerByResume.frame=CGRect(x: btnNerByResume.frame.size.width-40, y: 10, width: 20, height: 20)
        imgSBLocation.frame=CGRect(x: btnSBLocation.frame.size.width-40, y: 10, width: 20, height: 20)
        imgSBName.frame=CGRect(x: btnSBName.frame.size.width-40, y: 10, width: 20, height: 20)
        imgSBCategory.frame=CGRect(x: btnSBCategory.frame.size.width-40, y: 10, width: 20, height: 20)
        //icon_DropDown
        imgNerByResume.image=UIImage(named: "icon_DropDown")
        imgSBLocation.image=UIImage(named: "icon_DropDown")
        imgSBName.image=UIImage(named: "icon_DropDown")
        imgSBCategory.image=UIImage(named: "icon_DropDown")
        
        btnNerByResume .addSubview(imgNerByResume)
        btnSBLocation.addSubview(imgSBLocation)
        btnSBName.addSubview(imgSBName)
        btnSBCategory.addSubview(imgSBCategory)
        
        
        btnSBLocation.addTarget(self, action:#selector(self.btnSLocationPressed), for:.touchUpInside)
        btnSBCategory.addTarget(self, action:#selector(self.btnSBCategoryPressed), for:.touchUpInside)
        
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width: Constant.GlobalConstants.screenWidth, height: 30)
        btnBtmNotification.setImage(UIImage(named:"icon_BottomImg"), for: UIControlState.normal)
        btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-5, y: 10, width: 10, height: 10)
        imgDot.layer.cornerRadius=imgDot.frame.size.height/2
        imgDot.backgroundColor = UIColor.white
        btnBtmNotification.addSubview(imgDot)
    }
    
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    
    func btnNBResumePressed(sender: UIButton!) {
        
        if isResumePressed
        {
            isResumePressed=false
            viewForControls .removeFromSuperview()
            viewForCategory.removeFromSuperview()
            
            btnSBLocation.frame=CGRect(x: 0, y: btnNerByResume.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBName.frame=CGRect(x: 0, y: btnSBLocation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBCategory.frame=CGRect(x: 0, y: btnSBName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            isResumePressed=true
            viewForControls .removeFromSuperview()
            viewForCategory.removeFromSuperview()
            
            viewForControls.frame=CGRect(x: 0, y: btnNerByResume.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 170)
            viewForControls.backgroundColor=UIColor.white.withAlphaComponent(0.7)
            self.view.addSubview(viewForControls)
            btnNerByResume.frame=CGRect(x: 0, y: UIScreen.main.bounds.size.height/100*20, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBLocation.frame=CGRect(x: 0, y: viewForControls.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBName.frame=CGRect(x: 0, y: btnSBLocation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBCategory.frame=CGRect(x: 0, y: btnSBName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            
            tblResume.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height: viewForControls.frame.size.height-30)
            tblResume.dataSource = self
            tblResume.delegate = self
            tblResume.tableFooterView=UIView()
            tblResume.backgroundColor=UIColor.clear
            viewForControls.addSubview(tblResume)
            
            btnFindMore.frame=CGRect(x: 0, y: tblResume.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
            btnFindMore .setTitle("Find more applicants  ", for: .normal)
            btnFindMore.setTitleColor(UIColor.white, for: .normal)
            btnFindMore.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            btnFindMore.contentHorizontalAlignment = .right;
            btnFindMore.titleLabel?.font=UIFont(name:"Helvetica-Light", size: 16)
            viewForControls.addSubview(btnFindMore)
        }
    }
    
    func btnSLocationPressed(sender: UIButton!)
    {
        let MS = MapScreen(nibName: "MapScreen", bundle: nil)
        self.navigationController?.pushViewController(MS, animated: true)
    }
    
    func btnSBNamePressed(sender: UIButton!) {
        
        if isNamePressed {
            
        }
        else
        {
            
        }
    }
    
    func btnSBCategoryPressed(sender: UIButton!) {
        if isCategoryPressed
        {
            self.isCategoryPressed=false
            self.viewForControls .removeFromSuperview()
            self.viewForCategory .removeFromSuperview()
            tblResume.removeFromSuperview()
            
            btnNerByResume.frame=CGRect(x: 0, y: UIScreen.main.bounds.size.height/100*20, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBLocation.frame=CGRect(x: 0, y: btnNerByResume.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBName.frame=CGRect(x: 0, y: btnSBLocation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBCategory.frame=CGRect(x: 0, y: btnSBName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
        }
        else
        {
            self.isCategoryPressed=true
            self.viewForControls .removeFromSuperview()
            self.viewForCategory .removeFromSuperview()
            tblResume.removeFromSuperview()
            
            btnNerByResume.frame=CGRect(x: 0, y: UIScreen.main.bounds.size.height/100*20, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBLocation.frame=CGRect(x: 0, y: btnNerByResume.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBName.frame=CGRect(x: 0, y: btnSBLocation.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            btnSBCategory.frame=CGRect(x: 0, y: btnSBName.frame.maxY+10, width: Constant.GlobalConstants.screenWidth, height: 40)
            self.viewForCategory.frame=CGRect(x: 0, y: btnSBCategory.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 180)
            viewForCategory.backgroundColor=UIColor.white.withAlphaComponent(0.7)
            self.view.addSubview(viewForCategory)
            
            let btn24 = UIButton()
            let btn12 = UIButton()
            let btnUrgent = UIButton()
            let btnIntership = UIButton()
            let btnVolunter = UIButton()
           
            let lbl24 = UILabel()
            let lbl12 = UILabel()
            let lblUrgent = UILabel()
            let lblIntership = UILabel()
            let lblVolunter = UILabel()
           
            
            btn24 .removeFromSuperview()
            btn24.frame=CGRect(x: 25, y: 5, width: 40, height: 40)
            btn24.setImage(UIImage(named:"icon_Clock"), for: .normal)
            btn24.backgroundColor = .clear
            viewForCategory.addSubview(btn24)
            
            btn12 .removeFromSuperview()
            btn12.frame=CGRect(x: viewForCategory.frame.size.width/2-27, y: 5, width: 40, height: 40)
            btn12.setImage(UIImage(named:"icon_Clock"), for: .normal)
            btn12.backgroundColor = .clear
            viewForCategory.addSubview(btn12)
            
            btnUrgent .removeFromSuperview()
            btnUrgent.frame=CGRect(x: viewForCategory.frame.size.width-68, y: 5, width: 40, height: 40)
            btnUrgent.setImage(UIImage(named:"icon_Alert"), for: .normal)
            btnUrgent.backgroundColor = .clear
            viewForCategory.addSubview(btnUrgent)
            
            btnIntership.removeFromSuperview()
            btnIntership.frame=CGRect(x: viewForCategory.frame.size.width/2-80, y: btnUrgent.frame.maxY+27, width: 45, height: 35)
            btnIntership.setImage(UIImage(named:"icon_Cap"), for: .normal)
            btnIntership.backgroundColor = .clear
            viewForCategory.addSubview(btnIntership)
            
            btnVolunter .removeFromSuperview()
            btnVolunter.frame=CGRect(x: viewForCategory.frame.size.width/2+50, y: btnUrgent.frame.maxY+27, width: 30, height: 35)
            btnVolunter.setImage(UIImage(named:"icon_volunter"), for: .normal)
            //btnVolunter.addTarget(self, action:#selector(self.btncreateNewRPressed), for:.touchUpInside)
            btnVolunter.backgroundColor = .clear
            viewForCategory.addSubview(btnVolunter)
           
            lbl24.frame=CGRect(x: 0, y: btn24.frame.maxY+2, width: viewForCategory.frame.size.width/3, height: 30)
            lbl24.text="Full Time"
            lbl24.textAlignment=NSTextAlignment.center
            lbl24.font =  UIFont(name:"Helvetica-Light", size: 14)
            lbl24.textColor=UIColor.gray
            viewForCategory.addSubview(lbl24)
            
            lbl12.frame=CGRect(x: lbl24.frame.maxX, y: btn24.frame.maxY+2, width: viewForCategory.frame.size.width/3, height: 30)
            lbl12.text="Part Time"
            lbl12.textAlignment=NSTextAlignment.center
            lbl12.font =  UIFont(name:"Helvetica-Light", size: 14)
            lbl12.textColor=UIColor.gray
            viewForCategory.addSubview(lbl12)
            
            lblUrgent.frame=CGRect(x: lbl12.frame.maxX, y: btn24.frame.maxY+2, width: viewForCategory.frame.size.width/3, height: 30)
            lblUrgent.text="Urgent shift"
            lblUrgent.textAlignment=NSTextAlignment.center
            lblUrgent.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblUrgent.textColor=UIColor.gray
            viewForCategory.addSubview(lblUrgent)
            
            lblIntership.frame=CGRect(x: btnIntership.frame.maxX-55, y: btnIntership.frame.maxY, width: 75, height: 20)
            lblIntership.text="Interships"
            lblIntership.textAlignment=NSTextAlignment.center
            lblIntership.textColor=UIColor.gray
            lblIntership.font =  UIFont(name:"Helvetica-Light", size: 14)
            
            viewForCategory.addSubview(lblIntership)
            
            lblVolunter.frame=CGRect(x: btnVolunter.frame.maxX-55, y: btnVolunter.frame.maxY, width: 80, height: 20)
            lblVolunter.text="Volunteer"
            lblVolunter.textAlignment=NSTextAlignment.center
            lblVolunter.font =  UIFont(name:"Helvetica-Light", size: 14)
            lblVolunter.textColor=UIColor.gray
            viewForCategory.addSubview(lblVolunter)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView==tblNotifications
        {
            return 5
        }
        else
        {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let lblTitle = UILabel()
        let lblType = UILabel()
        let imgProfile = UIImageView()
        let btnResume = UIButton()
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblResume.separatorStyle = .none
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 66)
        viewForCell.backgroundColor=UIColor.white.withAlphaComponent(0.6)
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        
        if tableView==tblNotifications
        {
            viewForCell.backgroundColor=UIColor.init(red: 222/255.0, green: 230/255.0, blue: 236/255.0, alpha: 1)
            imgProfile .removeFromSuperview()
            imgProfile.frame=CGRect(x: 5, y: 25, width: 10, height: 10)
            imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
            imgProfile.backgroundColor = UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            viewForCell.addSubview(imgProfile)
            
            let lblTitle = UILabel()
            lblTitle.frame=CGRect(x: 20, y: 5, width: Constant.GlobalConstants.screenWidth-20, height:50)
            lblTitle.text="A new job has been posted in \n [Homestream Category] Apply Now"
            lblTitle.textColor=UIColor.black
            lblTitle.font=UIFont(name:"Helvetica-Light", size: 14)
            lblTitle.textAlignment=NSTextAlignment.left
            lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblTitle.numberOfLines = 0
            viewForCell.addSubview(lblTitle)
            
            let lblTime = UILabel()
            lblTime.frame=CGRect(x: viewForCell.frame.size.width-40, y: 0, width: 40, height: 25)
            lblTime.text="1.35 pm"
            lblTime.textAlignment = .left
            lblTime.font=UIFont(name:"Helvetica-Light", size: 10)
            lblTime.textColor=UIColor.lightGray
            viewForCell.addSubview(lblTime)
        }
        else
        {
            imgProfile .removeFromSuperview()
            imgProfile.frame=CGRect(x: 5, y: 13, width: 40, height: 40)
            imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
            imgProfile.image = UIImage(named:"img_Profile")
            viewForCell.addSubview(imgProfile)
        
            lblTitle .removeFromSuperview()
            lblTitle.frame=CGRect(x: imgProfile.frame.maxX+5, y: 3, width: 120, height: 30)
            lblTitle.text="Jay Garrick"
            lblTitle.textColor=UIColor.black
            lblTitle.font=UIFont(name:"Helvetica", size: 16)
            viewForCell.addSubview(lblTitle)
        
            lblType .removeFromSuperview()
            lblType.frame=CGRect(x: imgProfile.frame.maxX+5, y: lblTitle.frame.maxY, width: 120, height: 20)
            lblType.text="Calgray AB"
            lblType.textColor=UIColor.gray
            lblType.font=UIFont(name:"Helvetica-Light", size: 13)
            viewForCell.addSubview(lblType)
        
            btnResume .removeFromSuperview()
            btnResume.frame=CGRect(x: Constant.GlobalConstants.screenWidth-50, y: 13, width: 40, height: 40)
            btnResume.setBackgroundImage(UIImage(named: "icon_resume"), for: UIControlState.normal)
            viewForCell .addSubview(btnResume)
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 67
    }
    
    //MARK: ViewFor Bottom NotificationUI
    func viewforNotificationUI()
    {
        if isopen
        {
            isopen=false
            var frameViewForHelp = CGRect()
            frameViewForHelp=self.viewForNotification.frame;
            frameViewForHelp=CGRect(x: 0,y: Constant.GlobalConstants.screenHeight,width: Constant.GlobalConstants.screenWidth,height: 0);
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseOut,
                           animations: {
                            self.viewForNotification.frame=frameViewForHelp
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                            
                            
            },completion: { finished in
                self.viewForNotification.frame=frameViewForHelp
                self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                self.imgDot.frame=CGRect(x: self.btnBtmNotification.frame.size.width/2-5, y: 10, width: 10, height: 10)
                self.tblNotifications .removeFromSuperview()
                self.lblNoti.removeFromSuperview()
                self.btnNotiSetting.removeFromSuperview()
            })
        }
        else
        {
            isopen=true
            self.viewForNotification.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
            self.viewForNotification.backgroundColor=UIColor.clear
            var frame1 = CGRect()
            
            frame1=viewForNotification.frame
            frame1=CGRect(x:0,y:self.view.frame.size.height/2,width:self.view.frame.size.width ,height: self.view.frame.size.height/2)
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseIn,
                           animations: {
                            self.viewForNotification.frame=frame1
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight/2-40, width:Constant.GlobalConstants.screenWidth, height: 40)
                            self.imgDot.frame=CGRect(x: self.btnBtmNotification.frame.size.width/2-5, y: 7, width: 10, height: 10)
                            self.lblNoti = UILabel()
                            self.lblNoti.frame=CGRect(x: 0, y: 20, width: Constant.GlobalConstants.screenWidth, height:20)
                            self.lblNoti.text="Notifications"
                            self.lblNoti.textAlignment=NSTextAlignment.center
                            self.lblNoti.textColor=UIColor.white
                            self.lblNoti.font=UIFont(name:"Helvetica-Light", size: 12)
                            self.btnBtmNotification .addSubview(self.lblNoti)
                            
            },completion: { finished in
                
                self.btnNotiSetting.frame=CGRect(x: Constant.GlobalConstants.screenWidth-30, y: 10, width: 25, height: 25)
                self.btnNotiSetting.setImage(UIImage(named:"icon_Setting"), for: UIControlState.normal)
                self.btnBtmNotification.addSubview(self.btnNotiSetting)
                self.tblNotifications .removeFromSuperview()
                self.tblNotifications.frame=CGRect(x: 0, y: 0, width: self.viewForNotification.frame.size.width, height: self.viewForNotification.frame.size.height)
                self.tblNotifications.dataSource=self
                self.tblNotifications.delegate=self
                self.tblNotifications.tableFooterView=UIView()
                self.tblNotifications.backgroundColor=UIColor.clear
                self.viewForNotification.addSubview(self.tblNotifications)
            })
            
            self.view.addSubview(viewForNotification)
        }
    }

}
