//
//  PaymentSecScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 23/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class PaymentSecScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {

   @IBOutlet var lblTitle1: UILabel!
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var btnIAgree = UIButton()
    var lblIAgree = UILabel()
    
    var viewMain = UIView()
    var imgLogo = UIImageView()
    
    var btnProceed = UIButton()
    var tblInfo = UITableView()
    let arrFields : [String] = ["3 cents per tap when looking for resume","3 centes per tap when employee taps on your \n job posts","3 cents per tap when browsing per resume"]
    
    var strTokenID = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        loadInitialUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    //MARK: load InitialUI
    func loadInitialUI()
    {
        lblTitle1.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        viewMain .removeFromSuperview()
        viewMain.frame=CGRect(x: 0, y: 90, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-60)
        viewMain.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        self.view.addSubview(viewMain)
        
        imgLogo.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-50, y: 40, width: 100, height: 100)
        imgLogo.image=UIImage(named: "icon_TLogo")
        self.view.addSubview(imgLogo)
        
        let lblTitle=UILabel()
        lblTitle.frame=CGRect(x: 20, y: imgLogo.frame.maxY+30, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblTitle.text="Pay Per Tap"
        lblTitle.font=UIFont(name: "Helvetica", size: 18)
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        self.view.addSubview(lblTitle)
        
        let lblFeturs=UILabel()
        lblFeturs.frame=CGRect(x: 20, y: lblTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        lblFeturs.text="Features:"
        lblFeturs.font=UIFont(name: "Helvetica", size: 16)
        lblFeturs.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        self.view.addSubview(lblFeturs)
        
        tblInfo .removeFromSuperview()
        tblInfo.frame=CGRect(x: 0, y: lblFeturs.frame.maxY-5, width: Constant.GlobalConstants.screenWidth, height: 95)
        tblInfo.delegate=self;
        tblInfo.dataSource=self
        tblInfo.tableFooterView=UIView()
        tblInfo.backgroundColor=UIColor.clear
        tblInfo.isScrollEnabled=false
        self.view.addSubview(tblInfo)
        
        let lblTC=UILabel()
        lblTC.frame=CGRect(x: 20, y: tblInfo.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-40, height: 30)
        lblTC.text="Terms and Conditions"
        lblTC.font=UIFont(name: "Helvetica", size: 16)
        lblTC.textAlignment=NSTextAlignment.center
        lblTC.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        self.view.addSubview(lblTC)
        
        btnIAgree.frame=CGRect(x: 20, y: Constant.GlobalConstants.screenHeight-90, width: 20, height: 20)
        btnIAgree.layer.borderWidth=1.0;
        btnIAgree.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        btnIAgree.layer.cornerRadius=4.0
        self.view.addSubview(btnIAgree)
        
        lblIAgree.frame=CGRect(x: btnIAgree.frame.maxX+5, y: Constant.GlobalConstants.screenHeight-92, width: Constant.GlobalConstants.screenWidth-50, height: 30)
        lblIAgree.text="I agree and understand the terms and conditions"
        lblIAgree.font=UIFont(name: "Helvetica", size: 12)
        lblIAgree.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        self.view.addSubview(lblIAgree)
        
        btnProceed .removeFromSuperview()
        btnProceed.frame=CGRect(x: 20, y: Constant.GlobalConstants.screenHeight-50, width: Constant.GlobalConstants.screenWidth-40, height: 35)
        btnProceed .setTitle("Proceed", for: .normal)
        btnProceed.titleLabel?.font=UIFont(name: "Helvetica", size: 18)
        btnProceed.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        btnProceed.layer.cornerRadius=5.0
        btnProceed.addTarget(self, action:#selector(self.btnProceedPressed), for:.touchUpInside)
        self.view.addSubview(btnProceed)
    }
    
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    
    
    func btnProceedPressed(sender: UIButton!)
    {
        let cardParams = STPCardParams()
        
        cardParams.number="4242424242424242"
        cardParams.expMonth = 10;
        cardParams.expYear = 2020;
        cardParams.cvc = "121"
        
        STPAPIClient.shared().createToken(withCard: cardParams, completion: { (token, error) -> Void in
            
            if error != nil {
                //self.handleError(error!)
                print(error?.localizedDescription ?? 0)
                return
            }
            self.strTokenID = (token?.tokenId)!
            print(token?.tokenId ?? 0)
            self.payPaymentAPI()
            // self.postStripeToken(token!)
        })
        //self.viewForAlertUI()
        //let profEdit = ProfileCompleteScreen(nibName: "ProfileCompleteScreen", bundle: nil)
        //self.navigationController?.pushViewController(profEdit, animated: true)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblInfo.separatorStyle = .none
        
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor=UIColor.clear
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        imgBar.frame=CGRect(x: 5,y: 5,width:30,height: 30)
        
        if indexPath.row==1 {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
            imgBar.frame=CGRect(x: 5,y: viewForCell.frame.size.height/2-2.5,width:5,height: 5)
            lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:40)
        }
        else
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 29)
            imgBar.frame=CGRect(x: 5,y: viewForCell.frame.size.height/2-2.5,width:5,height: 5)
            lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:30)
        }
        imgBar.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        imgBar.layer.cornerRadius = imgBar.frame.size.width/2
        imgBar.clipsToBounds = true
        viewForCell .addSubview(imgBar)
        
        lblTitle.text = arrFields [(indexPath as NSIndexPath).row]
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        lblTitle.textAlignment=NSTextAlignment.left
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 12)
        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblTitle.numberOfLines = 0
        viewForCell .addSubview(lblTitle)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row==1
        {
            return 40
        }
        else
        {
            return 30
        }
    }
    
    //MARK: Pay Payment Screen
    func payPaymentAPI()
    {
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((self.strTokenID) , forKey: "stripe_id" as NSCopying)
        
        print("BODY DATA for User Registration : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsupdatestripe.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Employer_Proflile(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Employer_Proflile(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                let ProfileComp = ProfileCompleteScreen(nibName: "ProfileCompleteScreen", bundle: nil)
                self.navigationController?.pushViewController(ProfileComp, animated: true)
            }
        }
        catch
        {
            print("Error")
        }
        
    }
}
