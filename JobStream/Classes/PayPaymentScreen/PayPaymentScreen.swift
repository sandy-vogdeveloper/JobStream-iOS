//
//  PayPaymentScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 03/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class PayPaymentScreen: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var lblTitle: UILabel!
    var viewMain = UIView()
    var imgLogo = UIImageView()
    var txtEmail = UITextField()
    var txtCard = UITextField()
    var txtDate = UITextField()
    var txtCVC = UITextField()
    var txtAmt = UITextField()
    
    var viewEmail = UIView()
    var viewCard = UIView()
    var viewDate = UIView()
    var viewCVC = UIView()
    var viewAmt = UIView()
    
    var btnCheckMark = UIButton()
    var btnPay = UIButton()
    var btnProceed = UIButton()
    var btnAmt1 = UIButton()
    var btnAmt2 = UIButton()
    var btnAmt3 = UIButton()
    var btnAmt4 = UIButton()
    
    var viewForAlert = UIButton()
    
     var btnBack = UIButton()
     var btnBack1 = UIButton()

    var tblInfo = UITableView()
    let arrFields : [String] = ["3 cents per tap when looking for resume","3 centes per tap when employee taps on your \n job posts","3 cents per tap when browsing per resume"]

    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: LoadInitialUI
    func loadInitialUI()
    {
        lblTitle.textColor=UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        viewMain .removeFromSuperview()
        viewMain.frame=CGRect(x: 0, y: 90, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-60)
        viewMain.backgroundColor=UIColor.white.withAlphaComponent(0.8)
        //self.view.addSubview(viewMain)
        
        imgLogo.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2-50, y: 40, width: 100, height: 100)
        imgLogo.image=UIImage(named: "icon_TLogo")
        self.view.addSubview(imgLogo)
        
                /*--------------       Enter Email   ---------------*/
         viewEmail.frame=CGRect(x: 20, y: imgLogo.frame.maxY+30, width: Constant.GlobalConstants.screenWidth-40, height: 35)
         viewEmail.layer.cornerRadius=5.0
         viewEmail.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
         viewEmail.backgroundColor=UIColor.white
         viewEmail.layer.borderWidth=1.0
         self.view.addSubview(viewEmail)
        
        txtEmail .removeFromSuperview()
        txtEmail.frame=CGRect(x: 5, y: 0, width: viewEmail.frame.size.width-5, height: 35)
       
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email", attributes:[NSForegroundColorAttributeName: UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1),NSFontAttributeName :UIFont(name: "Helvetica", size: 14)!])
        
        txtEmail.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        txtEmail.text=Constant.GlobalConstants.theAppDelegate.strLEmail as String
        txtEmail.isUserInteractionEnabled=false
        txtEmail.backgroundColor=UIColor.clear
        viewEmail.addSubview(txtEmail)
        
        /*--------------       Enter Card   ---------------*/
        viewCard.frame=CGRect(x: 20, y: viewEmail.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-40, height: 35)
        viewCard.layer.cornerRadius=5.0
        viewCard.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        viewCard.backgroundColor=UIColor.white
        viewCard.layer.borderWidth=1.0
        self.view.addSubview(viewCard)
        
        txtCard .removeFromSuperview()
        txtCard.frame=CGRect(x: 5, y: 0, width: viewCard.frame.size.width-5, height: 35)
        txtCard.attributedPlaceholder = NSAttributedString(string:"Card Number", attributes:[NSForegroundColorAttributeName: UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1),NSFontAttributeName :UIFont(name: "Helvetica", size: 14)!])
        
        txtCard.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        txtCard.backgroundColor=UIColor.clear
        viewCard.addSubview(txtCard)
    
        
        /*--------------       Enter Date   ---------------*/
        viewDate.frame=CGRect(x: 20, y: viewCard.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/2-30, height: 35)
        viewDate.layer.cornerRadius=5.0
        viewDate.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        viewDate.backgroundColor=UIColor.white
        viewDate.layer.borderWidth=1.0
        self.view.addSubview(viewDate)
        
        txtDate .removeFromSuperview()
        txtDate.frame=CGRect(x: 5, y: 0, width: viewDate.frame.size.width-5, height: 35)
        //txtDate.placeholder="MM/YY"
        
        txtDate.attributedPlaceholder = NSAttributedString(string:"MM/YY", attributes:[NSForegroundColorAttributeName: UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1),NSFontAttributeName :UIFont(name: "Helvetica", size: 14)!])
        txtDate.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
       txtDate.backgroundColor=UIColor.clear
        viewDate.addSubview(txtDate)
        
        
                    /*--------------       Enter CVC   ---------------*/
        viewCVC.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2, y: viewCard.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/2-30, height: 35)
        viewCVC.layer.cornerRadius=5.0
        viewCVC.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        viewCVC.backgroundColor=UIColor.white
        viewCVC.layer.borderWidth=1.0
        self.view.addSubview(viewCVC)
        
        txtCVC .removeFromSuperview()
        txtCVC.frame=CGRect(x: 5, y: 0, width: viewCVC.frame.size.width-5, height: 35)
        txtCVC.attributedPlaceholder = NSAttributedString(string:"CVC", attributes:[NSForegroundColorAttributeName: UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1),NSFontAttributeName :UIFont(name: "Helvetica", size: 14)!])
        txtCVC.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        txtCVC.backgroundColor=UIColor.clear
        viewCVC.addSubview(txtCVC)
        
        btnCheckMark .removeFromSuperview()
        btnCheckMark.frame=CGRect(x: 20, y: viewCVC.frame.maxY+15, width: 15, height: 15)
        btnCheckMark.layer.cornerRadius=3.0
        btnCheckMark.layer.borderWidth=1.0
        
        btnCheckMark.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        self.view .addSubview(btnCheckMark)
        
        let lblRembr=UILabel()
        lblRembr.frame=CGRect(x: btnCheckMark.frame.maxX+10, y: viewCVC.frame.maxY+7, width: 200, height: 30)
        lblRembr.text="Remember me everywhere"
        lblRembr.font=UIFont(name: "Helvetica", size: 14)
        lblRembr.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        self.view.addSubview(lblRembr)
        
        viewAmt.frame=CGRect(x: 20, y: lblRembr.frame.maxY+10, width: Constant.GlobalConstants.screenWidth-40, height: 35)
        viewAmt.layer.cornerRadius=5.0
        viewAmt.layer.borderColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1).cgColor
        viewAmt.backgroundColor=UIColor.white
        viewAmt.layer.borderWidth=1.0
        self.view.addSubview(viewAmt)
        
        txtAmt .removeFromSuperview()
        txtAmt.frame=CGRect(x: 5, y: 0, width: viewAmt.frame.size.width, height: 35)
        txtAmt.attributedPlaceholder = NSAttributedString(string:"Enter Amount", attributes:[NSForegroundColorAttributeName: UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1),NSFontAttributeName :UIFont(name: "Helvetica", size: 14)!])
        txtAmt.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
         txtAmt.backgroundColor=UIColor.clear
        viewAmt.addSubview(txtAmt)
        
        btnAmt1.frame=CGRect(x: 20, y: viewAmt.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/4-18, height: 35)
        btnAmt1.setTitle("$50", for: .normal)
        btnAmt1.setTitleColor(UIColor.white, for: .normal)
        btnAmt1.titleLabel?.font=UIFont(name: "Helvetica", size: 14)
        btnAmt1.layer.borderWidth=1.0
        btnAmt1.layer.cornerRadius=4.0
        btnAmt1.layer.borderColor=UIColor.black.cgColor
        self.view.addSubview(btnAmt1)
        
        btnAmt2.frame=CGRect(x: btnAmt1.frame.maxX+10, y: viewAmt.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/4-18, height: 35)
        btnAmt2.setTitle("$100", for: .normal)
        btnAmt2.setTitleColor(UIColor.white, for: .normal)
        btnAmt2.titleLabel?.font=UIFont(name: "Helvetica", size: 14)
        btnAmt2.layer.borderWidth=1.0
        btnAmt2.layer.cornerRadius=4.0
        btnAmt2.layer.borderColor=UIColor.black.cgColor
        self.view.addSubview(btnAmt2)
        
        btnAmt3.frame=CGRect(x: btnAmt2.frame.maxX+10, y: viewAmt.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/4-18, height: 35)
        btnAmt3.setTitle("$150", for: .normal)
        btnAmt3.setTitleColor(UIColor.white, for: .normal)
        btnAmt3.titleLabel?.font=UIFont(name: "Helvetica", size: 14)
        btnAmt3.layer.borderWidth=1.0
        btnAmt3.layer.cornerRadius=4.0
        btnAmt3.layer.borderColor=UIColor.black.cgColor
        self.view.addSubview(btnAmt3)
        
        btnAmt4.frame=CGRect(x: btnAmt3.frame.maxX+10, y: viewAmt.frame.maxY+10, width: Constant.GlobalConstants.screenWidth/4-18, height: 35)
        btnAmt4.setTitle("$200", for: .normal)
        btnAmt4.setTitleColor(UIColor.white, for: .normal)
        btnAmt4.titleLabel?.font=UIFont(name: "Helvetica", size: 14)
        btnAmt4.layer.borderWidth=1.0
        btnAmt4.layer.cornerRadius=4.0
        btnAmt4.layer.borderColor=UIColor.black.cgColor
        self.view.addSubview(btnAmt4)
        
        btnAmt1.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt2.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt3.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt4.setTitleColor(UIColor (red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        
        btnAmt1.addTarget(self, action:#selector(self.btnAmt1Pressed), for:.touchUpInside)
        btnAmt2.addTarget(self, action:#selector(self.btnAmt2Pressed), for:.touchUpInside)
        btnAmt3.addTarget(self, action:#selector(self.btnAmt3Pressed), for:.touchUpInside)
        btnAmt4.addTarget(self, action:#selector(self.btnAmt4Pressed), for:.touchUpInside)
        
        btnPay.removeFromSuperview()
        btnPay.frame=CGRect(x: 20, y: btnAmt1.frame.maxY+25, width: Constant.GlobalConstants.screenWidth-40, height: 35)
        btnPay.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        btnPay.layer.cornerRadius=5.0
        btnPay.setTitle("Pay", for: .normal)
        btnPay.titleLabel?.font=UIFont(name: "Helvetica", size: 16)
        btnPay.addTarget(self, action:#selector(self.btnPayPressed), for:.touchUpInside)
        self.view.addSubview(btnPay)
        
        
        txtCard.keyboardType=UIKeyboardType.numberPad
        txtCVC.keyboardType=UIKeyboardType.numberPad
        txtAmt.keyboardType=UIKeyboardType.numberPad
        
        txtCard.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtCVC.clearButtonMode = UITextFieldViewMode.whileEditing;
        txtAmt.clearButtonMode = UITextFieldViewMode.whileEditing;
        
        /*----------------      Tool Bar for Card textField      ----------*/
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtCard.inputAccessoryView = toolBar
        
        /*----------------      Tool Bar for CVC textField      ----------*/
        let toolBar1 = UIToolbar()
        toolBar1.barStyle = .default
        toolBar1.isTranslucent = true
        toolBar1.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar1.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton1 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar1.setItems([spaceButton1, doneButton1], animated: false)
        toolBar1.isUserInteractionEnabled = true
        txtCVC.inputAccessoryView = toolBar1
        
        /*----------------      Tool Bar for Ammount textField      ----------*/
        let toolBar2 = UIToolbar()
        toolBar2.barStyle = .default
        toolBar2.isTranslucent = true
        toolBar2.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar2.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton2 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar2.setItems([spaceButton2, doneButton2], animated: false)
        toolBar2.isUserInteractionEnabled = true
        txtAmt.inputAccessoryView = toolBar2
    }

    func doneClick() {
        
        txtAmt.resignFirstResponder()
        txtCVC.resignFirstResponder()
        txtCard.resignFirstResponder()
    }

    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
    
    func btnPayPressed(sender: UIButton!)
    {
        let pay2 = PaymentSecScreen(nibName: "PaymentSecScreen", bundle: nil)
        self.navigationController?.pushViewController(pay2, animated: true)
    }
    
    func btnAmt1Pressed(sender: UIButton!)
    {
            btnAmt1.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            btnAmt2.backgroundColor=UIColor.clear
            btnAmt3.backgroundColor=UIColor.clear
            btnAmt4.backgroundColor=UIColor.clear
            txtAmt.text="$50"
        
        btnAmt1.setTitleColor(UIColor.white, for: .normal)
        btnAmt2.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt3.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt4.setTitleColor(UIColor (red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
    }
    
    func btnAmt2Pressed(sender: UIButton!)
    {
            btnAmt2.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            btnAmt1.backgroundColor=UIColor.clear
            btnAmt3.backgroundColor=UIColor.clear
            btnAmt4.backgroundColor=UIColor.clear
            txtAmt.text="$100"
        btnAmt1.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt2.setTitleColor(UIColor.white, for: .normal)
        btnAmt3.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        btnAmt4.setTitleColor(UIColor (red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
        
    }
    
    func btnAmt3Pressed(sender: UIButton!)
    {
            btnAmt3.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            btnAmt1.backgroundColor=UIColor.clear
            btnAmt2.backgroundColor=UIColor.clear
            btnAmt4.backgroundColor=UIColor.clear
            txtAmt.text="$150"
        
            btnAmt1.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
            btnAmt2.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
            btnAmt3.setTitleColor(UIColor.white, for: .normal)
            btnAmt4.setTitleColor(UIColor (red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
    }
    
    func btnAmt4Pressed(sender: UIButton!)
    {
            btnAmt4.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
            btnAmt1.backgroundColor=UIColor.clear
            btnAmt2.backgroundColor=UIColor.clear
            btnAmt3.backgroundColor=UIColor.clear
            txtAmt.text="$200"
            btnAmt1.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
            btnAmt2.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
            btnAmt3.setTitleColor(UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0), for: .normal)
            btnAmt4.setTitleColor(UIColor.white, for: .normal)
    }
    
    func btnProceedPressed(sender: UIButton!)
    {
        let cardParams = STPCardParams()
        
        cardParams.number="4242424242424242"
        cardParams.expMonth = 10;
        cardParams.expYear = 2020;
        cardParams.cvc = "121"
        
        STPAPIClient.shared().createToken(withCard: cardParams, completion: { (token, error) -> Void in
            
            if error != nil {
                //self.handleError(error!)
                print(error?.localizedDescription ?? 0)
                return
            }
            print(token?.tokenId ?? 0)
            
            // self.postStripeToken(token!)
        })
        //self.viewForAlertUI()        
        //let profEdit = ProfileCompleteScreen(nibName: "ProfileCompleteScreen", bundle: nil)
        //self.navigationController?.pushViewController(profEdit, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        
        let viewForCell=UIView()
        let imgBar=UIImageView()
        let lblTitle=UILabel()
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        self.tblInfo.separatorStyle = .none
        
        viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 70)
        viewForCell.backgroundColor=UIColor.clear
        cell.contentView .addSubview(viewForCell)
        cell.backgroundColor=UIColor.clear
        imgBar.frame=CGRect(x: 5,y: 5,width:30,height: 30)
        
        if indexPath.row==1 {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 39)
            imgBar.frame=CGRect(x: 5,y: viewForCell.frame.size.height/2-2.5,width:5,height: 5)
            lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:40)
        }
        else
        {
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 29)
            imgBar.frame=CGRect(x: 5,y: viewForCell.frame.size.height/2-2.5,width:5,height: 5)
            lblTitle.frame=CGRect(x: imgBar.frame.maxX+10,y: 0,width: UIScreen.main.bounds.size.width-20,height:30)
        }
        imgBar.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        imgBar.layer.cornerRadius = imgBar.frame.size.width/2
        imgBar.clipsToBounds = true
        viewForCell .addSubview(imgBar)
        
        lblTitle.text = arrFields [(indexPath as NSIndexPath).row]
        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        lblTitle.textAlignment=NSTextAlignment.left
        lblTitle.font =  UIFont(name:"Helvetica-Light", size: 12)
        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblTitle.numberOfLines = 0
        viewForCell .addSubview(lblTitle)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row==1
        {
            return 40
        }
        else
        {
            return 30
        }
    }
    
    //MARK: ViewForHelpView
    func viewForAlertUI()
    {
        viewForAlert.frame=CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0)
        let whte = UIColor.black
        viewForAlert.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForAlert)
        
        var frame1 = CGRect()
        frame1=viewForAlert.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        let imgCheck = UIImageView()
                        
                        btnClose.frame=CGRect(x: self.viewForAlert.frame.size.width-40, y: 2, width: 35, height: 35)
                        btnClose.setBackgroundImage(UIImage(named: "icon_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnCloseSavePressed), for:.touchUpInside)
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-60
                            , width: Constant.GlobalConstants.screenWidth-60, height: 100)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        imgCheck .removeFromSuperview()
                        imgCheck.frame=CGRect(x: viewforTip.frame.size.width/2-15, y: 5, width: 35, height: 30)
                        imgCheck.image=UIImage(named: "icon_CheckMark")
                        
                        lblTitle.frame=CGRect(x: 5, y: imgCheck.frame.maxY, width: viewforTip.frame.size.width-10, height: 30)
                        lblTitle.text="Payment Successful!"
                        lblTitle.font=UIFont(name:"Helvetica", size: 16)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.frame=CGRect(x: 0, y: lblTitle.frame.maxY-5, width: viewforTip.frame.size.width, height: 30)
                        lblSubTitle.text="(recipet will be sent to your email)"
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 14)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        lblSubTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblSubTitle.numberOfLines = 0
                        
                        self.viewForAlert.addSubview(btnClose)
                        self.viewForAlert.addSubview(viewforTip)
                        
                        viewforTip.addSubview(imgCheck)
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)
        })
    }
    
    //MARK: UIButton Help Pressed
    func btnCloseSavePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForAlert .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForAlert.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForAlert.frame=frameViewForHelp
        })
    }
}
