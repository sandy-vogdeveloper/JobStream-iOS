//
//  MapScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 04/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit
import MapKit
class MapScreen: UIViewController,MKMapViewDelegate {
    
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    let mapView = MKMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadMainUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Load MainUI
    func loadMainUI()
    {
        btnBack.frame=CGRect(x: 3, y: 5, width: 18,height: 18)
        let image = UIImage(named: "icon_BackMove") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        mapView.frame=CGRect(x: 0, y: 50, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-30)
        mapView.delegate = self
        self.view.addSubview(mapView)
    }
    
    //MARK: UIButton Pressed delegate
    func btn_back_action(sender: UIButton!) {
        
        navigationController?.popViewController(animated:true)
    }
}
