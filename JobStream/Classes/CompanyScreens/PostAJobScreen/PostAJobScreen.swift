//
//  PostAJobScreen.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 02/05/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit

class PostAJobScreen: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
     var imgJob = UIImageView()
     var viewBG = UIView()
     var lblCompName = UILabel()
    
    var txtPosition = UITextField()
    var txtBox = UITextField()
    var btnBack = UIButton()
    var btnBack1 = UIButton()
    var btnCalender = UIButton()
    
    var btnAvailable = UIButton()
    
    var btnApply = UIButton()
    
    var lblAbout = UILabel()
    var lblAbtDtls = UILabel()
    
    var lblTDesc = UILabel()
    var lblDesc = UILabel()
    
    var lblPosition = UILabel()
    var lblPostDesc = UILabel()
    var viewAVBG = UIView()
    var lblCompensation = UILabel()
    var lblComptionDesc = UILabel()
    var lblDescPHolder = UILabel()
    var lblPPHolder = UILabel()
    var lblCompPPHolder = UILabel()
    var lblDate = UILabel()
    var lblPDur = UILabel()
    var lblAvailable = UILabel()
    var viewBack = UIView()
    
    var lblTitle = UILabel()
    var lblSubTitle = UILabel()
    
    var txtVAbt = UITextView()
    var txtVDescription = UITextView()
    var txtVCompnDesc = UITextView()
    var txtVPosition = UITextView()
    var txtVPDuration = UITextView()
    
    var strCategory = NSString()
    var strSubCategory = NSString()
    var strCID = NSString()
    var strSID = NSString()
    
    var datePicker : UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self
            .keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.loadInitialUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    func loadInitialUI()
    {
        
        btnBack.frame=CGRect(x: 10, y: 5, width: 10,height: 20)
        let image = UIImage(named: "icon_BckNew") as UIImage?
        btnBack.setImage(image, for: .normal)
        btnBack1.frame=CGRect(x: 0, y:0, width: 50,height: 50)
        
        btnBack.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        btnBack1.addTarget(self, action:#selector(self.btn_back_action), for:.touchUpInside)
        
        self.view.addSubview(btnBack)
        self.view.addSubview(btnBack1)
        
        lblTitle.frame=CGRect(x: 30, y: 3, width: Constant.GlobalConstants.screenWidth-60,height: 30)
        lblTitle.text=strSubCategory as String
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont(name:"Helvetica", size: 22)
        lblTitle.textColor = UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        self.view.addSubview(lblTitle)
        
        lblSubTitle.frame=CGRect(x: 30, y: lblTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth-60,height: 30)
        lblSubTitle.text=strCategory as String
        lblSubTitle.textAlignment = .center
        lblSubTitle.font = UIFont(name:"Helvetica-Light", size: 18)
        lblSubTitle.textColor = UIColor(red: 24.0/255.0, green: 55/255.0, blue: 127/255.0, alpha: 1.0)
        self.view.addSubview(lblSubTitle)
        
        viewBG.frame=CGRect(x: 5, y: lblSubTitle.frame.maxY, width: Constant.GlobalConstants.screenWidth-10,height: Constant.GlobalConstants.screenHeight-70)
        viewBG.layer.borderWidth=1.0
        viewBG.layer.borderColor = UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7).cgColor
        viewBG.layer.cornerRadius=5.0
        viewBG.backgroundColor = UIColor(red: 150.0/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1.0).withAlphaComponent(0.7)
        self.view.addSubview(viewBG)
        
        imgJob.frame=CGRect(x: 5, y: 5, width: Constant.GlobalConstants.screenWidth/2-30,height: 100)
        imgJob.image = UIImage(named: "icom_job")
        imgJob.layer.cornerRadius = 5.0
        imgJob.layer.borderColor = UIColor.white.cgColor
        imgJob.layer.borderWidth = 1.0
        viewBG.addSubview(imgJob)
        
        lblCompName.frame=CGRect(x: imgJob.frame.maxX+5, y: 5, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        lblCompName.text="Company Name"
        lblCompName.textColor = UIColor.white
        lblCompName.textAlignment = .left
        lblCompName.font = UIFont(name:"Helvetica", size: 16)
        viewBG.addSubview(lblCompName)
        
        
         txtPosition.frame=CGRect(x: imgJob.frame.maxX+5, y: lblCompName.frame.maxY-5, width: 150, height: 30)
        
         txtPosition.attributedPlaceholder = NSAttributedString(string:"Position", attributes:[NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName :UIFont(name: "Helvetica", size: 18)!])
         
         txtPosition.font =  UIFont(name:"Helvetica", size: 18)
         txtPosition.keyboardType=UIKeyboardType.default
         txtPosition.returnKeyType = UIReturnKeyType.done
         txtPosition.clearButtonMode = UITextFieldViewMode.whileEditing;
         txtPosition.textAlignment = .left
         txtPosition.delegate = self
         txtPosition.isUserInteractionEnabled = false
         txtPosition.textColor=UIColor.white
         viewBG.addSubview(txtPosition)
        
        viewAVBG.frame=CGRect(x: imgJob.frame.maxX, y: txtPosition.frame.maxY, width: (viewBG.frame.size.width-imgJob.frame.size.width)-5, height: 30)
        viewAVBG.backgroundColor=UIColor(red: 26.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 1.0)
        viewBG.addSubview(viewAVBG)
        
        lblAvailable.frame=CGRect(x: 5, y: 0, width: viewAVBG.frame.size.width-5, height: 30)
        lblAvailable.text="Available-Bold"
        lblAvailable.textColor=UIColor.white
        lblAvailable.font = UIFont(name:"Helvetica", size: 16)
        viewAVBG.addSubview(lblAvailable)
        
        lblAvailable.backgroundColor = UIColor(red: 26.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 1.0)
       
        btnAvailable.backgroundColor = UIColor(red: 26.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 1.0)
        //btnAvailable.addTarget(self, action:#selector(self.btnSignInPressed), for:.touchUpInside)
        
        btnApply.frame=CGRect(x: Constant.GlobalConstants.screenWidth-110, y: 10, width: 100, height: 30)
        btnApply .setTitle("Post a Job", for: .normal)
        btnApply.titleLabel?.font=UIFont(name:"Helvetica", size: 14)
        btnApply.layer.cornerRadius=4.0
        btnApply.backgroundColor = UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        btnApply.addTarget(self, action:#selector(self.btnPostAJobPressed), for:.touchUpInside)
        self.view .addSubview(btnApply)
        
        /*------------------        About Title      -----------------------*/
        if Constant.GlobalConstants.screenWidth<325
        {
            lblAbout.frame=CGRect(x: 5, y: imgJob.frame.maxY-20, width: 100, height: 30)
        }
        else
        {
            lblAbout.frame=CGRect(x: 5, y: imgJob.frame.maxY, width: 100, height: 30)
        }
            lblAbout.text="About:"
        lblAbout.textColor=UIColor.white
        lblAbout.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblAbout)
        
        lblAbtDtls.frame=CGRect(x: 5, y: lblAbout.frame.maxY-5, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        lblAbtDtls.text="Lorem ipsum dolor sit amet, consectetuer adipiscing \n elit, sed diam nonummy nibh euismod Lorem ipsum \n dolor sit amet, consectetuer adipiscing elit. send diam \n nonummy nibh euismod"
        
        lblAbtDtls.textColor=UIColor.white
        lblAbtDtls.font=UIFont(name:"Helvetica", size: 12)
        lblAbtDtls.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblAbtDtls.numberOfLines = 0
        viewBG.addSubview(lblAbtDtls)
        
        /*------------------        Description Title      -----------------------*/
        
        lblTDesc.frame=CGRect(x: 5, y: lblAbtDtls.frame.maxY-2, width: 100, height: 30)
        lblTDesc.text="Description:"
        lblTDesc.textColor=UIColor.white
        lblTDesc.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblTDesc)
        
        txtVDescription.frame=CGRect(x: 5, y: lblTDesc.frame.maxY-7, width: Constant.GlobalConstants.screenWidth-10, height: 60)
        txtVDescription.delegate = self
        txtVDescription.textColor=UIColor.white
        txtVDescription.font=UIFont(name:"Helvetica", size: 14)
        txtVDescription.backgroundColor=UIColor.clear
        txtVDescription.autocorrectionType = .no
        viewBG.addSubview(txtVDescription)
        
        lblDescPHolder.frame=CGRect(x: 0, y: 0, width: txtVDescription.frame.size.width, height: 30)
        
        lblDescPHolder.text="Please enter job description here"
        lblDescPHolder.font=UIFont(name:"Helvetica", size: 14)
        lblDescPHolder.textAlignment = .left
        lblDescPHolder.textColor=UIColor.white
        txtVDescription.addSubview(lblDescPHolder)
        
        /*------------------        Poistion Title      -----------------------*/
        
        lblPosition.frame=CGRect(x: 5, y: txtVDescription.frame.maxY-2, width: 100, height: 30)
        lblPosition.text="Position:"
        lblPosition.textColor=UIColor.white
        lblPosition.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblPosition)
        
        txtVPosition.frame=CGRect(x: 5, y: lblPosition.frame.maxY-7, width: Constant.GlobalConstants.screenWidth-10, height: 60)
        txtVPosition.backgroundColor=UIColor.clear
        txtVPosition.delegate = self
        txtVPosition.textColor=UIColor.white
        txtVPosition.font=UIFont(name:"Helvetica", size: 14)
        txtVPosition.autocorrectionType = .no
        viewBG.addSubview(txtVPosition)
        
        lblPPHolder.frame=CGRect(x: 0, y: 0, width: txtVPosition.frame.size.width, height: 30)
        lblPPHolder.text="Please enter job position details here"
        lblPPHolder.font=UIFont(name:"Helvetica", size: 14)
        lblPPHolder.textAlignment = .left
        lblPPHolder.textColor=UIColor.white
        txtVPosition.addSubview(lblPPHolder)
        
        /*------------------        Compensation Title      -----------------------*/
        lblCompensation.frame=CGRect(x: 5, y: txtVPosition.frame.maxY-2, width: 130, height: 30)
        lblCompensation.text="Compensation:"
        lblCompensation.textColor=UIColor.white
        lblCompensation.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblCompensation)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            txtVCompnDesc.frame=CGRect(x: 5, y: lblCompensation.frame.maxY-7, width: Constant.GlobalConstants.screenWidth-10, height: 60)
        }
        else
        {
            txtVCompnDesc.frame=CGRect(x: 5, y: lblCompensation.frame.maxY-7, width: Constant.GlobalConstants.screenWidth-10, height: 70)
        }
        
        txtVCompnDesc.backgroundColor=UIColor.clear
        txtVCompnDesc.delegate = self
        txtVCompnDesc.textColor=UIColor.white
        txtVCompnDesc.font=UIFont(name:"Helvetica", size: 14)
        txtVCompnDesc.autocorrectionType = .no
        viewBG.addSubview(txtVCompnDesc)
        
        lblCompPPHolder.frame=CGRect(x: 0, y: 0, width: txtVCompnDesc.frame.size.width, height: 30)
        lblCompPPHolder.text="Please enter job position details here"
        lblCompPPHolder.font=UIFont(name:"Helvetica", size: 14)
        lblCompPPHolder.textAlignment = .left
        lblCompPPHolder.textColor=UIColor.white
        txtVCompnDesc.addSubview(lblCompPPHolder)
        txtVCompnDesc.autocorrectionType = .no
        
        lblPDur.frame=CGRect(x: 5, y: txtVCompnDesc.frame.maxY-2, width: 130, height: 30)
        lblPDur.text="Post Duration"
        lblPDur.textColor=UIColor.white
        lblPDur.font=UIFont(name:"Helvetica-Bold", size: 16)
        viewBG.addSubview(lblPDur)
        
        viewBack.frame=CGRect(x: 5, y: lblPDur.frame.maxY, width: Constant.GlobalConstants.screenWidth/2-5, height: 30)
        viewBack.backgroundColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
        viewBG.addSubview(viewBack)
        
        lblDate.frame=CGRect(x: 0, y: 0, width: viewBack.frame.size.width-35, height: 30)
        lblDate.backgroundColor=UIColor.clear
        lblDate.text="12 - 5 - 17"
        lblDate.textColor=UIColor.white
        lblDate.textAlignment = .center
        lblDate.font=UIFont(name:"Helvetica-Light", size: 14)
        viewBack.addSubview(lblDate)
        
        btnCalender.frame=CGRect(x: viewBack.frame.size.width-30, y: 5, width: 20, height: 20)
        txtBox.frame=CGRect(x: viewBack.frame.size.width-30, y: 5, width: 20, height: 20)
        txtBox.backgroundColor=UIColor.clear
        txtBox.delegate=self
        btnCalender.setImage(UIImage(named:"icon_Menu1"), for: UIControlState.normal)
        viewBack.addSubview(btnCalender)
        viewBack.addSubview(txtBox)
    }
    @objc(textView:shouldChangeTextInRange:replacementText:) func textView(_ shouldChangeTextIntextView: UITextView, shouldChangeTextIn shouldChangeTextIne: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            txtVCompnDesc.resignFirstResponder()
            txtVPosition.resignFirstResponder()
            txtVCompnDesc.resignFirstResponder()
            txtVDescription.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK:- textFiled Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUpDate(self.txtBox)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtVDescription
        {
            lblDescPHolder .isHidden=true
        }
        else if textView == txtVPosition
        {
            lblPPHolder.isHidden=true
        }
        else if textView == txtVCompnDesc
        {
            lblCompPPHolder.isHidden=true
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        var intCount1 = NSInteger()
        
        
        if textView == txtVDescription
        {
            intCount1=(txtVDescription.text?.characters.count)!
            if intCount1>0 {
                self.lblDescPHolder.isHidden=true
            }
        }
        else if textView == txtVPosition
        {
            intCount1=(txtVPosition.text?.characters.count)!
            if intCount1>0 {
                self.lblPPHolder.isHidden=true
            }
        }
        else if textView == txtVCompnDesc
        {
            intCount1=(txtVCompnDesc.text?.characters.count)!
            if intCount1>0 {
                self.lblCompPPHolder.isHidden=true
            }
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func pickUpDate(_ textField : UITextField){
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        txtBox.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(ViewController.cancelClick))
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtBox.inputAccessoryView = toolBar
    }
    //MARK: UIButton Pressed delegate declarations here
    
    //MARK: UIButton Pressed delegate
    func doneClick()
    {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        lblDate.text = dateFormatter1.string(from: datePicker.date)
        txtBox.resignFirstResponder()
    }
    func btn_back_action(sender: UIButton!)
    {
        let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
        self.navigationController?.pushViewController(UMD, animated: true)
    }
   
    func btnPostAJobPressed()
    {
        if isValidTextField()
        {
            postAJobAPI()
        }
    }
    
    func isValidTextField() -> Bool
    {
        
        var strMsg = NSString()
        
        var intVDescription = NSInteger()
        var intVPosition = NSInteger()
        var intVCompnDesc = NSInteger()
        
        intVDescription=(txtVDescription.text?.characters.count)!
        intVPosition=(txtVPosition.text?.characters.count)!
        intVCompnDesc=(txtVCompnDesc.text?.characters.count)!
        
        if intVDescription > 0
        {
            if intVPosition > 0
            {
                if intVCompnDesc > 0
                {
                    return true;
                }
                else
                {
                    strMsg="Please enter Compensation."
                }
            }
                else
            {
                strMsg="Please enter Position."
            }
        }
        else
        {
            strMsg = "Please enter Description ."
        }
        let alertMessage = UIAlertController(title: "Warning", message: strMsg as String, preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alertMessage, animated: true, completion: nil)
        
        return false
    }

    func postAJobAPI()
    {
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        var strPosition = ""
        var strCompensation = ""
        var strDescription = ""
        
        let strEDate = "2017-10-10"
        var strDesc = NSString()
        strDesc = "hi  job opening, good experice with us"
        strPosition = txtVPosition.text!
        strCompensation = txtVCompnDesc.text!
        strDescription = txtVDescription.text!
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((strCID), forKey: "cid" as NSCopying)
        bodydata.setObject((strSID), forKey: "sid" as NSCopying)
        bodydata.setObject((strDescription), forKey: "description" as NSCopying)
        bodydata.setObject((strPosition), forKey: "position" as NSCopying)
        bodydata.setObject((strCompensation), forKey: "compensation" as NSCopying)
        bodydata.setObject((strEDate), forKey: "expirydate" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjobpost.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                let UMD = UserMainDashboard(nibName: "UserMainDashboard", bundle: nil)
                self.navigationController?.pushViewController(UMD, animated: true)
            }
        }
        catch
        {
            print("Error")
        }
        
    }

    //Keyboard Show & Hide methods
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -140
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
}
