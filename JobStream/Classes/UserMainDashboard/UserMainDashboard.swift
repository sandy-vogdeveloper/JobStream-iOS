//
//  UserMainDashboard.swift
//  JobStream
//
//  Created by Bhimashankar Vibhute on 27/04/17.
//  Copyright © 2017 Syneotek Software Solution. All rights reserved.
//

import UIKit
// MARK: - Section Data Structure
//
struct Section {
    var name: String!
    var items: [String]!
    var collapsed: Bool!
    
    init(name: String, items: [String], collapsed: Bool = false) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

class UserMainDashboard: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var btnHelp: UIButton!
    @IBOutlet var btnHelp1: UIButton!
    var btnMenu = UIButton()
    var btnMenu1 = UIButton()
    
    var viewForNotification = UIView()
    var isopen = Bool()
    var isMenuPressed:Bool = false
    var sections = [Section]()
    var btnSetting = UIButton()
    var btnUpgrade = UIButton()
    var btnLogOut = UIButton()
    var btnBtmNotification = UIButton()
    var btnHomeStream = UIButton()
    var btnApplied = UIButton()
    var btnNotiSetting = UIButton()
    var btnAddJob = UIButton()
    
    var viewMenu = UIView()
    var viewForAlert = UIView()
    var viewForHomeStream = UIView()
    var viewForShortListed = UIView()
    var viewbtnSeleted = UIView()
    var imgDot = UIImageView()
    var imgChat = UIImageView()
    var lblNoti = UILabel()
    var tblHomeStream = UITableView()
    var tblShortlist = UITableView()
    var tblNotifications = UITableView()
    
    var isHomeStream:Bool = true
    var app=UIApplication.shared.delegate as! AppDelegate
    
    var isHomeStreamData:Bool = true
    var isUserJList:Bool = false
    var isUserAppliedAPI: Bool = false
    var arrHSCategoryList:NSMutableArray=[]
    var arrShortListedData:NSMutableArray=[]
    var strHStremSID = Int()
    var arrHSCatDetilsList:NSMutableArray=[]
    var arrCheckCollapsed:NSMutableArray=[]
    var imgForMenuBG = UIImageView()
    var imgHSteram = UIImageView()
    var imgSList = UIImageView()
    
    //MARK: Load InitialUI
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.loadMainUI()
        getDHomeStreamListAPI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        viewMenu .removeFromSuperview()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        btnMenu.frame=CGRect(x: 3, y: 5, width: 20,height: 20)
        let image = UIImage(named: "icon_Menu.png") as UIImage?
        btnMenu.setImage(image, for: .normal)
        btnMenu1.frame=CGRect(x: 0, y:0, width: 35,height: 50)
        btnMenu1.backgroundColor=UIColor.clear
        isMenuPressed=false
        viewMenu.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    //MARK: Load MainUI
    func loadMainUI()
    {
        btnMenu.frame=CGRect(x: 3, y: 5, width: 20,height: 20)
        let image = UIImage(named: "icon_Menu.png") as UIImage?
        btnMenu.setImage(image, for: .normal)
        btnMenu1.frame=CGRect(x: 0, y:0, width: 35,height: 50)
        btnMenu1.backgroundColor=UIColor.clear
        isMenuPressed=false
        self.view.addSubview(btnMenu)
        self.view.addSubview(btnMenu1)
        
        btnMenu.addTarget(self, action:#selector(self.btnMenuPressed), for:.touchUpInside)
        btnMenu1.addTarget(self, action:#selector(self.btnMenuPressed), for:.touchUpInside)
        btnHelp.addTarget(self, action:#selector(self.btnHelpPressed), for:.touchUpInside)
        btnHelp1.addTarget(self, action:#selector(self.btnHelpPressed), for:.touchUpInside)
        
        imgForMenuBG.frame=CGRect(x: 0, y: btnMenu1.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 30)
        imgForMenuBG.image=UIImage(named: "icon_MenuBG")
        self.view.addSubview(imgForMenuBG)
        
        btnHomeStream.frame=CGRect(x: 0, y: btnMenu1.frame.maxY, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        btnApplied.frame=CGRect(x: btnHomeStream.frame.maxX, y: btnMenu1.frame.maxY, width: Constant.GlobalConstants.screenWidth/2, height: 30)
        
        btnHomeStream.backgroundColor=UIColor.clear
        btnApplied.backgroundColor=UIColor.clear
        
        if app.strSelectedUser == "non-profit"
        {
            btnHomeStream.setTitle("VolunStream", for: .normal)
        }
        else
        {
            btnHomeStream.setTitle("Homestream", for: .normal)
        }
        if app.strSelectedUser == "employee"
        {
            btnApplied.setTitle("Applied", for: .normal)
        }
        else
        {
            btnApplied.setTitle("Shortlist", for: .normal)
        }        
        
        btnHomeStream.setTitleColor(UIColor.white, for: .normal)
        btnApplied.setTitleColor(UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1), for: .normal)
        
        btnHomeStream.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 20)
        btnApplied.titleLabel!.font =  UIFont(name:"Helvetica-Light", size: 20)
        btnHomeStream.addTarget(self, action:#selector(self.btnHomeStreamPressed), for:.touchUpInside)
        btnApplied.addTarget(self, action:#selector(self.btnShortListedPressed), for:.touchUpInside)
        
        if Constant.GlobalConstants.screenWidth < 325
        {
            btnHomeStream.setTitle(" Homestream", for: .normal)
            imgHSteram.frame=CGRect(x: 6, y: 6, width: 18, height: 18)
            imgSList.frame=CGRect(x: 23, y: 7, width: 20, height: 16)
        }
        else
        {
            imgHSteram.frame=CGRect(x: 13, y: 5, width: 18, height: 20)
            imgSList.frame=CGRect(x: 25, y: 7, width: 20, height: 16)
        }
        
        imgHSteram.image=UIImage(named: "icon_HStrm")
        imgSList.image=UIImage(named: "icon_SList")
        
        btnHomeStream.addSubview(imgHSteram)
        btnApplied.addSubview(imgSList)
        
        //btnApplied.addTarget(self, action:#selector(self.btnShortListedPressed), for:.touchUpInside)
        
        self.view.addSubview(btnHomeStream)
        self.view.addSubview(btnApplied)
        
        let viewSept = UIView()
        viewSept.frame=CGRect(x: 0, y: btnHomeStream.frame.maxY, width: Constant.GlobalConstants.screenWidth, height: 1)
        viewSept.backgroundColor=UIColor.white;
        self.view .addSubview(viewSept)
        
        viewbtnSeleted.frame=CGRect(x: 0, y: btnHomeStream.frame.maxY-0.5, width: Constant.GlobalConstants.screenWidth/2, height: 2)
        viewbtnSeleted.backgroundColor=UIColor.white
        self.view.addSubview(viewbtnSeleted)
        
        viewForHomeStream .removeFromSuperview()
        viewForShortListed .removeFromSuperview()
        
        viewForHomeStream.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+1, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-150)
        self.view.addSubview(viewForHomeStream)
        
        tblHomeStream .removeFromSuperview()
        tblHomeStream.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height:viewForHomeStream.frame.size.height)
        tblHomeStream.delegate=self
        tblHomeStream.dataSource=self
        tblHomeStream.backgroundColor=UIColor.clear
        tblHomeStream.tableFooterView=UIView()
        
        viewForHomeStream.addSubview(tblHomeStream)
    
        if app.strSelectedUser == "employee"
        {
            viewForHomeStream.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+1, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-130)
        }
        else
        {
            btnAddJob=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth-40, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            btnAddJob.setImage(UIImage(named:"icon_PlusNew"), for: .normal)
            btnAddJob.addTarget(self, action: #selector(self.btnJobPostScreenUI), for: .touchUpInside)
            btnAddJob.backgroundColor = .clear
            self.view.addSubview(btnAddJob)
        }
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-25, width: Constant.GlobalConstants.screenWidth, height: 25)
        btnBtmNotification.setImage(UIImage(named:"img_NotiBG"), for: UIControlState.normal)
        btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 3)
        imgDot.backgroundColor = UIColor.white
        imgDot.layer.borderWidth = 0.5
        imgDot.layer.borderColor = UIColor.white.cgColor
        imgDot.layer.cornerRadius = 3.0
        btnBtmNotification.addSubview(imgDot)
        
        imgChat.frame=CGRect(x: 10, y: 2.5, width: 20, height: 20)
        imgChat.image=UIImage(named: "icon_ChatN")
        btnBtmNotification.addSubview(imgChat)
    }
    
    func tapOnScreen()
    {
        viewMenu.removeFromSuperview()
    }
    //MARK: ViewForLeftMenn
    func viewForLeftMenu()
    {
        viewMenu.frame=CGRect(x: 0, y: btnMenu.frame.maxY, width: 150, height: 100)
        viewMenu.backgroundColor=UIColor(red: 35.0/255.0, green: 81.0/255.0, blue: 125.0/255.0, alpha: 1.0)
        self.view.addSubview(viewMenu)        
        
        btnSetting.frame=CGRect(x: 0, y: 0, width: viewMenu.frame.size.width, height: 35)
        
        if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
        {
            viewMenu.frame=CGRect(x: 0, y: btnMenu.frame.maxY, width: 150, height: 100)
            btnUpgrade.frame=CGRect(x: 0, y: btnSetting.frame.maxY+2, width: viewMenu.frame.size.width, height: 35)
            btnLogOut.frame=CGRect(x: 0, y: btnUpgrade.frame.maxY+2, width: viewMenu.frame.size.width, height: 35)
        }
        else
        {
            viewMenu.frame=CGRect(x: 0, y: btnMenu.frame.maxY, width: 150, height: 65)
            btnLogOut.frame=CGRect(x: 0, y: btnSetting.frame.maxY+2, width: viewMenu.frame.size.width, height: 35)
        }
        
        btnSetting .setTitle("Settings", for: UIControlState.normal)
        btnUpgrade .setTitle("Upgrade", for: UIControlState.normal)
        btnLogOut .setTitle("Logout", for: UIControlState.normal)
        
        btnSetting.backgroundColor =  UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        btnUpgrade.backgroundColor = UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        btnLogOut.backgroundColor = UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        
        btnSetting.setTitleColor(UIColor.white, for: .normal)
        btnUpgrade.setTitleColor(UIColor.white, for: .normal)
        btnLogOut.setTitleColor(UIColor.white, for: .normal)
        
        btnSetting.titleLabel!.font =  UIFont(name:"Helvetica", size: 18)
        btnUpgrade.titleLabel!.font =  UIFont(name:"Helvetica", size: 18)
        btnLogOut.titleLabel!.font =  UIFont(name:"Helvetica", size: 18)
        
        btnSetting.addTarget(self, action: #selector(self.btnSettingPressed), for: .touchUpInside)
        btnUpgrade.addTarget(self, action: #selector(self.btnUpgradePressed), for: .touchUpInside)
        btnLogOut.addTarget(self, action: #selector(self.btnLogOutPressed), for: .touchUpInside)
        
        viewMenu.addSubview(btnSetting)
        viewMenu.addSubview(btnUpgrade)
        viewMenu.addSubview(btnLogOut)
        
        let imgSeting = UIImageView()
        let imgUpgrade = UIImageView()
        let imgLogOut = UIImageView()
        
        if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
        {
            imgSeting.frame=CGRect(x: 5, y: 5, width: 25, height: 25)
            imgUpgrade.frame=CGRect(x: 5, y: 5, width: 20, height: 25)
            imgLogOut.frame=CGRect(x: 5, y: 5, width: 25, height: 25)
        }
        else
        {
            imgSeting.frame=CGRect(x: 5, y: 5, width: 25, height: 25)
            imgLogOut.frame=CGRect(x: 5, y: 5, width: 25, height: 25)
        }
        imgSeting.image = UIImage(named:"icon_Nseting")
        imgUpgrade.image = UIImage(named:"icon_NUpgrde")
        imgLogOut.image = UIImage(named:"icon_NProf")
        
        btnSetting.addSubview(imgSeting)
        btnUpgrade.addSubview(imgUpgrade)
        btnLogOut.addSubview(imgLogOut)
    }
    
    //MARK:UIButton pressed delegate declarations here
    
    func btnJobPostScreenUI()
    {
         Constant.GlobalConstants.theAppDelegate.isPostAJobPressed=true
        
        let WTS=WorkTypeScreen(nibName: "WorkTypeScreen", bundle: nil)        
        self.navigationController?.pushViewController(WTS, animated: true)
    }
    
    func btnMenuPressed()
    {
        if isMenuPressed
        {
            let image = UIImage(named: "icon_Menu.png") as UIImage?
            btnMenu.setImage(image, for: .normal)
            btnMenu1.backgroundColor=UIColor.clear
            isMenuPressed = false
            viewMenu.removeFromSuperview()
        }
        else
        {
            btnMenu1.backgroundColor=UIColor(red: 19.0/255.0, green: 57.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            let image = UIImage(named: "icon_MenuW") as UIImage?
            btnMenu.setImage(image, for: .normal)
            self.view.bringSubview(toFront: btnMenu)
            viewForLeftMenu()
            isMenuPressed = true
        }
    }
    //MARK: btnHomeStreamPressed
    func btnHomeStreamPressed()
    {
        isHomeStream=true
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        
        self.isopen=false
        self.tblNotifications .removeFromSuperview()
        self.lblNoti.removeFromSuperview()
        self.btnNotiSetting.removeFromSuperview()
        
        viewbtnSeleted.frame=CGRect(x: 0, y: btnHomeStream.frame.maxY-0.5, width: Constant.GlobalConstants.screenWidth/2, height: 2)
        
        viewForHomeStream .removeFromSuperview()
        viewForShortListed .removeFromSuperview()
        
        viewForHomeStream .removeFromSuperview()
        viewForShortListed .removeFromSuperview()
        
        viewForHomeStream.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+1, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-150)
        viewForHomeStream.backgroundColor=UIColor.clear
        self.view.addSubview(viewForHomeStream)
        
        tblHomeStream .removeFromSuperview()
        
        tblHomeStream.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height:viewForHomeStream.frame.size.height)
        tblHomeStream.delegate=self
        tblHomeStream.dataSource=self
        tblHomeStream.backgroundColor=UIColor.clear
        tblHomeStream.tableFooterView=UIView()
        viewForHomeStream.addSubview(tblHomeStream)
        
        if app.strSelectedUser == "employee"
        {
            viewForHomeStream.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+1, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-130)
        }
        else
        {
            btnAddJob.removeFromSuperview()
            btnAddJob=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth-40, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            btnAddJob.setImage(UIImage(named:"icon_MenuAdd"), for: .normal)
            btnAddJob.addTarget(self, action: #selector(self.btnJobPostScreenUI), for: .touchUpInside)
            btnAddJob.backgroundColor = .clear
            self.view.addSubview(btnAddJob)
        }
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-25, width: Constant.GlobalConstants.screenWidth, height: 25)
        btnBtmNotification.setImage(UIImage(named:"img_NotiBG"), for: UIControlState.normal)
        btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 3)
        imgDot.backgroundColor = UIColor.white
        imgDot.layer.borderWidth = 0.5
        imgDot.layer.borderColor = UIColor.white.cgColor
        imgDot.layer.cornerRadius = 3.0
        
        btnBtmNotification.addSubview(imgDot)
        
        imgChat.frame=CGRect(x: 10, y: 2.5, width: 20, height: 20)
        imgChat.image=UIImage(named: "icon_ChatN")
        btnBtmNotification.addSubview(imgChat)
        
        getDHomeStreamListAPI()
    }
    
    //MARK: btnHomeStreamPressed
    func btnShortListedPressed()
    {
        isHomeStream=false
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        
        self.isopen=false
        self.tblNotifications .removeFromSuperview()
        self.lblNoti.removeFromSuperview()
        self.btnNotiSetting.removeFromSuperview()
        
        viewbtnSeleted.frame=CGRect(x: Constant.GlobalConstants.screenWidth/2, y: btnHomeStream.frame.maxY-0.5, width: Constant.GlobalConstants.screenWidth/2, height: 2)
        
        viewForHomeStream .removeFromSuperview()
        viewForShortListed .removeFromSuperview()
        
        viewForShortListed.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+2, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-130)
        viewForShortListed.backgroundColor=UIColor.clear
        self.view.addSubview(viewForShortListed)
        tblShortlist .removeFromSuperview()
                
        tblShortlist.frame=CGRect(x: 0, y: 0, width: Constant.GlobalConstants.screenWidth, height:viewForShortListed.frame.size.height)
        tblShortlist.delegate=self
        tblShortlist.dataSource=self
        tblShortlist.backgroundColor=UIColor.clear
        tblShortlist.tableFooterView=UIView()
        viewForShortListed.addSubview(tblShortlist)
        
        if app.strSelectedUser == "employee"
        {
            viewForHomeStream.frame=CGRect(x: 0, y: self.viewbtnSeleted.frame.maxY+1, width: Constant.GlobalConstants.screenWidth, height: Constant.GlobalConstants.screenHeight-130)
        }
        else
        {
            btnAddJob.removeFromSuperview()
            btnAddJob=UIButton(frame: CGRect(x:Constant.GlobalConstants.screenWidth-40, y: Constant.GlobalConstants.screenHeight-60, width: 30, height: 30))
            btnAddJob.setImage(UIImage(named:"icon_MenuAdd"), for: .normal)
            btnAddJob.addTarget(self, action: #selector(self.btnJobPostScreenUI), for: .touchUpInside)
            btnAddJob.backgroundColor = .clear
            self.view.addSubview(btnAddJob)
        }
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        
        btnBtmNotification .removeFromSuperview()
        imgDot.removeFromSuperview()
        btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-25, width: Constant.GlobalConstants.screenWidth, height: 25)
        btnBtmNotification.setImage(UIImage(named:"img_NotiBG"), for: UIControlState.normal)
        btnBtmNotification.addTarget(self, action:#selector(self.viewforNotificationUI), for:.touchUpInside)
        self.view.addSubview(btnBtmNotification)
        
        imgDot.frame=CGRect(x: btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 3)
        imgDot.backgroundColor = UIColor.white
        imgDot.layer.borderWidth = 0.5
        imgDot.layer.borderColor = UIColor.white.cgColor
        imgDot.layer.cornerRadius = 3.0
        
        btnBtmNotification.addSubview(imgDot)
        
        imgChat.frame=CGRect(x: 10, y: 2.5, width: 20, height: 20)
        imgChat.image=UIImage(named: "icon_ChatN")
        btnBtmNotification.addSubview(imgChat)
        
        if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employer" {
            getDShortListedAPI()
        }
        else
        {
            getUserAppliedJListedAPI()
        }
        
    }
    //MARK:btnSettingPressed
    func btnSettingPressed()
    {
        let USS = UserSettingScreen(nibName: "UserSettingScreen", bundle: nil)
        self.navigationController?.pushViewController(USS, animated: true)
    }
    
    func btnHelpPressed()
    {
        //let PJS = PostAJobScreen(nibName: "PostAJobScreen", bundle: nil)
        //self.navigationController?.pushViewController(PJS, animated: true)
        
        viewForAlertUI()
    }
    //MARK:btnUpgradePressed
    func btnUpgradePressed()
    {
        let UpgS = UpgradeScreen(nibName: "UpgradeScreen", bundle: nil)
        self.navigationController?.pushViewController(UpgS, animated: true)
    }
    
    //MARK: btnLogout Pressed
    func btnLogOutPressed()
    {
        let LC = LoginScreen(nibName: "LoginScreen", bundle: nil)
        self.navigationController?.pushViewController(LC, animated: true)
    }
    
    //
    // MARK: - View Controller DataSource and Delegate
    //
    
         func numberOfSections(in tableView: UITableView) -> Int {
            
            if tableView==tblHomeStream
            {
                return arrHSCategoryList.count
               
            }
            else if tableView == tblShortlist
            {
                return 1
            }
            else if tableView == tblNotifications
            {
                return 1
            }
           return 0
        }
        
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            if tableView == tblHomeStream
            {
                if section == strHStremSID
                {
                    return arrHSCatDetilsList.count
                }
                return 0
            }
            else if tableView == tblShortlist
            {
              return arrShortListedData.count
            }
            else if tableView == tblNotifications
            {
                return 0
            }
            return 0
    }
        // Cell
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestCell")
            
             let viewForCell=UIView()
            let lblTitle = UILabel()
            let lblSubTitle = UILabel()
            let lblType = UILabel()
            let imgProfile = UIImageView()
            let btnStar = UIButton()
            let btnResume = UIButton()
            
            cell.selectionStyle=UITableViewCellSelectionStyle.none
            cell.backgroundColor=UIColor.clear
            tblHomeStream.separatorStyle = .none
            viewForCell.frame=CGRect(x: 0,y: 0,width: UIScreen.main.bounds.size.width,height: 66)
            viewForCell.backgroundColor=UIColor.white.withAlphaComponent(0.6)
            cell.contentView .addSubview(viewForCell)
            cell.backgroundColor=UIColor.clear
       
            if tableView==tblNotifications
            {
                viewForCell.backgroundColor=UIColor.init(red: 222/255.0, green: 230/255.0, blue: 236/255.0, alpha: 1)
                imgProfile .removeFromSuperview()
                imgProfile.frame=CGRect(x: 5, y: 25, width: 10, height: 10)
                imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
                imgProfile.backgroundColor = UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                viewForCell.addSubview(imgProfile)
                
                let lblTitle = UILabel()
                lblTitle.frame=CGRect(x: 20, y: 5, width: Constant.GlobalConstants.screenWidth-20, height:50)
                lblTitle.text="A new job has been posted in \n [Homestream Category] Apply Now!"
                lblTitle.textColor=UIColor.black
                lblTitle.font=UIFont(name:"Helvetica-Light", size: 14)
                lblTitle.textAlignment=NSTextAlignment.left
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.numberOfLines = 0
                viewForCell.addSubview(lblTitle)
                
                let lblTime = UILabel()
                lblTime.frame=CGRect(x: viewForCell.frame.size.width-40, y: 0, width: 40, height: 25)
                lblTime.text="1.35 pm"
                lblTime.textAlignment = .left
                lblTime.font=UIFont(name:"Helvetica-Light", size: 10)
                lblTime.textColor=UIColor.lightGray
                viewForCell.addSubview(lblTime)
            }
            else if tableView==tblHomeStream
            {
                var dict = NSDictionary()
                
                dict = arrHSCatDetilsList[(indexPath as NSIndexPath).row] as! NSDictionary
                
                var strImage = ""
                var strImgURL = Constant.GlobalConstants.Imgbase_URL
                
                imgProfile .removeFromSuperview()
                imgProfile.frame=CGRect(x: 5, y: 13, width: 40, height: 40)
                imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
                
                if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
                {
                   strImage=(dict.value(forKey: "logo") as! String?)!
                }
                else if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employer"
                {
                    strImage=(dict.value(forKey: "image") as! String?)!
                }
                
                if strImage == "no"
                {
                    imgProfile.image=UIImage(named: "icom_job")
                }
                else
                {
                    strImgURL = strImgURL+strImage
                    imgProfile.setImageWith(NSURL(string: strImgURL) as URL!, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.white)
                }
                
                imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
                imgProfile.clipsToBounds=true
                imgProfile.layer.borderWidth=1.0
                imgProfile.layer.borderColor=UIColor.gray.cgColor
                viewForCell.addSubview(imgProfile)
            
                lblTitle .removeFromSuperview()
                lblTitle.frame=CGRect(x: imgProfile.frame.maxX+3, y: 3, width: 160, height: 20)
                
                if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
                {
                    lblTitle.text=dict.value(forKey: "company_name") as! String?
                }
                else if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employer"
                {
                    lblTitle.text=dict.value(forKey: "user_name") as! String?
                }
                
                lblTitle.textColor=UIColor.black
                lblTitle.font=UIFont(name:"Helvetica", size: 16)
                viewForCell.addSubview(lblTitle)
        
                lblSubTitle .removeFromSuperview()
                lblSubTitle.frame=CGRect(x: imgProfile.frame.maxX+3, y: lblTitle.frame.maxY, width: 120,    height: 20)
                lblSubTitle.text=dict.value(forKey: "subcatname") as! String?
                lblSubTitle.textColor=UIColor.black
                lblSubTitle.font=UIFont(name:"Helvetica-Light", size: 14)
                viewForCell.addSubview(lblSubTitle)
            
                lblType .removeFromSuperview()
                lblType.frame=CGRect(x: imgProfile.frame.maxX+3, y: lblSubTitle.frame.maxY, width: 120, height: 20)
                lblType.text=dict.value(forKey: "catname") as! String?
                lblType.textColor=UIColor.gray
                lblType.font=UIFont(name:"Helvetica-Light", size: 13)
                viewForCell.addSubview(lblType)
            
                btnStar .removeFromSuperview()
                btnStar.frame=CGRect(x: Constant.GlobalConstants.screenWidth/1.5, y: viewForCell.frame.size.height/2-15, width: 30, height: 30)
            
                viewForCell .addSubview(btnStar)
            
                if tableView == tblHomeStream
                {
                    btnStar.setBackgroundImage(UIImage(named: "icon_Star"), for: UIControlState.normal)
                }
                else
                {
                    btnStar.setBackgroundImage(UIImage(named: "icon_FStar"), for: UIControlState.normal)
                }
            
                btnResume .removeFromSuperview()
                btnResume.frame=CGRect(x: Constant.GlobalConstants.screenWidth-50, y: 13, width: 43, height: 40)
                btnResume.setBackgroundImage(UIImage(named: "icon_resume"), for: UIControlState.normal)
                viewForCell .addSubview(btnResume)
            }
            else if tableView==tblShortlist
            {
                var dict = NSDictionary()
                
                dict = arrShortListedData[(indexPath as NSIndexPath).row] as! NSDictionary
                
                var strImage = ""
                var strImgURL = Constant.GlobalConstants.Imgbase_URL
                
                
                imgProfile .removeFromSuperview()
                imgProfile.frame=CGRect(x: 5, y: 13, width: 40, height: 40)
                imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
                
                if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
                {
                    strImage=(dict.value(forKey: "logo") as! String?)!
                }
                else
                {
                     strImage=(dict.value(forKey: "image") as! String?)!
                }
            
                if strImage == "no"
                {
                    imgProfile.image=UIImage(named: "icom_job")
                }
                else
                {
                    strImgURL = strImgURL+strImage
                    imgProfile.setImageWith(NSURL(string: strImgURL) as URL!, usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.white)
                }
                
                imgProfile.layer.cornerRadius=imgProfile.frame.size.height/2
                imgProfile.clipsToBounds=true
                imgProfile.layer.borderWidth=1.0
                imgProfile.layer.borderColor=UIColor.gray.cgColor
                viewForCell.addSubview(imgProfile)
                
                lblTitle .removeFromSuperview()
                lblTitle.frame=CGRect(x: imgProfile.frame.maxX+3, y: 3, width: 160, height: 20)
                
                lblSubTitle .removeFromSuperview()
                lblSubTitle.frame=CGRect(x: imgProfile.frame.maxX+3, y: lblTitle.frame.maxY, width: 120,    height: 20)
                
                if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
                {
                    lblTitle.text=dict.value(forKey: "company_name") as! String?
                    lblSubTitle.text=dict.value(forKey: "subcatname") as! String?
                }
                else
                {
                    lblTitle.text=dict.value(forKey: "user_name") as! String?
                    lblSubTitle.text=dict.value(forKey: "subCategory") as! String?
                }
                
                lblTitle.textColor=UIColor.black
                lblTitle.font=UIFont(name:"Helvetica", size: 16)
                viewForCell.addSubview(lblTitle)
                
                lblSubTitle.textColor=UIColor.black
                lblSubTitle.font=UIFont(name:"Helvetica-Light", size: 14)
                viewForCell.addSubview(lblSubTitle)
                
                lblType .removeFromSuperview()
                lblType.frame=CGRect(x: imgProfile.frame.maxX+3, y: lblSubTitle.frame.maxY, width: 120, height: 20)
                lblType.text=dict.value(forKey: "catname") as! String?
                lblType.textColor=UIColor.gray
                lblType.font=UIFont(name:"Helvetica-Light", size: 13)
                viewForCell.addSubview(lblType)
                
                btnStar .removeFromSuperview()
                btnStar.frame=CGRect(x: Constant.GlobalConstants.screenWidth/1.5, y: viewForCell.frame.size.height/2-15, width: 30, height: 30)
                
                viewForCell .addSubview(btnStar)
                
                if tableView == tblHomeStream
                {
                    btnStar.setBackgroundImage(UIImage(named: "icon_Star"), for: UIControlState.normal)
                }
                else
                {
                    btnStar.setBackgroundImage(UIImage(named: "icon_FStar"), for: UIControlState.normal)
                }
                
                btnResume .removeFromSuperview()
                btnResume.frame=CGRect(x: Constant.GlobalConstants.screenWidth-50, y: 13, width: 43, height: 40)
                btnResume.setBackgroundImage(UIImage(named: "icon_resume"), for: UIControlState.normal)
                viewForCell .addSubview(btnResume)
            }
            return cell
        }
    
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            if tableView == tblHomeStream
            {
                if app.strSelectedUser == "employee"
                {
                    let JAS = JobApplyScreen(nibName: "JobApplyScreen", bundle: nil)
                    var dict = NSDictionary()
                    
                    dict = arrHSCatDetilsList[(indexPath as NSIndexPath).row] as! NSDictionary
                    
                    JAS.SelectedJobID = dict.value(forKey: "jid") as! NSString
                    JAS.strCompName = dict.value(forKey: "company_name") as! NSString
                    JAS.strImg = dict.value(forKey: "logo") as! NSString
                    JAS.strCatName = dict.value(forKey: "catname") as! NSString
                    JAS.strSubName = dict.value(forKey: "subcatname") as! NSString
                    self.navigationController?.pushViewController(JAS, animated: true)
                }
                else
                {
                    let JAS = UserProfileScreen(nibName: "UserProfileScreen", bundle: nil)
                    var dict = NSDictionary()
                    
                    dict = arrHSCatDetilsList[(indexPath as NSIndexPath).row] as! NSDictionary
                    JAS.strSelectedUID=dict.value(forKey: "userid") as! NSString
                    Constant.GlobalConstants.theAppDelegate.strSelectedJobAppliedID=dict.value(forKey: "jobapplied_id") as! NSString
                    print(Constant.GlobalConstants.theAppDelegate.strSelectedJobAppliedID)
                    self.navigationController?.pushViewController(JAS, animated: true)
                }
            }
        }
    
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 67.0
        }
        
        // Header
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
           
            if tableView == tblHomeStream
            {
                var dict = NSDictionary()
                var strCName = NSString()
                
                dict = arrHSCategoryList[section] as! NSDictionary
                strCName = dict.value(forKey: "sub_category") as! NSString
                
                print(header.frame.size.height)
                header.frame=CGRect(x: 0, y: 0, width: header.frame.size.width, height: 68)
                header.lblTitle.text = strCName as String
                header.arrowLabel.text = ">"
                
                var collapsed = false
                var strCollapsed = ""
                strCollapsed = arrCheckCollapsed .object(at: section) as! String
                print(strCollapsed)
                
                if strCollapsed == "true"
                {
                    collapsed = false
                }
                else
                {
                    collapsed = true
                }
               header.setCollapsed(collapsed: collapsed)
                header.section = section
                header.delegate = self
                return header
            }
            return nil
    }
    
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            
            if tableView == tblHomeStream
            {
                return 70.0
            }
            else
            {
                return 0
            }
        
    }
    //MARK: ViewForHelpView
    func viewForAlertUI()
    {
        viewForAlert.frame=CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width,height:0)
        let whte = UIColor.black
        viewForAlert.backgroundColor = whte.withAlphaComponent(0.7)
        self.view.addSubview(viewForAlert)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.btnCloseSavePressed))
        viewForAlert.addGestureRecognizer(tap)
        viewForAlert.isUserInteractionEnabled = true
        
        var frame1 = CGRect()
        
        frame1=viewForAlert.frame
        frame1=CGRect(x:0,y:0,width:self.view.frame.size.width ,height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frame1
        },
                       completion: { finished in
                        
                        let viewforTip = UIView()
                        let btnClose = UIButton()
                        let lblTitle = UILabel()
                        let lblSubTitle = UILabel()
                        let lblSubTitle1 = UILabel()
                        let imgCheck = UIImageView()
                        
                        btnClose.frame=CGRect(x: self.viewForAlert.frame.size.width-40, y: 2, width: 35, height: 35)
                        //btnClose.setBackgroundImage(UIImage(named: "icon_Close"), for: UIControlState.normal)
                        btnClose.addTarget(self, action:#selector(self.btnCloseSavePressed), for:.touchUpInside)
                        
                        viewforTip.frame=CGRect(x: 30, y:Constant.GlobalConstants.screenHeight/2-50
                            , width: Constant.GlobalConstants.screenWidth-60, height: 100)
                        viewforTip.backgroundColor=UIColor.white
                        viewforTip.layer.cornerRadius=10.0
                        viewforTip.layer.borderWidth=1.0
                        viewforTip.layer.borderColor=UIColor.black.cgColor
                        
                        lblTitle.frame=CGRect(x: 5, y: 10, width: viewforTip.frame.size.width-10, height: 30)
                        lblTitle.text="saved!"
                        lblTitle.font=UIFont(name:"Helvetica", size: 18)
                        lblTitle.textAlignment=NSTextAlignment.center
                        lblTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.frame=CGRect(x: 5, y: lblTitle.frame.maxY, width: viewforTip.frame.size.width-10, height: 50)
                        lblSubTitle1.frame=CGRect(x: 5, y: lblSubTitle.frame.maxY+10, width: viewforTip.frame.size.width-10, height: 40)
                        
                        lblSubTitle.text="This job has been added to your saved Jobs. \n You can apply to this job at a later date by \n selecting the Jobs option in the menu."
                        
                        lblSubTitle.font=UIFont(name:"Helvetica", size: 12)
                        lblSubTitle.textAlignment=NSTextAlignment.center
                        
                        lblSubTitle.textColor=UIColor.init(red: 19/255.0, green: 57/255.0, blue: 107/255.0, alpha: 1)
                        
                        lblSubTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                        lblSubTitle.numberOfLines = 0
                        self.viewForAlert.addSubview(btnClose)
                        self.viewForAlert.addSubview(viewforTip)
                        
                        viewforTip.addSubview(lblTitle)
                        viewforTip.addSubview(lblSubTitle)                        
        })
    }
    
    //MARK: UIButton Help Pressed
    func btnCloseSavePressed()
    {
        let subview = UIView ()
        
        for  subview in self.viewForAlert .subviews {
            subview .removeFromSuperview()
        }
        //        for(UIView *subview in [_viewForPopUp subviews])
        //        {
        //            [subview removeFromSuperview];
        //        }
        var frameViewForHelp = CGRect()
        
        frameViewForHelp=self.viewForAlert.frame;
        frameViewForHelp=CGRect(x: 30,y: Constant.GlobalConstants.screenHeight/2,width: Constant.GlobalConstants.screenWidth-60,height: 0);
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.viewForAlert.frame=frameViewForHelp
        },completion: { finished in
            
            self.viewForAlert.frame=frameViewForHelp
        })
    }
    
    //MARK: ViewFor Bottom NotificationUI
    func viewforNotificationUI()
    {
        if isopen
        {
            isopen=false
            var frameViewForHelp = CGRect()
            frameViewForHelp=self.viewForNotification.frame;
            frameViewForHelp=CGRect(x: 0,y: Constant.GlobalConstants.screenHeight,width: Constant.GlobalConstants.screenWidth,height: 0);
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseOut,
                           animations: {
                            self.viewForNotification.frame=frameViewForHelp
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                            
                            
            },completion: { finished in
                self.viewForNotification.frame=frameViewForHelp
                self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight-30, width:Constant.GlobalConstants.screenWidth, height: 30)
                self.imgDot.frame=CGRect(x: self.btnBtmNotification.frame.size.width/2-30, y: 10, width: 60, height: 5)
                self.tblNotifications .removeFromSuperview()
                self.lblNoti.removeFromSuperview()
                self.btnNotiSetting.removeFromSuperview()
            })
        }
        else
        {
            isopen=true
            self.viewForNotification.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
            self.viewForNotification.backgroundColor=UIColor.clear
            var frame1 = CGRect()
            
            frame1=viewForNotification.frame
            frame1=CGRect(x:0,y:self.view.frame.size.height/2,width:self.view.frame.size.width ,height: self.view.frame.size.height/2)
            
            UIView.animate(withDuration: 0.8,delay: 0.5,options: UIViewAnimationOptions.curveEaseIn,
                           animations: {
                            self.viewForNotification.frame=frame1
                            self.btnBtmNotification.frame=CGRect(x: 0, y: Constant.GlobalConstants.screenHeight/2-40, width:Constant.GlobalConstants.screenWidth, height: 25)
                            
            },completion: { finished in
                
                self.tblNotifications .removeFromSuperview()
                self.tblNotifications.frame=CGRect(x: 0, y: 0, width: self.viewForNotification.frame.size.width, height: self.viewForNotification.frame.size.height)
                self.tblNotifications.dataSource=self
                self.tblNotifications.delegate=self
                self.tblNotifications.tableFooterView=UIView()
                self.tblNotifications.backgroundColor=UIColor.clear
                self.viewForNotification.addSubview(self.tblNotifications)
            })
            
            self.view.addSubview(viewForNotification)
        }
    }
                    /*---------------    User Side   -----------------*/
    //MARK: get Shorlisted user Info
    func getUserAppliedJListedAPI()
    {
        isHomeStreamData=false
        isUserJList=false
        isUserAppliedAPI=true
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjobapplied_listbyUser.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    //Employee/ User HomeStream Job List
    func getUsrHSJobListAPI()
    {
        var dict = NSDictionary()
        var strCName = NSString()
        let bodydata = NSMutableDictionary()
        dict = arrHSCategoryList[strHStremSID] as! NSDictionary
        strCName = dict.value(forKey: "sid") as! NSString
        isHomeStreamData=false
        isUserJList=true
        isUserAppliedAPI=false
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((strCName), forKey: "sid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsgetjobs_list.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
                    /*---------------    Employer/ Company Side   -----------------*/
    
    //Employer/ Company get  HomeStream Users List
    func getEmployerHSJobListAPI()
    {
        var dict = NSDictionary()
        var strCName = NSString()
        let bodydata = NSMutableDictionary()
        dict = arrHSCategoryList[strHStremSID] as! NSDictionary
        strCName = dict.value(forKey: "sid") as! NSString
        isHomeStreamData=false
        isUserJList=true
        isUserAppliedAPI=false
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((strCName), forKey: "sid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjobapplied_listbyemployer.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    //MARK: get Shorlisted user Info
    func getDShortListedAPI()
    {
        isHomeStreamData=false
        isUserJList=false
        isUserAppliedAPI=false
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsshortlist_list.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    
    //MARK: get Applied Job List
    func getDAppliedJobListAPI()
    {
        isHomeStreamData=false
        isUserJList=false
        isUserAppliedAPI=false
        
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "sid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsjobapplied_listbyemployer.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    
    
    /*-----------       Get Homestream Data by all side     ------------*/
    func getDHomeStreamListAPI()
    {
        isHomeStreamData=true
        isUserJList=false
        isUserAppliedAPI=false
        let bodydata = NSMutableDictionary()
        bodydata.setObject((Constant.GlobalConstants.theAppDelegate.strLUID), forKey: "uid" as NSCopying)
        print("BODY DATA for Sub Work Type : ",bodydata)
        
        Constant.GlobalConstants.theAppDelegate.showSpinnerView(_view: self.view)
        let objc = WebApiController()
        objc.callAPI_POST("api/wsemployerHomestream.php", andParams: bodydata as [NSObject : AnyObject], successCallback: #selector(self.response_Work_SubType(apiAlias:response:)), andDelegate: self)
    }
    
    func response_Work_SubType(apiAlias:NSString,response:NSData)
    {
        Constant.GlobalConstants.theAppDelegate.stopSpinner()
        do
        {
            let ResponceData = try JSONSerialization.jsonObject(with: response as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            //print("Response For response_user_login : \n ",ResponceData)
            
            let strsucess1=(ResponceData .value(forKey: "status") as! NSNumber)
            print(strsucess1)
            var strMSG1 = NSString()
            strMSG1=ResponceData .value(forKey: "message") as! NSString
            if strMSG1 == "sus"
            {
                if isUserJList
                {
                    arrHSCatDetilsList.removeAllObjects()
                    self.arrHSCatDetilsList = (ResponceData.value(forKey: "info")as! NSMutableArray)
                    tblHomeStream.reloadData()
                }
                else if isHomeStreamData
                {
                    arrHSCategoryList.removeAllObjects()
                    self.arrHSCategoryList = (ResponceData.value(forKey: "info")as! NSMutableArray)
                    
                    for i in 0 ..< self.arrHSCategoryList.count
                    {
                       arrCheckCollapsed.insert("false", at: i)
                    }
                    print(arrCheckCollapsed)
                    tblHomeStream.reloadData()
                }
                else
                {
                    arrShortListedData.removeAllObjects()
                    self.arrShortListedData = (ResponceData.value(forKey: "info")as! NSMutableArray)
                    tblShortlist.reloadData()
                }
            }
        }
        catch
        {
            print("Error")
        }
    }
}
//
// MARK: - Section Header Delegate
//
extension UserMainDashboard: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(header: CollapsibleTableViewHeader, section: Int) {
       
        var collapsed = false
        print(collapsed)
        
        var strCollapsed = ""
        strCollapsed = arrCheckCollapsed .object(at: section) as! String
        print(strCollapsed)
        
        if strCollapsed == "true"
        {
            arrCheckCollapsed.removeObject(at: section)
            arrCheckCollapsed.insert("false", at: section)
            collapsed = false
            strHStremSID = section
            arrHSCatDetilsList.removeAllObjects()
            tblHomeStream .reloadData()
        }
        else
        {
            arrCheckCollapsed.removeObject(at: section)
            arrCheckCollapsed.insert("true", at: section)
            collapsed = true
            
            if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employee"
            {
                getUsrHSJobListAPI()
            }
            else if Constant.GlobalConstants.theAppDelegate.strSelectedUser == "employer"
            {
                getEmployerHSJobListAPI()
            }
        }
        print(collapsed)
        header.setCollapsed(collapsed: collapsed)
}
}
